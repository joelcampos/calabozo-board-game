﻿
namespace Calabozo
{
    partial class MainBoard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainBoard));
            this.PanelTable = new System.Windows.Forms.Panel();
            this.PicTransportTager = new System.Windows.Forms.PictureBox();
            this.PanelPlayerAction = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.PanelActionBattle = new System.Windows.Forms.Panel();
            this.lblPLayerActionName = new System.Windows.Forms.Label();
            this.lblActionSelectLowerMessage = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.PanelUseItem = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.PanelRollDice = new System.Windows.Forms.Panel();
            this.PanelBattleWindow = new System.Windows.Forms.Panel();
            this.lblBattleExtraTreasures = new System.Windows.Forms.Label();
            this.PicBattleTreasure = new System.Windows.Forms.PictureBox();
            this.lblBattleLowerMessage = new System.Windows.Forms.Label();
            this.lblBattleMonsterStrengh = new System.Windows.Forms.Label();
            this.lblBattlePlayerBonus = new System.Windows.Forms.Label();
            this.PicBattleDice2 = new System.Windows.Forms.PictureBox();
            this.PicBattleDice1 = new System.Windows.Forms.PictureBox();
            this.lblBattlePlayerName = new System.Windows.Forms.Label();
            this.lblBattleMonsterName = new System.Windows.Forms.Label();
            this.lblBattleTopMessage = new System.Windows.Forms.Label();
            this.PicBattlePlayer = new System.Windows.Forms.PictureBox();
            this.PicBattleMonster = new System.Windows.Forms.PictureBox();
            this.PanelTreasureSelection = new System.Windows.Forms.Panel();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.PicTreasureSelect5 = new System.Windows.Forms.PictureBox();
            this.PicTreasureSelect4 = new System.Windows.Forms.PictureBox();
            this.PicTreasureSelect3 = new System.Windows.Forms.PictureBox();
            this.PicTreasureSelect2 = new System.Windows.Forms.PictureBox();
            this.PicTreasureSelectRArrow = new System.Windows.Forms.PictureBox();
            this.PicTreasureSelectLArrow = new System.Windows.Forms.PictureBox();
            this.PicTreasureSelect1 = new System.Windows.Forms.PictureBox();
            this.PicMonsterPreviewWest = new System.Windows.Forms.PictureBox();
            this.PicMonsterPreviewEast = new System.Windows.Forms.PictureBox();
            this.PicMonsterPreviewSouth = new System.Windows.Forms.PictureBox();
            this.PicMonsterPreviewNorth = new System.Windows.Forms.PictureBox();
            this.PanelWinner = new System.Windows.Forms.Panel();
            this.ffgvdr = new System.Windows.Forms.Label();
            this.PicWinner = new System.Windows.Forms.PictureBox();
            this.PanelTurnLost = new System.Windows.Forms.Panel();
            this.lblTurnLostTopMessage = new System.Windows.Forms.Label();
            this.lblTurnLostDownMessage = new System.Windows.Forms.Label();
            this.lblTurnLostMessage = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.PanelPlayerSelector = new System.Windows.Forms.Panel();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.lblPlayerSelectStartGame = new System.Windows.Forms.Label();
            this.lblElf2Select = new System.Windows.Forms.Label();
            this.lblElf1Select = new System.Windows.Forms.Label();
            this.lblMage2Select = new System.Windows.Forms.Label();
            this.lblMage1Select = new System.Windows.Forms.Label();
            this.lblDwarf2Select = new System.Windows.Forms.Label();
            this.lblDwarf1Select = new System.Windows.Forms.Label();
            this.lblWarrior2Select = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblWarrior1Select = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.lblPlayerSelectbanner = new System.Windows.Forms.Label();
            this.PanelOpenLockedDoorRoll = new System.Windows.Forms.Panel();
            this.lblLockedRollMessage = new System.Windows.Forms.Label();
            this.PicLockedDoorRoll = new System.Windows.Forms.PictureBox();
            this.lblMovementBanner = new System.Windows.Forms.Label();
            this.PicRightArrow = new System.Windows.Forms.PictureBox();
            this.PicLeftArrow = new System.Windows.Forms.PictureBox();
            this.PicDownArrow = new System.Windows.Forms.PictureBox();
            this.PicUpArrow = new System.Windows.Forms.PictureBox();
            this.PanelRollDiceToMove = new System.Windows.Forms.Panel();
            this.PicDiceRoll2 = new System.Windows.Forms.PictureBox();
            this.PicDiceRoll1 = new System.Windows.Forms.PictureBox();
            this.lblRollDiceToMoveTopMessage = new System.Windows.Forms.Label();
            this.lblRollDiceToMoveLowerMessage = new System.Windows.Forms.Label();
            this.lblRollDiceMessage = new System.Windows.Forms.Label();
            this.lblActivePlayerTopMessage = new System.Windows.Forms.Label();
            this.lblTurnOrderTop = new System.Windows.Forms.Label();
            this.lblRoundCounter = new System.Windows.Forms.Label();
            this.PanelTurnOrderDecider = new System.Windows.Forms.Panel();
            this.PicTurn8 = new System.Windows.Forms.PictureBox();
            this.PicTurnArrow7 = new System.Windows.Forms.PictureBox();
            this.PicTurn7 = new System.Windows.Forms.PictureBox();
            this.PicTurnArrow3 = new System.Windows.Forms.PictureBox();
            this.PicTurn3 = new System.Windows.Forms.PictureBox();
            this.PicTurnArrow2 = new System.Windows.Forms.PictureBox();
            this.PicTurn2 = new System.Windows.Forms.PictureBox();
            this.PicTurnArrow1 = new System.Windows.Forms.PictureBox();
            this.PicTurn1 = new System.Windows.Forms.PictureBox();
            this.PicTurnArrow6 = new System.Windows.Forms.PictureBox();
            this.PicTurn6 = new System.Windows.Forms.PictureBox();
            this.PicTurnArrow5 = new System.Windows.Forms.PictureBox();
            this.PicTurn5 = new System.Windows.Forms.PictureBox();
            this.PicTurnArrow4 = new System.Windows.Forms.PictureBox();
            this.PicTurn4 = new System.Windows.Forms.PictureBox();
            this.lblTurnDeciderMessage = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.PicWarrior2 = new System.Windows.Forms.PictureBox();
            this.PicElf2 = new System.Windows.Forms.PictureBox();
            this.PicDwarf2 = new System.Windows.Forms.PictureBox();
            this.PicMage2 = new System.Windows.Forms.PictureBox();
            this.PicMage1 = new System.Windows.Forms.PictureBox();
            this.PicDwarf1 = new System.Windows.Forms.PictureBox();
            this.PicElf1 = new System.Windows.Forms.PictureBox();
            this.PicWarrior1 = new System.Windows.Forms.PictureBox();
            this.PicCell9 = new System.Windows.Forms.PictureBox();
            this.PicCell40 = new System.Windows.Forms.PictureBox();
            this.PicCell36 = new System.Windows.Forms.PictureBox();
            this.PicCell34 = new System.Windows.Forms.PictureBox();
            this.PicCell28 = new System.Windows.Forms.PictureBox();
            this.PicCell27 = new System.Windows.Forms.PictureBox();
            this.PicCell26 = new System.Windows.Forms.PictureBox();
            this.PicCell23 = new System.Windows.Forms.PictureBox();
            this.PicCell22 = new System.Windows.Forms.PictureBox();
            this.PicCell21 = new System.Windows.Forms.PictureBox();
            this.PicCell20 = new System.Windows.Forms.PictureBox();
            this.PicCell13 = new System.Windows.Forms.PictureBox();
            this.PicCell16 = new System.Windows.Forms.PictureBox();
            this.PicCell10 = new System.Windows.Forms.PictureBox();
            this.PicCell8 = new System.Windows.Forms.PictureBox();
            this.PicCell7 = new System.Windows.Forms.PictureBox();
            this.PicCell5 = new System.Windows.Forms.PictureBox();
            this.PicCell4 = new System.Windows.Forms.PictureBox();
            this.PicCell3 = new System.Windows.Forms.PictureBox();
            this.PicCell41 = new System.Windows.Forms.PictureBox();
            this.PicCell399 = new System.Windows.Forms.PictureBox();
            this.PicCell38 = new System.Windows.Forms.PictureBox();
            this.PicCell37 = new System.Windows.Forms.PictureBox();
            this.PicCell333 = new System.Windows.Forms.PictureBox();
            this.PicCell32 = new System.Windows.Forms.PictureBox();
            this.PicCell31 = new System.Windows.Forms.PictureBox();
            this.PicCell30 = new System.Windows.Forms.PictureBox();
            this.PicCell35 = new System.Windows.Forms.PictureBox();
            this.PicCell25 = new System.Windows.Forms.PictureBox();
            this.PicCell24 = new System.Windows.Forms.PictureBox();
            this.PicCell29 = new System.Windows.Forms.PictureBox();
            this.PicCell19 = new System.Windows.Forms.PictureBox();
            this.PicCell18 = new System.Windows.Forms.PictureBox();
            this.PicCell17 = new System.Windows.Forms.PictureBox();
            this.PicCell15 = new System.Windows.Forms.PictureBox();
            this.PicCell14 = new System.Windows.Forms.PictureBox();
            this.PicCell12 = new System.Windows.Forms.PictureBox();
            this.PicCell11 = new System.Windows.Forms.PictureBox();
            this.PicCell6 = new System.Windows.Forms.PictureBox();
            this.PicCell2 = new System.Windows.Forms.PictureBox();
            this.PicCell0 = new System.Windows.Forms.PictureBox();
            this.PicTower6 = new System.Windows.Forms.PictureBox();
            this.PicTower5 = new System.Windows.Forms.PictureBox();
            this.PicTower4 = new System.Windows.Forms.PictureBox();
            this.PicTower3 = new System.Windows.Forms.PictureBox();
            this.PicTower2 = new System.Windows.Forms.PictureBox();
            this.PicTower1 = new System.Windows.Forms.PictureBox();
            this.PicCell1 = new System.Windows.Forms.PictureBox();
            this.lblDebugOutput = new System.Windows.Forms.Label();
            this.PicCell39 = new System.Windows.Forms.PictureBox();
            this.PicCell33 = new System.Windows.Forms.PictureBox();
            this.PanelPlayerDead = new System.Windows.Forms.Panel();
            this.lblPlayerDeadUpperMessage = new System.Windows.Forms.Label();
            this.lblPlayerDeadLowerMessage = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.PanelUseElixir = new System.Windows.Forms.Panel();
            this.lblUseElixir = new System.Windows.Forms.Label();
            this.lblUseElixirLowerMessage = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.PanelTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicTransportTager)).BeginInit();
            this.PanelPlayerAction.SuspendLayout();
            this.PanelBattleWindow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicBattleTreasure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBattleDice2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBattleDice1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBattlePlayer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBattleMonster)).BeginInit();
            this.PanelTreasureSelection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTreasureSelect5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTreasureSelect4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTreasureSelect3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTreasureSelect2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTreasureSelectRArrow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTreasureSelectLArrow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTreasureSelect1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicMonsterPreviewWest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicMonsterPreviewEast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicMonsterPreviewSouth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicMonsterPreviewNorth)).BeginInit();
            this.PanelWinner.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicWinner)).BeginInit();
            this.PanelTurnLost.SuspendLayout();
            this.PanelPlayerSelector.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.PanelOpenLockedDoorRoll.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicLockedDoorRoll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicRightArrow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicLeftArrow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicDownArrow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicUpArrow)).BeginInit();
            this.PanelRollDiceToMove.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicDiceRoll2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicDiceRoll1)).BeginInit();
            this.PanelTurnOrderDecider.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicTurn8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTurnArrow7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTurn7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTurnArrow3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTurn3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTurnArrow2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTurn2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTurnArrow1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTurn1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTurnArrow6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTurn6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTurnArrow5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTurn5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTurnArrow4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTurn4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicWarrior2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicElf2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicDwarf2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicMage2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicMage1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicDwarf1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicElf1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicWarrior1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell399)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell333)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTower6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTower5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTower4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTower3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTower2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTower1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell33)).BeginInit();
            this.PanelPlayerDead.SuspendLayout();
            this.PanelUseElixir.SuspendLayout();
            this.SuspendLayout();
            // 
            // PanelTable
            // 
            this.PanelTable.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PanelTable.BackgroundImage")));
            this.PanelTable.Controls.Add(this.PanelPlayerSelector);
            this.PanelTable.Controls.Add(this.PicTransportTager);
            this.PanelTable.Controls.Add(this.PanelPlayerAction);
            this.PanelTable.Controls.Add(this.PanelBattleWindow);
            this.PanelTable.Controls.Add(this.PanelTreasureSelection);
            this.PanelTable.Controls.Add(this.PicMonsterPreviewWest);
            this.PanelTable.Controls.Add(this.PicMonsterPreviewEast);
            this.PanelTable.Controls.Add(this.PicMonsterPreviewSouth);
            this.PanelTable.Controls.Add(this.PicMonsterPreviewNorth);
            this.PanelTable.Controls.Add(this.PanelWinner);
            this.PanelTable.Controls.Add(this.PanelTurnLost);
            this.PanelTable.Controls.Add(this.PanelOpenLockedDoorRoll);
            this.PanelTable.Controls.Add(this.lblMovementBanner);
            this.PanelTable.Controls.Add(this.PicRightArrow);
            this.PanelTable.Controls.Add(this.PicLeftArrow);
            this.PanelTable.Controls.Add(this.PicDownArrow);
            this.PanelTable.Controls.Add(this.PicUpArrow);
            this.PanelTable.Controls.Add(this.PanelRollDiceToMove);
            this.PanelTable.Controls.Add(this.lblActivePlayerTopMessage);
            this.PanelTable.Controls.Add(this.lblTurnOrderTop);
            this.PanelTable.Controls.Add(this.lblRoundCounter);
            this.PanelTable.Controls.Add(this.PanelTurnOrderDecider);
            this.PanelTable.Controls.Add(this.PicWarrior2);
            this.PanelTable.Controls.Add(this.PicElf2);
            this.PanelTable.Controls.Add(this.PicDwarf2);
            this.PanelTable.Controls.Add(this.PicMage2);
            this.PanelTable.Controls.Add(this.PicMage1);
            this.PanelTable.Controls.Add(this.PicDwarf1);
            this.PanelTable.Controls.Add(this.PicElf1);
            this.PanelTable.Controls.Add(this.PicWarrior1);
            this.PanelTable.Controls.Add(this.PicCell9);
            this.PanelTable.Controls.Add(this.PicCell40);
            this.PanelTable.Controls.Add(this.PicCell36);
            this.PanelTable.Controls.Add(this.PicCell34);
            this.PanelTable.Controls.Add(this.PicCell28);
            this.PanelTable.Controls.Add(this.PicCell27);
            this.PanelTable.Controls.Add(this.PicCell26);
            this.PanelTable.Controls.Add(this.PicCell23);
            this.PanelTable.Controls.Add(this.PicCell22);
            this.PanelTable.Controls.Add(this.PicCell21);
            this.PanelTable.Controls.Add(this.PicCell20);
            this.PanelTable.Controls.Add(this.PicCell13);
            this.PanelTable.Controls.Add(this.PicCell16);
            this.PanelTable.Controls.Add(this.PicCell10);
            this.PanelTable.Controls.Add(this.PicCell8);
            this.PanelTable.Controls.Add(this.PicCell7);
            this.PanelTable.Controls.Add(this.PicCell5);
            this.PanelTable.Controls.Add(this.PicCell4);
            this.PanelTable.Controls.Add(this.PicCell3);
            this.PanelTable.Controls.Add(this.PicCell41);
            this.PanelTable.Controls.Add(this.PicCell399);
            this.PanelTable.Controls.Add(this.PicCell38);
            this.PanelTable.Controls.Add(this.PicCell37);
            this.PanelTable.Controls.Add(this.PicCell333);
            this.PanelTable.Controls.Add(this.PicCell32);
            this.PanelTable.Controls.Add(this.PicCell31);
            this.PanelTable.Controls.Add(this.PicCell30);
            this.PanelTable.Controls.Add(this.PicCell35);
            this.PanelTable.Controls.Add(this.PicCell25);
            this.PanelTable.Controls.Add(this.PicCell24);
            this.PanelTable.Controls.Add(this.PicCell29);
            this.PanelTable.Controls.Add(this.PicCell19);
            this.PanelTable.Controls.Add(this.PicCell18);
            this.PanelTable.Controls.Add(this.PicCell17);
            this.PanelTable.Controls.Add(this.PicCell15);
            this.PanelTable.Controls.Add(this.PicCell14);
            this.PanelTable.Controls.Add(this.PicCell12);
            this.PanelTable.Controls.Add(this.PicCell11);
            this.PanelTable.Controls.Add(this.PicCell6);
            this.PanelTable.Controls.Add(this.PicCell2);
            this.PanelTable.Controls.Add(this.PicCell0);
            this.PanelTable.Controls.Add(this.PicTower6);
            this.PanelTable.Controls.Add(this.PicTower5);
            this.PanelTable.Controls.Add(this.PicTower4);
            this.PanelTable.Controls.Add(this.PicTower3);
            this.PanelTable.Controls.Add(this.PicTower2);
            this.PanelTable.Controls.Add(this.PicTower1);
            this.PanelTable.Controls.Add(this.PicCell1);
            this.PanelTable.Location = new System.Drawing.Point(131, 0);
            this.PanelTable.Name = "PanelTable";
            this.PanelTable.Size = new System.Drawing.Size(1133, 1147);
            this.PanelTable.TabIndex = 0;
            // 
            // PicTransportTager
            // 
            this.PicTransportTager.BackColor = System.Drawing.Color.YellowGreen;
            this.PicTransportTager.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicTransportTager.BackgroundImage")));
            this.PicTransportTager.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicTransportTager.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicTransportTager.Location = new System.Drawing.Point(549, 533);
            this.PicTransportTager.Name = "PicTransportTager";
            this.PicTransportTager.Size = new System.Drawing.Size(40, 40);
            this.PicTransportTager.TabIndex = 76;
            this.PicTransportTager.TabStop = false;
            this.PicTransportTager.Visible = false;
            // 
            // PanelPlayerAction
            // 
            this.PanelPlayerAction.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PanelPlayerAction.BackgroundImage")));
            this.PanelPlayerAction.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PanelPlayerAction.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelPlayerAction.Controls.Add(this.label9);
            this.PanelPlayerAction.Controls.Add(this.PanelActionBattle);
            this.PanelPlayerAction.Controls.Add(this.lblPLayerActionName);
            this.PanelPlayerAction.Controls.Add(this.lblActionSelectLowerMessage);
            this.PanelPlayerAction.Controls.Add(this.label8);
            this.PanelPlayerAction.Controls.Add(this.PanelUseItem);
            this.PanelPlayerAction.Controls.Add(this.label7);
            this.PanelPlayerAction.Controls.Add(this.PanelRollDice);
            this.PanelPlayerAction.Location = new System.Drawing.Point(966, 879);
            this.PanelPlayerAction.Name = "PanelPlayerAction";
            this.PanelPlayerAction.Size = new System.Drawing.Size(482, 229);
            this.PanelPlayerAction.TabIndex = 1;
            this.PanelPlayerAction.Visible = false;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(323, 41);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(141, 20);
            this.label9.TabIndex = 68;
            this.label9.Text = "Battle!";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // PanelActionBattle
            // 
            this.PanelActionBattle.BackColor = System.Drawing.Color.Transparent;
            this.PanelActionBattle.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PanelActionBattle.BackgroundImage")));
            this.PanelActionBattle.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PanelActionBattle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelActionBattle.Location = new System.Drawing.Point(335, 63);
            this.PanelActionBattle.Name = "PanelActionBattle";
            this.PanelActionBattle.Size = new System.Drawing.Size(121, 121);
            this.PanelActionBattle.TabIndex = 67;
            // 
            // lblPLayerActionName
            // 
            this.lblPLayerActionName.BackColor = System.Drawing.Color.Transparent;
            this.lblPLayerActionName.Font = new System.Drawing.Font("Arial Narrow", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPLayerActionName.ForeColor = System.Drawing.Color.White;
            this.lblPLayerActionName.Location = new System.Drawing.Point(62, 5);
            this.lblPLayerActionName.Name = "lblPLayerActionName";
            this.lblPLayerActionName.Size = new System.Drawing.Size(335, 35);
            this.lblPLayerActionName.TabIndex = 66;
            this.lblPLayerActionName.Text = "Warrior1\'s Turn";
            this.lblPLayerActionName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblActionSelectLowerMessage
            // 
            this.lblActionSelectLowerMessage.BackColor = System.Drawing.Color.Transparent;
            this.lblActionSelectLowerMessage.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblActionSelectLowerMessage.ForeColor = System.Drawing.Color.White;
            this.lblActionSelectLowerMessage.Location = new System.Drawing.Point(61, 194);
            this.lblActionSelectLowerMessage.Name = "lblActionSelectLowerMessage";
            this.lblActionSelectLowerMessage.Size = new System.Drawing.Size(351, 30);
            this.lblActionSelectLowerMessage.TabIndex = 65;
            this.lblActionSelectLowerMessage.Text = "AD - Move Selection       J - Select";
            this.lblActionSelectLowerMessage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(159, 40);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(164, 20);
            this.label8.TabIndex = 64;
            this.label8.Text = "Teletransportation";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // PanelUseItem
            // 
            this.PanelUseItem.BackColor = System.Drawing.Color.Transparent;
            this.PanelUseItem.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PanelUseItem.BackgroundImage")));
            this.PanelUseItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PanelUseItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelUseItem.Location = new System.Drawing.Point(180, 63);
            this.PanelUseItem.Name = "PanelUseItem";
            this.PanelUseItem.Size = new System.Drawing.Size(121, 121);
            this.PanelUseItem.TabIndex = 63;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(9, 41);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(141, 20);
            this.label7.TabIndex = 62;
            this.label7.Text = "Roll Dice!";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // PanelRollDice
            // 
            this.PanelRollDice.BackColor = System.Drawing.Color.DarkRed;
            this.PanelRollDice.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PanelRollDice.BackgroundImage")));
            this.PanelRollDice.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PanelRollDice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelRollDice.Location = new System.Drawing.Point(23, 63);
            this.PanelRollDice.Name = "PanelRollDice";
            this.PanelRollDice.Size = new System.Drawing.Size(111, 121);
            this.PanelRollDice.TabIndex = 0;
            // 
            // PanelBattleWindow
            // 
            this.PanelBattleWindow.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PanelBattleWindow.BackgroundImage")));
            this.PanelBattleWindow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PanelBattleWindow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelBattleWindow.Controls.Add(this.lblBattleExtraTreasures);
            this.PanelBattleWindow.Controls.Add(this.PicBattleTreasure);
            this.PanelBattleWindow.Controls.Add(this.lblBattleLowerMessage);
            this.PanelBattleWindow.Controls.Add(this.lblBattleMonsterStrengh);
            this.PanelBattleWindow.Controls.Add(this.lblBattlePlayerBonus);
            this.PanelBattleWindow.Controls.Add(this.PicBattleDice2);
            this.PanelBattleWindow.Controls.Add(this.PicBattleDice1);
            this.PanelBattleWindow.Controls.Add(this.lblBattlePlayerName);
            this.PanelBattleWindow.Controls.Add(this.lblBattleMonsterName);
            this.PanelBattleWindow.Controls.Add(this.lblBattleTopMessage);
            this.PanelBattleWindow.Controls.Add(this.PicBattlePlayer);
            this.PanelBattleWindow.Controls.Add(this.PicBattleMonster);
            this.PanelBattleWindow.Location = new System.Drawing.Point(66, 1076);
            this.PanelBattleWindow.Name = "PanelBattleWindow";
            this.PanelBattleWindow.Size = new System.Drawing.Size(627, 414);
            this.PanelBattleWindow.TabIndex = 2;
            this.PanelBattleWindow.Visible = false;
            // 
            // lblBattleExtraTreasures
            // 
            this.lblBattleExtraTreasures.BackColor = System.Drawing.Color.Transparent;
            this.lblBattleExtraTreasures.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBattleExtraTreasures.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBattleExtraTreasures.ForeColor = System.Drawing.Color.White;
            this.lblBattleExtraTreasures.Location = new System.Drawing.Point(213, 284);
            this.lblBattleExtraTreasures.Name = "lblBattleExtraTreasures";
            this.lblBattleExtraTreasures.Size = new System.Drawing.Size(199, 78);
            this.lblBattleExtraTreasures.TabIndex = 78;
            this.lblBattleExtraTreasures.Text = "Extra Treasures:";
            this.lblBattleExtraTreasures.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblBattleExtraTreasures.Visible = false;
            // 
            // PicBattleTreasure
            // 
            this.PicBattleTreasure.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicBattleTreasure.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicBattleTreasure.BackgroundImage")));
            this.PicBattleTreasure.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicBattleTreasure.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicBattleTreasure.Location = new System.Drawing.Point(243, 117);
            this.PicBattleTreasure.Name = "PicBattleTreasure";
            this.PicBattleTreasure.Size = new System.Drawing.Size(134, 164);
            this.PicBattleTreasure.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicBattleTreasure.TabIndex = 77;
            this.PicBattleTreasure.TabStop = false;
            this.PicBattleTreasure.Visible = false;
            // 
            // lblBattleLowerMessage
            // 
            this.lblBattleLowerMessage.BackColor = System.Drawing.Color.Transparent;
            this.lblBattleLowerMessage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBattleLowerMessage.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBattleLowerMessage.ForeColor = System.Drawing.Color.White;
            this.lblBattleLowerMessage.Location = new System.Drawing.Point(101, 362);
            this.lblBattleLowerMessage.Name = "lblBattleLowerMessage";
            this.lblBattleLowerMessage.Size = new System.Drawing.Size(424, 30);
            this.lblBattleLowerMessage.TabIndex = 76;
            this.lblBattleLowerMessage.Text = "J - Roll for Attack!";
            this.lblBattleLowerMessage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblBattleLowerMessage.Visible = false;
            // 
            // lblBattleMonsterStrengh
            // 
            this.lblBattleMonsterStrengh.BackColor = System.Drawing.Color.Transparent;
            this.lblBattleMonsterStrengh.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBattleMonsterStrengh.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBattleMonsterStrengh.ForeColor = System.Drawing.Color.White;
            this.lblBattleMonsterStrengh.Location = new System.Drawing.Point(13, 316);
            this.lblBattleMonsterStrengh.Name = "lblBattleMonsterStrengh";
            this.lblBattleMonsterStrengh.Size = new System.Drawing.Size(180, 30);
            this.lblBattleMonsterStrengh.TabIndex = 75;
            this.lblBattleMonsterStrengh.Text = "Strenght: 12";
            this.lblBattleMonsterStrengh.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblBattlePlayerBonus
            // 
            this.lblBattlePlayerBonus.BackColor = System.Drawing.Color.Transparent;
            this.lblBattlePlayerBonus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBattlePlayerBonus.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBattlePlayerBonus.ForeColor = System.Drawing.Color.White;
            this.lblBattlePlayerBonus.Location = new System.Drawing.Point(437, 316);
            this.lblBattlePlayerBonus.Name = "lblBattlePlayerBonus";
            this.lblBattlePlayerBonus.Size = new System.Drawing.Size(180, 30);
            this.lblBattlePlayerBonus.TabIndex = 74;
            this.lblBattlePlayerBonus.Text = "Bonus Points: 0";
            this.lblBattlePlayerBonus.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // PicBattleDice2
            // 
            this.PicBattleDice2.BackColor = System.Drawing.Color.Transparent;
            this.PicBattleDice2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicBattleDice2.BackgroundImage")));
            this.PicBattleDice2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicBattleDice2.Location = new System.Drawing.Point(308, 150);
            this.PicBattleDice2.Name = "PicBattleDice2";
            this.PicBattleDice2.Size = new System.Drawing.Size(115, 115);
            this.PicBattleDice2.TabIndex = 73;
            this.PicBattleDice2.TabStop = false;
            this.PicBattleDice2.Visible = false;
            // 
            // PicBattleDice1
            // 
            this.PicBattleDice1.BackColor = System.Drawing.Color.Transparent;
            this.PicBattleDice1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicBattleDice1.BackgroundImage")));
            this.PicBattleDice1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicBattleDice1.Location = new System.Drawing.Point(188, 148);
            this.PicBattleDice1.Name = "PicBattleDice1";
            this.PicBattleDice1.Size = new System.Drawing.Size(115, 115);
            this.PicBattleDice1.TabIndex = 72;
            this.PicBattleDice1.TabStop = false;
            this.PicBattleDice1.Visible = false;
            // 
            // lblBattlePlayerName
            // 
            this.lblBattlePlayerName.BackColor = System.Drawing.Color.Transparent;
            this.lblBattlePlayerName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBattlePlayerName.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBattlePlayerName.ForeColor = System.Drawing.Color.White;
            this.lblBattlePlayerName.Location = new System.Drawing.Point(437, 287);
            this.lblBattlePlayerName.Name = "lblBattlePlayerName";
            this.lblBattlePlayerName.Size = new System.Drawing.Size(180, 30);
            this.lblBattlePlayerName.TabIndex = 71;
            this.lblBattlePlayerName.Text = "Player";
            this.lblBattlePlayerName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblBattleMonsterName
            // 
            this.lblBattleMonsterName.BackColor = System.Drawing.Color.Transparent;
            this.lblBattleMonsterName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBattleMonsterName.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBattleMonsterName.ForeColor = System.Drawing.Color.White;
            this.lblBattleMonsterName.Location = new System.Drawing.Point(13, 287);
            this.lblBattleMonsterName.Name = "lblBattleMonsterName";
            this.lblBattleMonsterName.Size = new System.Drawing.Size(180, 30);
            this.lblBattleMonsterName.TabIndex = 70;
            this.lblBattleMonsterName.Text = "?";
            this.lblBattleMonsterName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblBattleTopMessage
            // 
            this.lblBattleTopMessage.BackColor = System.Drawing.Color.Transparent;
            this.lblBattleTopMessage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBattleTopMessage.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBattleTopMessage.ForeColor = System.Drawing.Color.White;
            this.lblBattleTopMessage.Location = new System.Drawing.Point(35, 22);
            this.lblBattleTopMessage.Name = "lblBattleTopMessage";
            this.lblBattleTopMessage.Size = new System.Drawing.Size(536, 30);
            this.lblBattleTopMessage.TabIndex = 69;
            this.lblBattleTopMessage.Text = "Battle!";
            this.lblBattleTopMessage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // PicBattlePlayer
            // 
            this.PicBattlePlayer.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicBattlePlayer.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicBattlePlayer.BackgroundImage")));
            this.PicBattlePlayer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicBattlePlayer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicBattlePlayer.Location = new System.Drawing.Point(456, 120);
            this.PicBattlePlayer.Name = "PicBattlePlayer";
            this.PicBattlePlayer.Size = new System.Drawing.Size(134, 164);
            this.PicBattlePlayer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicBattlePlayer.TabIndex = 19;
            this.PicBattlePlayer.TabStop = false;
            // 
            // PicBattleMonster
            // 
            this.PicBattleMonster.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicBattleMonster.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicBattleMonster.BackgroundImage")));
            this.PicBattleMonster.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicBattleMonster.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicBattleMonster.Location = new System.Drawing.Point(35, 120);
            this.PicBattleMonster.Name = "PicBattleMonster";
            this.PicBattleMonster.Size = new System.Drawing.Size(134, 164);
            this.PicBattleMonster.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicBattleMonster.TabIndex = 18;
            this.PicBattleMonster.TabStop = false;
            // 
            // PanelTreasureSelection
            // 
            this.PanelTreasureSelection.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PanelTreasureSelection.BackgroundImage")));
            this.PanelTreasureSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PanelTreasureSelection.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelTreasureSelection.Controls.Add(this.pictureBox18);
            this.PanelTreasureSelection.Controls.Add(this.label11);
            this.PanelTreasureSelection.Controls.Add(this.label10);
            this.PanelTreasureSelection.Controls.Add(this.PicTreasureSelect5);
            this.PanelTreasureSelection.Controls.Add(this.PicTreasureSelect4);
            this.PanelTreasureSelection.Controls.Add(this.PicTreasureSelect3);
            this.PanelTreasureSelection.Controls.Add(this.PicTreasureSelect2);
            this.PanelTreasureSelection.Controls.Add(this.PicTreasureSelectRArrow);
            this.PanelTreasureSelection.Controls.Add(this.PicTreasureSelectLArrow);
            this.PanelTreasureSelection.Controls.Add(this.PicTreasureSelect1);
            this.PanelTreasureSelection.Location = new System.Drawing.Point(489, 924);
            this.PanelTreasureSelection.Name = "PanelTreasureSelection";
            this.PanelTreasureSelection.Size = new System.Drawing.Size(433, 197);
            this.PanelTreasureSelection.TabIndex = 2;
            this.PanelTreasureSelection.Visible = false;
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox18.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox18.Image")));
            this.pictureBox18.Location = new System.Drawing.Point(191, 140);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(45, 18);
            this.pictureBox18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox18.TabIndex = 69;
            this.pictureBox18.TabStop = false;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(171, 160);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(79, 20);
            this.label11.TabIndex = 68;
            this.label11.Text = "J - Select";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(132, 20);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(177, 20);
            this.label10.TabIndex = 67;
            this.label10.Text = "Select a Treasure!";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // PicTreasureSelect5
            // 
            this.PicTreasureSelect5.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicTreasureSelect5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicTreasureSelect5.BackgroundImage")));
            this.PicTreasureSelect5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicTreasureSelect5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicTreasureSelect5.Location = new System.Drawing.Point(330, 65);
            this.PicTreasureSelect5.Name = "PicTreasureSelect5";
            this.PicTreasureSelect5.Size = new System.Drawing.Size(55, 70);
            this.PicTreasureSelect5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicTreasureSelect5.TabIndex = 66;
            this.PicTreasureSelect5.TabStop = false;
            this.PicTreasureSelect5.Visible = false;
            // 
            // PicTreasureSelect4
            // 
            this.PicTreasureSelect4.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicTreasureSelect4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicTreasureSelect4.BackgroundImage")));
            this.PicTreasureSelect4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicTreasureSelect4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicTreasureSelect4.Location = new System.Drawing.Point(259, 64);
            this.PicTreasureSelect4.Name = "PicTreasureSelect4";
            this.PicTreasureSelect4.Size = new System.Drawing.Size(55, 70);
            this.PicTreasureSelect4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicTreasureSelect4.TabIndex = 65;
            this.PicTreasureSelect4.TabStop = false;
            this.PicTreasureSelect4.Visible = false;
            // 
            // PicTreasureSelect3
            // 
            this.PicTreasureSelect3.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicTreasureSelect3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicTreasureSelect3.BackgroundImage")));
            this.PicTreasureSelect3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicTreasureSelect3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicTreasureSelect3.Location = new System.Drawing.Point(186, 64);
            this.PicTreasureSelect3.Name = "PicTreasureSelect3";
            this.PicTreasureSelect3.Size = new System.Drawing.Size(55, 70);
            this.PicTreasureSelect3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicTreasureSelect3.TabIndex = 64;
            this.PicTreasureSelect3.TabStop = false;
            this.PicTreasureSelect3.Visible = false;
            // 
            // PicTreasureSelect2
            // 
            this.PicTreasureSelect2.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicTreasureSelect2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicTreasureSelect2.BackgroundImage")));
            this.PicTreasureSelect2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicTreasureSelect2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicTreasureSelect2.Location = new System.Drawing.Point(114, 64);
            this.PicTreasureSelect2.Name = "PicTreasureSelect2";
            this.PicTreasureSelect2.Size = new System.Drawing.Size(55, 70);
            this.PicTreasureSelect2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicTreasureSelect2.TabIndex = 63;
            this.PicTreasureSelect2.TabStop = false;
            this.PicTreasureSelect2.Visible = false;
            // 
            // PicTreasureSelectRArrow
            // 
            this.PicTreasureSelectRArrow.BackColor = System.Drawing.Color.Transparent;
            this.PicTreasureSelectRArrow.Image = ((System.Drawing.Image)(resources.GetObject("PicTreasureSelectRArrow.Image")));
            this.PicTreasureSelectRArrow.Location = new System.Drawing.Point(396, 77);
            this.PicTreasureSelectRArrow.Name = "PicTreasureSelectRArrow";
            this.PicTreasureSelectRArrow.Size = new System.Drawing.Size(18, 45);
            this.PicTreasureSelectRArrow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicTreasureSelectRArrow.TabIndex = 62;
            this.PicTreasureSelectRArrow.TabStop = false;
            this.PicTreasureSelectRArrow.Visible = false;
            // 
            // PicTreasureSelectLArrow
            // 
            this.PicTreasureSelectLArrow.BackColor = System.Drawing.Color.Transparent;
            this.PicTreasureSelectLArrow.Image = ((System.Drawing.Image)(resources.GetObject("PicTreasureSelectLArrow.Image")));
            this.PicTreasureSelectLArrow.Location = new System.Drawing.Point(16, 79);
            this.PicTreasureSelectLArrow.Name = "PicTreasureSelectLArrow";
            this.PicTreasureSelectLArrow.Size = new System.Drawing.Size(18, 45);
            this.PicTreasureSelectLArrow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicTreasureSelectLArrow.TabIndex = 61;
            this.PicTreasureSelectLArrow.TabStop = false;
            this.PicTreasureSelectLArrow.Visible = false;
            // 
            // PicTreasureSelect1
            // 
            this.PicTreasureSelect1.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicTreasureSelect1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicTreasureSelect1.BackgroundImage")));
            this.PicTreasureSelect1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicTreasureSelect1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicTreasureSelect1.Location = new System.Drawing.Point(45, 65);
            this.PicTreasureSelect1.Name = "PicTreasureSelect1";
            this.PicTreasureSelect1.Size = new System.Drawing.Size(55, 70);
            this.PicTreasureSelect1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicTreasureSelect1.TabIndex = 13;
            this.PicTreasureSelect1.TabStop = false;
            this.PicTreasureSelect1.Visible = false;
            // 
            // PicMonsterPreviewWest
            // 
            this.PicMonsterPreviewWest.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicMonsterPreviewWest.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicMonsterPreviewWest.BackgroundImage")));
            this.PicMonsterPreviewWest.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicMonsterPreviewWest.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicMonsterPreviewWest.Location = new System.Drawing.Point(144, 892);
            this.PicMonsterPreviewWest.Name = "PicMonsterPreviewWest";
            this.PicMonsterPreviewWest.Size = new System.Drawing.Size(159, 191);
            this.PicMonsterPreviewWest.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicMonsterPreviewWest.TabIndex = 75;
            this.PicMonsterPreviewWest.TabStop = false;
            this.PicMonsterPreviewWest.Visible = false;
            // 
            // PicMonsterPreviewEast
            // 
            this.PicMonsterPreviewEast.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicMonsterPreviewEast.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicMonsterPreviewEast.BackgroundImage")));
            this.PicMonsterPreviewEast.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicMonsterPreviewEast.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicMonsterPreviewEast.Location = new System.Drawing.Point(33, 881);
            this.PicMonsterPreviewEast.Name = "PicMonsterPreviewEast";
            this.PicMonsterPreviewEast.Size = new System.Drawing.Size(159, 191);
            this.PicMonsterPreviewEast.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicMonsterPreviewEast.TabIndex = 74;
            this.PicMonsterPreviewEast.TabStop = false;
            this.PicMonsterPreviewEast.Visible = false;
            // 
            // PicMonsterPreviewSouth
            // 
            this.PicMonsterPreviewSouth.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicMonsterPreviewSouth.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicMonsterPreviewSouth.BackgroundImage")));
            this.PicMonsterPreviewSouth.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicMonsterPreviewSouth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicMonsterPreviewSouth.Location = new System.Drawing.Point(14, 878);
            this.PicMonsterPreviewSouth.Name = "PicMonsterPreviewSouth";
            this.PicMonsterPreviewSouth.Size = new System.Drawing.Size(159, 191);
            this.PicMonsterPreviewSouth.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicMonsterPreviewSouth.TabIndex = 73;
            this.PicMonsterPreviewSouth.TabStop = false;
            this.PicMonsterPreviewSouth.Visible = false;
            // 
            // PicMonsterPreviewNorth
            // 
            this.PicMonsterPreviewNorth.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicMonsterPreviewNorth.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicMonsterPreviewNorth.BackgroundImage")));
            this.PicMonsterPreviewNorth.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicMonsterPreviewNorth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicMonsterPreviewNorth.Location = new System.Drawing.Point(3, 868);
            this.PicMonsterPreviewNorth.Name = "PicMonsterPreviewNorth";
            this.PicMonsterPreviewNorth.Size = new System.Drawing.Size(159, 191);
            this.PicMonsterPreviewNorth.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicMonsterPreviewNorth.TabIndex = 72;
            this.PicMonsterPreviewNorth.TabStop = false;
            this.PicMonsterPreviewNorth.Visible = false;
            // 
            // PanelWinner
            // 
            this.PanelWinner.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PanelWinner.BackgroundImage")));
            this.PanelWinner.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PanelWinner.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelWinner.Controls.Add(this.ffgvdr);
            this.PanelWinner.Controls.Add(this.PicWinner);
            this.PanelWinner.Location = new System.Drawing.Point(461, 885);
            this.PanelWinner.Name = "PanelWinner";
            this.PanelWinner.Size = new System.Drawing.Size(241, 251);
            this.PanelWinner.TabIndex = 70;
            this.PanelWinner.Visible = false;
            // 
            // ffgvdr
            // 
            this.ffgvdr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ffgvdr.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ffgvdr.ForeColor = System.Drawing.Color.White;
            this.ffgvdr.Location = new System.Drawing.Point(41, 27);
            this.ffgvdr.Name = "ffgvdr";
            this.ffgvdr.Size = new System.Drawing.Size(162, 30);
            this.ffgvdr.TabIndex = 69;
            this.ffgvdr.Text = "Winner!!!";
            this.ffgvdr.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // PicWinner
            // 
            this.PicWinner.BackColor = System.Drawing.Color.Transparent;
            this.PicWinner.Location = new System.Drawing.Point(71, 97);
            this.PicWinner.Name = "PicWinner";
            this.PicWinner.Size = new System.Drawing.Size(92, 114);
            this.PicWinner.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicWinner.TabIndex = 0;
            this.PicWinner.TabStop = false;
            // 
            // PanelTurnLost
            // 
            this.PanelTurnLost.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PanelTurnLost.BackgroundImage")));
            this.PanelTurnLost.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PanelTurnLost.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelTurnLost.Controls.Add(this.lblTurnLostTopMessage);
            this.PanelTurnLost.Controls.Add(this.lblTurnLostDownMessage);
            this.PanelTurnLost.Controls.Add(this.lblTurnLostMessage);
            this.PanelTurnLost.Controls.Add(this.panel3);
            this.PanelTurnLost.Location = new System.Drawing.Point(325, 893);
            this.PanelTurnLost.Name = "PanelTurnLost";
            this.PanelTurnLost.Size = new System.Drawing.Size(482, 229);
            this.PanelTurnLost.TabIndex = 69;
            this.PanelTurnLost.Visible = false;
            // 
            // lblTurnLostTopMessage
            // 
            this.lblTurnLostTopMessage.BackColor = System.Drawing.Color.Transparent;
            this.lblTurnLostTopMessage.Font = new System.Drawing.Font("Arial Narrow", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTurnLostTopMessage.ForeColor = System.Drawing.Color.White;
            this.lblTurnLostTopMessage.Location = new System.Drawing.Point(72, 5);
            this.lblTurnLostTopMessage.Name = "lblTurnLostTopMessage";
            this.lblTurnLostTopMessage.Size = new System.Drawing.Size(335, 35);
            this.lblTurnLostTopMessage.TabIndex = 66;
            this.lblTurnLostTopMessage.Text = "Warrior1\'s Turn";
            this.lblTurnLostTopMessage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblTurnLostDownMessage
            // 
            this.lblTurnLostDownMessage.BackColor = System.Drawing.Color.Transparent;
            this.lblTurnLostDownMessage.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTurnLostDownMessage.ForeColor = System.Drawing.Color.White;
            this.lblTurnLostDownMessage.Location = new System.Drawing.Point(61, 194);
            this.lblTurnLostDownMessage.Name = "lblTurnLostDownMessage";
            this.lblTurnLostDownMessage.Size = new System.Drawing.Size(351, 30);
            this.lblTurnLostDownMessage.TabIndex = 65;
            this.lblTurnLostDownMessage.Text = "AD - Move Selection       J - Select";
            this.lblTurnLostDownMessage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblTurnLostMessage
            // 
            this.lblTurnLostMessage.BackColor = System.Drawing.Color.Transparent;
            this.lblTurnLostMessage.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTurnLostMessage.ForeColor = System.Drawing.Color.White;
            this.lblTurnLostMessage.Location = new System.Drawing.Point(97, 40);
            this.lblTurnLostMessage.Name = "lblTurnLostMessage";
            this.lblTurnLostMessage.Size = new System.Drawing.Size(299, 30);
            this.lblTurnLostMessage.TabIndex = 64;
            this.lblTurnLostMessage.Text = "Use Item";
            this.lblTurnLostMessage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel3.BackgroundImage")));
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Location = new System.Drawing.Point(180, 71);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(121, 121);
            this.panel3.TabIndex = 63;
            // 
            // PanelPlayerSelector
            // 
            this.PanelPlayerSelector.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PanelPlayerSelector.BackgroundImage")));
            this.PanelPlayerSelector.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PanelPlayerSelector.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelPlayerSelector.Controls.Add(this.pictureBox9);
            this.PanelPlayerSelector.Controls.Add(this.pictureBox10);
            this.PanelPlayerSelector.Controls.Add(this.lblPlayerSelectStartGame);
            this.PanelPlayerSelector.Controls.Add(this.lblElf2Select);
            this.PanelPlayerSelector.Controls.Add(this.lblElf1Select);
            this.PanelPlayerSelector.Controls.Add(this.lblMage2Select);
            this.PanelPlayerSelector.Controls.Add(this.lblMage1Select);
            this.PanelPlayerSelector.Controls.Add(this.lblDwarf2Select);
            this.PanelPlayerSelector.Controls.Add(this.lblDwarf1Select);
            this.PanelPlayerSelector.Controls.Add(this.lblWarrior2Select);
            this.PanelPlayerSelector.Controls.Add(this.label5);
            this.PanelPlayerSelector.Controls.Add(this.lblWarrior1Select);
            this.PanelPlayerSelector.Controls.Add(this.label4);
            this.PanelPlayerSelector.Controls.Add(this.label3);
            this.PanelPlayerSelector.Controls.Add(this.label2);
            this.PanelPlayerSelector.Controls.Add(this.label1);
            this.PanelPlayerSelector.Controls.Add(this.pictureBox1);
            this.PanelPlayerSelector.Controls.Add(this.pictureBox2);
            this.PanelPlayerSelector.Controls.Add(this.pictureBox3);
            this.PanelPlayerSelector.Controls.Add(this.pictureBox4);
            this.PanelPlayerSelector.Controls.Add(this.pictureBox5);
            this.PanelPlayerSelector.Controls.Add(this.pictureBox6);
            this.PanelPlayerSelector.Controls.Add(this.pictureBox7);
            this.PanelPlayerSelector.Controls.Add(this.pictureBox8);
            this.PanelPlayerSelector.Controls.Add(this.lblPlayerSelectbanner);
            this.PanelPlayerSelector.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.PanelPlayerSelector.Location = new System.Drawing.Point(40, 906);
            this.PanelPlayerSelector.Name = "PanelPlayerSelector";
            this.PanelPlayerSelector.Size = new System.Drawing.Size(584, 370);
            this.PanelPlayerSelector.TabIndex = 1;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox9.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox9.Image")));
            this.pictureBox9.Location = new System.Drawing.Point(546, 194);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(18, 26);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox9.TabIndex = 80;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox10.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox10.Image")));
            this.pictureBox10.Location = new System.Drawing.Point(38, 191);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(18, 26);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox10.TabIndex = 79;
            this.pictureBox10.TabStop = false;
            // 
            // lblPlayerSelectStartGame
            // 
            this.lblPlayerSelectStartGame.BackColor = System.Drawing.Color.DarkOliveGreen;
            this.lblPlayerSelectStartGame.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayerSelectStartGame.ForeColor = System.Drawing.Color.White;
            this.lblPlayerSelectStartGame.Location = new System.Drawing.Point(211, 270);
            this.lblPlayerSelectStartGame.Name = "lblPlayerSelectStartGame";
            this.lblPlayerSelectStartGame.Size = new System.Drawing.Size(189, 25);
            this.lblPlayerSelectStartGame.TabIndex = 78;
            this.lblPlayerSelectStartGame.Text = "Not Enough Players";
            this.lblPlayerSelectStartGame.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblElf2Select
            // 
            this.lblElf2Select.BackColor = System.Drawing.Color.DarkOliveGreen;
            this.lblElf2Select.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblElf2Select.ForeColor = System.Drawing.Color.White;
            this.lblElf2Select.Location = new System.Drawing.Point(493, 194);
            this.lblElf2Select.Name = "lblElf2Select";
            this.lblElf2Select.Size = new System.Drawing.Size(48, 25);
            this.lblElf2Select.TabIndex = 77;
            this.lblElf2Select.Text = "Select";
            this.lblElf2Select.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblElf1Select
            // 
            this.lblElf1Select.BackColor = System.Drawing.Color.DarkOliveGreen;
            this.lblElf1Select.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblElf1Select.ForeColor = System.Drawing.Color.White;
            this.lblElf1Select.Location = new System.Drawing.Point(438, 194);
            this.lblElf1Select.Name = "lblElf1Select";
            this.lblElf1Select.Size = new System.Drawing.Size(48, 25);
            this.lblElf1Select.TabIndex = 76;
            this.lblElf1Select.Text = "Select";
            this.lblElf1Select.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMage2Select
            // 
            this.lblMage2Select.BackColor = System.Drawing.Color.DarkOliveGreen;
            this.lblMage2Select.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMage2Select.ForeColor = System.Drawing.Color.White;
            this.lblMage2Select.Location = new System.Drawing.Point(369, 193);
            this.lblMage2Select.Name = "lblMage2Select";
            this.lblMage2Select.Size = new System.Drawing.Size(48, 25);
            this.lblMage2Select.TabIndex = 75;
            this.lblMage2Select.Text = "Select";
            this.lblMage2Select.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMage1Select
            // 
            this.lblMage1Select.BackColor = System.Drawing.Color.DarkOliveGreen;
            this.lblMage1Select.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMage1Select.ForeColor = System.Drawing.Color.White;
            this.lblMage1Select.Location = new System.Drawing.Point(315, 193);
            this.lblMage1Select.Name = "lblMage1Select";
            this.lblMage1Select.Size = new System.Drawing.Size(48, 25);
            this.lblMage1Select.TabIndex = 74;
            this.lblMage1Select.Text = "Select";
            this.lblMage1Select.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDwarf2Select
            // 
            this.lblDwarf2Select.BackColor = System.Drawing.Color.DarkOliveGreen;
            this.lblDwarf2Select.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDwarf2Select.ForeColor = System.Drawing.Color.White;
            this.lblDwarf2Select.Location = new System.Drawing.Point(246, 193);
            this.lblDwarf2Select.Name = "lblDwarf2Select";
            this.lblDwarf2Select.Size = new System.Drawing.Size(48, 25);
            this.lblDwarf2Select.TabIndex = 73;
            this.lblDwarf2Select.Text = "Select";
            this.lblDwarf2Select.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDwarf1Select
            // 
            this.lblDwarf1Select.BackColor = System.Drawing.Color.DarkOliveGreen;
            this.lblDwarf1Select.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDwarf1Select.ForeColor = System.Drawing.Color.White;
            this.lblDwarf1Select.Location = new System.Drawing.Point(191, 193);
            this.lblDwarf1Select.Name = "lblDwarf1Select";
            this.lblDwarf1Select.Size = new System.Drawing.Size(48, 25);
            this.lblDwarf1Select.TabIndex = 72;
            this.lblDwarf1Select.Text = "Select";
            this.lblDwarf1Select.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWarrior2Select
            // 
            this.lblWarrior2Select.BackColor = System.Drawing.Color.DarkOliveGreen;
            this.lblWarrior2Select.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWarrior2Select.ForeColor = System.Drawing.Color.White;
            this.lblWarrior2Select.Location = new System.Drawing.Point(117, 192);
            this.lblWarrior2Select.Name = "lblWarrior2Select";
            this.lblWarrior2Select.Size = new System.Drawing.Size(48, 25);
            this.lblWarrior2Select.TabIndex = 71;
            this.lblWarrior2Select.Text = "Select";
            this.lblWarrior2Select.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(68, 330);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(468, 25);
            this.label5.TabIndex = 70;
            this.label5.Text = "WASD - Move Selector         J - Change Selection";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblWarrior1Select
            // 
            this.lblWarrior1Select.BackColor = System.Drawing.Color.Maroon;
            this.lblWarrior1Select.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWarrior1Select.ForeColor = System.Drawing.Color.White;
            this.lblWarrior1Select.Location = new System.Drawing.Point(62, 192);
            this.lblWarrior1Select.Name = "lblWarrior1Select";
            this.lblWarrior1Select.Size = new System.Drawing.Size(48, 25);
            this.lblWarrior1Select.TabIndex = 69;
            this.lblWarrior1Select.Text = "Select";
            this.lblWarrior1Select.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Black;
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(432, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 25);
            this.label4.TabIndex = 68;
            this.label4.Text = "Elfo";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Black;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(310, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 25);
            this.label3.TabIndex = 67;
            this.label3.Text = "Mago";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Black;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(188, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 25);
            this.label2.TabIndex = 66;
            this.label2.Text = "Duende";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Black;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(58, 101);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 25);
            this.label1.TabIndex = 65;
            this.label1.Text = "Guerrero";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(116, 129);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(50, 60);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 64;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Location = new System.Drawing.Point(488, 129);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(50, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 63;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox3.BackgroundImage")));
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox3.Location = new System.Drawing.Point(245, 130);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(50, 60);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 62;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox4.BackgroundImage")));
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox4.Location = new System.Drawing.Point(366, 129);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(50, 60);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 61;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox5.BackgroundImage")));
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox5.Location = new System.Drawing.Point(313, 129);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(50, 60);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 60;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox6.BackgroundImage")));
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox6.Location = new System.Drawing.Point(191, 130);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(50, 60);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 59;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox7.BackgroundImage")));
            this.pictureBox7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox7.Location = new System.Drawing.Point(435, 129);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(50, 60);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 58;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox8.BackgroundImage")));
            this.pictureBox8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox8.Location = new System.Drawing.Point(60, 129);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(50, 60);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 57;
            this.pictureBox8.TabStop = false;
            // 
            // lblPlayerSelectbanner
            // 
            this.lblPlayerSelectbanner.BackColor = System.Drawing.Color.Black;
            this.lblPlayerSelectbanner.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayerSelectbanner.ForeColor = System.Drawing.Color.White;
            this.lblPlayerSelectbanner.Location = new System.Drawing.Point(57, 32);
            this.lblPlayerSelectbanner.Name = "lblPlayerSelectbanner";
            this.lblPlayerSelectbanner.Size = new System.Drawing.Size(468, 25);
            this.lblPlayerSelectbanner.TabIndex = 0;
            this.lblPlayerSelectbanner.Text = "Player Select";
            this.lblPlayerSelectbanner.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // PanelOpenLockedDoorRoll
            // 
            this.PanelOpenLockedDoorRoll.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PanelOpenLockedDoorRoll.BackgroundImage")));
            this.PanelOpenLockedDoorRoll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PanelOpenLockedDoorRoll.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelOpenLockedDoorRoll.Controls.Add(this.lblLockedRollMessage);
            this.PanelOpenLockedDoorRoll.Controls.Add(this.PicLockedDoorRoll);
            this.PanelOpenLockedDoorRoll.Location = new System.Drawing.Point(491, 366);
            this.PanelOpenLockedDoorRoll.Name = "PanelOpenLockedDoorRoll";
            this.PanelOpenLockedDoorRoll.Size = new System.Drawing.Size(150, 155);
            this.PanelOpenLockedDoorRoll.TabIndex = 1;
            this.PanelOpenLockedDoorRoll.Visible = false;
            // 
            // lblLockedRollMessage
            // 
            this.lblLockedRollMessage.BackColor = System.Drawing.Color.Transparent;
            this.lblLockedRollMessage.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLockedRollMessage.ForeColor = System.Drawing.Color.White;
            this.lblLockedRollMessage.Location = new System.Drawing.Point(6, 124);
            this.lblLockedRollMessage.Name = "lblLockedRollMessage";
            this.lblLockedRollMessage.Size = new System.Drawing.Size(141, 20);
            this.lblLockedRollMessage.TabIndex = 69;
            this.lblLockedRollMessage.Text = "J- Roll   I - Cancel";
            this.lblLockedRollMessage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // PicLockedDoorRoll
            // 
            this.PicLockedDoorRoll.BackColor = System.Drawing.Color.Transparent;
            this.PicLockedDoorRoll.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicLockedDoorRoll.BackgroundImage")));
            this.PicLockedDoorRoll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicLockedDoorRoll.Location = new System.Drawing.Point(19, 5);
            this.PicLockedDoorRoll.Name = "PicLockedDoorRoll";
            this.PicLockedDoorRoll.Size = new System.Drawing.Size(115, 115);
            this.PicLockedDoorRoll.TabIndex = 68;
            this.PicLockedDoorRoll.TabStop = false;
            // 
            // lblMovementBanner
            // 
            this.lblMovementBanner.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblMovementBanner.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMovementBanner.ForeColor = System.Drawing.Color.White;
            this.lblMovementBanner.Location = new System.Drawing.Point(536, 209);
            this.lblMovementBanner.Name = "lblMovementBanner";
            this.lblMovementBanner.Size = new System.Drawing.Size(170, 30);
            this.lblMovementBanner.TabIndex = 68;
            this.lblMovementBanner.Text = "Spaces Left: 12";
            this.lblMovementBanner.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblMovementBanner.Visible = false;
            // 
            // PicRightArrow
            // 
            this.PicRightArrow.BackColor = System.Drawing.Color.Transparent;
            this.PicRightArrow.Image = ((System.Drawing.Image)(resources.GetObject("PicRightArrow.Image")));
            this.PicRightArrow.Location = new System.Drawing.Point(641, 257);
            this.PicRightArrow.Name = "PicRightArrow";
            this.PicRightArrow.Size = new System.Drawing.Size(18, 45);
            this.PicRightArrow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicRightArrow.TabIndex = 60;
            this.PicRightArrow.TabStop = false;
            this.PicRightArrow.Visible = false;
            // 
            // PicLeftArrow
            // 
            this.PicLeftArrow.BackColor = System.Drawing.Color.Transparent;
            this.PicLeftArrow.Image = ((System.Drawing.Image)(resources.GetObject("PicLeftArrow.Image")));
            this.PicLeftArrow.Location = new System.Drawing.Point(580, 257);
            this.PicLeftArrow.Name = "PicLeftArrow";
            this.PicLeftArrow.Size = new System.Drawing.Size(18, 45);
            this.PicLeftArrow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicLeftArrow.TabIndex = 59;
            this.PicLeftArrow.TabStop = false;
            this.PicLeftArrow.Visible = false;
            // 
            // PicDownArrow
            // 
            this.PicDownArrow.BackColor = System.Drawing.Color.Transparent;
            this.PicDownArrow.Image = ((System.Drawing.Image)(resources.GetObject("PicDownArrow.Image")));
            this.PicDownArrow.Location = new System.Drawing.Point(597, 302);
            this.PicDownArrow.Name = "PicDownArrow";
            this.PicDownArrow.Size = new System.Drawing.Size(45, 18);
            this.PicDownArrow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicDownArrow.TabIndex = 58;
            this.PicDownArrow.TabStop = false;
            this.PicDownArrow.Visible = false;
            // 
            // PicUpArrow
            // 
            this.PicUpArrow.BackColor = System.Drawing.Color.Transparent;
            this.PicUpArrow.Image = ((System.Drawing.Image)(resources.GetObject("PicUpArrow.Image")));
            this.PicUpArrow.Location = new System.Drawing.Point(597, 238);
            this.PicUpArrow.Name = "PicUpArrow";
            this.PicUpArrow.Size = new System.Drawing.Size(45, 18);
            this.PicUpArrow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicUpArrow.TabIndex = 57;
            this.PicUpArrow.TabStop = false;
            this.PicUpArrow.Visible = false;
            // 
            // PanelRollDiceToMove
            // 
            this.PanelRollDiceToMove.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PanelRollDiceToMove.BackgroundImage")));
            this.PanelRollDiceToMove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PanelRollDiceToMove.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelRollDiceToMove.Controls.Add(this.PicDiceRoll2);
            this.PanelRollDiceToMove.Controls.Add(this.PicDiceRoll1);
            this.PanelRollDiceToMove.Controls.Add(this.lblRollDiceToMoveTopMessage);
            this.PanelRollDiceToMove.Controls.Add(this.lblRollDiceToMoveLowerMessage);
            this.PanelRollDiceToMove.Controls.Add(this.lblRollDiceMessage);
            this.PanelRollDiceToMove.Location = new System.Drawing.Point(354, 924);
            this.PanelRollDiceToMove.Name = "PanelRollDiceToMove";
            this.PanelRollDiceToMove.Size = new System.Drawing.Size(382, 229);
            this.PanelRollDiceToMove.TabIndex = 67;
            this.PanelRollDiceToMove.Visible = false;
            // 
            // PicDiceRoll2
            // 
            this.PicDiceRoll2.BackColor = System.Drawing.Color.Transparent;
            this.PicDiceRoll2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicDiceRoll2.BackgroundImage")));
            this.PicDiceRoll2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicDiceRoll2.Location = new System.Drawing.Point(213, 70);
            this.PicDiceRoll2.Name = "PicDiceRoll2";
            this.PicDiceRoll2.Size = new System.Drawing.Size(115, 115);
            this.PicDiceRoll2.TabIndex = 68;
            this.PicDiceRoll2.TabStop = false;
            // 
            // PicDiceRoll1
            // 
            this.PicDiceRoll1.BackColor = System.Drawing.Color.Transparent;
            this.PicDiceRoll1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicDiceRoll1.BackgroundImage")));
            this.PicDiceRoll1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicDiceRoll1.Location = new System.Drawing.Point(56, 68);
            this.PicDiceRoll1.Name = "PicDiceRoll1";
            this.PicDiceRoll1.Size = new System.Drawing.Size(115, 115);
            this.PicDiceRoll1.TabIndex = 67;
            this.PicDiceRoll1.TabStop = false;
            // 
            // lblRollDiceToMoveTopMessage
            // 
            this.lblRollDiceToMoveTopMessage.BackColor = System.Drawing.Color.Transparent;
            this.lblRollDiceToMoveTopMessage.Font = new System.Drawing.Font("Arial Narrow", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRollDiceToMoveTopMessage.ForeColor = System.Drawing.Color.White;
            this.lblRollDiceToMoveTopMessage.Location = new System.Drawing.Point(37, 5);
            this.lblRollDiceToMoveTopMessage.Name = "lblRollDiceToMoveTopMessage";
            this.lblRollDiceToMoveTopMessage.Size = new System.Drawing.Size(313, 35);
            this.lblRollDiceToMoveTopMessage.TabIndex = 66;
            this.lblRollDiceToMoveTopMessage.Text = "Warrior1\'s Turn";
            this.lblRollDiceToMoveTopMessage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblRollDiceToMoveLowerMessage
            // 
            this.lblRollDiceToMoveLowerMessage.BackColor = System.Drawing.Color.Transparent;
            this.lblRollDiceToMoveLowerMessage.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRollDiceToMoveLowerMessage.ForeColor = System.Drawing.Color.White;
            this.lblRollDiceToMoveLowerMessage.Location = new System.Drawing.Point(52, 194);
            this.lblRollDiceToMoveLowerMessage.Name = "lblRollDiceToMoveLowerMessage";
            this.lblRollDiceToMoveLowerMessage.Size = new System.Drawing.Size(270, 20);
            this.lblRollDiceToMoveLowerMessage.TabIndex = 65;
            this.lblRollDiceToMoveLowerMessage.Text = "J - Roll Dice";
            this.lblRollDiceToMoveLowerMessage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblRollDiceMessage
            // 
            this.lblRollDiceMessage.BackColor = System.Drawing.Color.Transparent;
            this.lblRollDiceMessage.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRollDiceMessage.ForeColor = System.Drawing.Color.White;
            this.lblRollDiceMessage.Location = new System.Drawing.Point(49, 40);
            this.lblRollDiceMessage.Name = "lblRollDiceMessage";
            this.lblRollDiceMessage.Size = new System.Drawing.Size(286, 20);
            this.lblRollDiceMessage.TabIndex = 62;
            this.lblRollDiceMessage.Text = "Roll Dice!";
            this.lblRollDiceMessage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblActivePlayerTopMessage
            // 
            this.lblActivePlayerTopMessage.BackColor = System.Drawing.Color.Transparent;
            this.lblActivePlayerTopMessage.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblActivePlayerTopMessage.ForeColor = System.Drawing.Color.White;
            this.lblActivePlayerTopMessage.Location = new System.Drawing.Point(167, -1);
            this.lblActivePlayerTopMessage.Name = "lblActivePlayerTopMessage";
            this.lblActivePlayerTopMessage.Size = new System.Drawing.Size(247, 25);
            this.lblActivePlayerTopMessage.TabIndex = 63;
            this.lblActivePlayerTopMessage.Text = "Active Player:";
            this.lblActivePlayerTopMessage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblTurnOrderTop
            // 
            this.lblTurnOrderTop.BackColor = System.Drawing.Color.Transparent;
            this.lblTurnOrderTop.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTurnOrderTop.ForeColor = System.Drawing.Color.White;
            this.lblTurnOrderTop.Location = new System.Drawing.Point(411, 1);
            this.lblTurnOrderTop.Name = "lblTurnOrderTop";
            this.lblTurnOrderTop.Size = new System.Drawing.Size(722, 25);
            this.lblTurnOrderTop.TabIndex = 62;
            this.lblTurnOrderTop.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblRoundCounter
            // 
            this.lblRoundCounter.BackColor = System.Drawing.Color.Transparent;
            this.lblRoundCounter.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRoundCounter.ForeColor = System.Drawing.Color.White;
            this.lblRoundCounter.Location = new System.Drawing.Point(26, -1);
            this.lblRoundCounter.Name = "lblRoundCounter";
            this.lblRoundCounter.Size = new System.Drawing.Size(141, 20);
            this.lblRoundCounter.TabIndex = 61;
            this.lblRoundCounter.Text = "Round: 1";
            this.lblRoundCounter.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // PanelTurnOrderDecider
            // 
            this.PanelTurnOrderDecider.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PanelTurnOrderDecider.BackgroundImage")));
            this.PanelTurnOrderDecider.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PanelTurnOrderDecider.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelTurnOrderDecider.Controls.Add(this.PicTurn8);
            this.PanelTurnOrderDecider.Controls.Add(this.PicTurnArrow7);
            this.PanelTurnOrderDecider.Controls.Add(this.PicTurn7);
            this.PanelTurnOrderDecider.Controls.Add(this.PicTurnArrow3);
            this.PanelTurnOrderDecider.Controls.Add(this.PicTurn3);
            this.PanelTurnOrderDecider.Controls.Add(this.PicTurnArrow2);
            this.PanelTurnOrderDecider.Controls.Add(this.PicTurn2);
            this.PanelTurnOrderDecider.Controls.Add(this.PicTurnArrow1);
            this.PanelTurnOrderDecider.Controls.Add(this.PicTurn1);
            this.PanelTurnOrderDecider.Controls.Add(this.PicTurnArrow6);
            this.PanelTurnOrderDecider.Controls.Add(this.PicTurn6);
            this.PanelTurnOrderDecider.Controls.Add(this.PicTurnArrow5);
            this.PanelTurnOrderDecider.Controls.Add(this.PicTurn5);
            this.PanelTurnOrderDecider.Controls.Add(this.PicTurnArrow4);
            this.PanelTurnOrderDecider.Controls.Add(this.PicTurn4);
            this.PanelTurnOrderDecider.Controls.Add(this.lblTurnDeciderMessage);
            this.PanelTurnOrderDecider.Controls.Add(this.label6);
            this.PanelTurnOrderDecider.Location = new System.Drawing.Point(381, 878);
            this.PanelTurnOrderDecider.Name = "PanelTurnOrderDecider";
            this.PanelTurnOrderDecider.Size = new System.Drawing.Size(415, 135);
            this.PanelTurnOrderDecider.TabIndex = 1;
            this.PanelTurnOrderDecider.Visible = false;
            // 
            // PicTurn8
            // 
            this.PicTurn8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicTurn8.BackgroundImage")));
            this.PicTurn8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicTurn8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicTurn8.Location = new System.Drawing.Point(367, 39);
            this.PicTurn8.Name = "PicTurn8";
            this.PicTurn8.Size = new System.Drawing.Size(35, 45);
            this.PicTurn8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicTurn8.TabIndex = 74;
            this.PicTurn8.TabStop = false;
            this.PicTurn8.Visible = false;
            // 
            // PicTurnArrow7
            // 
            this.PicTurnArrow7.BackColor = System.Drawing.Color.Transparent;
            this.PicTurnArrow7.Image = ((System.Drawing.Image)(resources.GetObject("PicTurnArrow7.Image")));
            this.PicTurnArrow7.Location = new System.Drawing.Point(350, 39);
            this.PicTurnArrow7.Name = "PicTurnArrow7";
            this.PicTurnArrow7.Size = new System.Drawing.Size(18, 45);
            this.PicTurnArrow7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicTurnArrow7.TabIndex = 73;
            this.PicTurnArrow7.TabStop = false;
            this.PicTurnArrow7.Visible = false;
            // 
            // PicTurn7
            // 
            this.PicTurn7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicTurn7.BackgroundImage")));
            this.PicTurn7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicTurn7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicTurn7.Location = new System.Drawing.Point(316, 39);
            this.PicTurn7.Name = "PicTurn7";
            this.PicTurn7.Size = new System.Drawing.Size(35, 45);
            this.PicTurn7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicTurn7.TabIndex = 72;
            this.PicTurn7.TabStop = false;
            this.PicTurn7.Visible = false;
            // 
            // PicTurnArrow3
            // 
            this.PicTurnArrow3.BackColor = System.Drawing.Color.Transparent;
            this.PicTurnArrow3.Image = ((System.Drawing.Image)(resources.GetObject("PicTurnArrow3.Image")));
            this.PicTurnArrow3.Location = new System.Drawing.Point(144, 41);
            this.PicTurnArrow3.Name = "PicTurnArrow3";
            this.PicTurnArrow3.Size = new System.Drawing.Size(18, 45);
            this.PicTurnArrow3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicTurnArrow3.TabIndex = 71;
            this.PicTurnArrow3.TabStop = false;
            this.PicTurnArrow3.Visible = false;
            // 
            // PicTurn3
            // 
            this.PicTurn3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicTurn3.BackgroundImage")));
            this.PicTurn3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicTurn3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicTurn3.Location = new System.Drawing.Point(110, 40);
            this.PicTurn3.Name = "PicTurn3";
            this.PicTurn3.Size = new System.Drawing.Size(35, 45);
            this.PicTurn3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicTurn3.TabIndex = 70;
            this.PicTurn3.TabStop = false;
            this.PicTurn3.Visible = false;
            // 
            // PicTurnArrow2
            // 
            this.PicTurnArrow2.BackColor = System.Drawing.Color.Transparent;
            this.PicTurnArrow2.Image = ((System.Drawing.Image)(resources.GetObject("PicTurnArrow2.Image")));
            this.PicTurnArrow2.Location = new System.Drawing.Point(93, 41);
            this.PicTurnArrow2.Name = "PicTurnArrow2";
            this.PicTurnArrow2.Size = new System.Drawing.Size(18, 45);
            this.PicTurnArrow2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicTurnArrow2.TabIndex = 69;
            this.PicTurnArrow2.TabStop = false;
            this.PicTurnArrow2.Visible = false;
            // 
            // PicTurn2
            // 
            this.PicTurn2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicTurn2.BackgroundImage")));
            this.PicTurn2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicTurn2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicTurn2.Location = new System.Drawing.Point(59, 40);
            this.PicTurn2.Name = "PicTurn2";
            this.PicTurn2.Size = new System.Drawing.Size(35, 45);
            this.PicTurn2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicTurn2.TabIndex = 68;
            this.PicTurn2.TabStop = false;
            this.PicTurn2.Visible = false;
            // 
            // PicTurnArrow1
            // 
            this.PicTurnArrow1.BackColor = System.Drawing.Color.Transparent;
            this.PicTurnArrow1.Image = ((System.Drawing.Image)(resources.GetObject("PicTurnArrow1.Image")));
            this.PicTurnArrow1.Location = new System.Drawing.Point(41, 41);
            this.PicTurnArrow1.Name = "PicTurnArrow1";
            this.PicTurnArrow1.Size = new System.Drawing.Size(18, 45);
            this.PicTurnArrow1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicTurnArrow1.TabIndex = 67;
            this.PicTurnArrow1.TabStop = false;
            this.PicTurnArrow1.Visible = false;
            // 
            // PicTurn1
            // 
            this.PicTurn1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicTurn1.BackgroundImage")));
            this.PicTurn1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicTurn1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicTurn1.Location = new System.Drawing.Point(7, 40);
            this.PicTurn1.Name = "PicTurn1";
            this.PicTurn1.Size = new System.Drawing.Size(35, 45);
            this.PicTurn1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicTurn1.TabIndex = 66;
            this.PicTurn1.TabStop = false;
            this.PicTurn1.Visible = false;
            // 
            // PicTurnArrow6
            // 
            this.PicTurnArrow6.BackColor = System.Drawing.Color.Transparent;
            this.PicTurnArrow6.Image = ((System.Drawing.Image)(resources.GetObject("PicTurnArrow6.Image")));
            this.PicTurnArrow6.Location = new System.Drawing.Point(299, 40);
            this.PicTurnArrow6.Name = "PicTurnArrow6";
            this.PicTurnArrow6.Size = new System.Drawing.Size(18, 45);
            this.PicTurnArrow6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicTurnArrow6.TabIndex = 65;
            this.PicTurnArrow6.TabStop = false;
            this.PicTurnArrow6.Visible = false;
            // 
            // PicTurn6
            // 
            this.PicTurn6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicTurn6.BackgroundImage")));
            this.PicTurn6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicTurn6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicTurn6.Location = new System.Drawing.Point(265, 40);
            this.PicTurn6.Name = "PicTurn6";
            this.PicTurn6.Size = new System.Drawing.Size(35, 45);
            this.PicTurn6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicTurn6.TabIndex = 64;
            this.PicTurn6.TabStop = false;
            this.PicTurn6.Visible = false;
            // 
            // PicTurnArrow5
            // 
            this.PicTurnArrow5.BackColor = System.Drawing.Color.Transparent;
            this.PicTurnArrow5.Image = ((System.Drawing.Image)(resources.GetObject("PicTurnArrow5.Image")));
            this.PicTurnArrow5.Location = new System.Drawing.Point(248, 40);
            this.PicTurnArrow5.Name = "PicTurnArrow5";
            this.PicTurnArrow5.Size = new System.Drawing.Size(18, 45);
            this.PicTurnArrow5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicTurnArrow5.TabIndex = 63;
            this.PicTurnArrow5.TabStop = false;
            this.PicTurnArrow5.Visible = false;
            // 
            // PicTurn5
            // 
            this.PicTurn5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicTurn5.BackgroundImage")));
            this.PicTurn5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicTurn5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicTurn5.Location = new System.Drawing.Point(214, 39);
            this.PicTurn5.Name = "PicTurn5";
            this.PicTurn5.Size = new System.Drawing.Size(35, 45);
            this.PicTurn5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicTurn5.TabIndex = 62;
            this.PicTurn5.TabStop = false;
            this.PicTurn5.Visible = false;
            // 
            // PicTurnArrow4
            // 
            this.PicTurnArrow4.BackColor = System.Drawing.Color.Transparent;
            this.PicTurnArrow4.Image = ((System.Drawing.Image)(resources.GetObject("PicTurnArrow4.Image")));
            this.PicTurnArrow4.Location = new System.Drawing.Point(196, 40);
            this.PicTurnArrow4.Name = "PicTurnArrow4";
            this.PicTurnArrow4.Size = new System.Drawing.Size(18, 45);
            this.PicTurnArrow4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicTurnArrow4.TabIndex = 61;
            this.PicTurnArrow4.TabStop = false;
            this.PicTurnArrow4.Visible = false;
            // 
            // PicTurn4
            // 
            this.PicTurn4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicTurn4.BackgroundImage")));
            this.PicTurn4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicTurn4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicTurn4.Location = new System.Drawing.Point(162, 40);
            this.PicTurn4.Name = "PicTurn4";
            this.PicTurn4.Size = new System.Drawing.Size(35, 45);
            this.PicTurn4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicTurn4.TabIndex = 50;
            this.PicTurn4.TabStop = false;
            this.PicTurn4.Visible = false;
            // 
            // lblTurnDeciderMessage
            // 
            this.lblTurnDeciderMessage.BackColor = System.Drawing.Color.Transparent;
            this.lblTurnDeciderMessage.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTurnDeciderMessage.ForeColor = System.Drawing.Color.White;
            this.lblTurnDeciderMessage.Location = new System.Drawing.Point(46, 91);
            this.lblTurnDeciderMessage.Name = "lblTurnDeciderMessage";
            this.lblTurnDeciderMessage.Size = new System.Drawing.Size(325, 25);
            this.lblTurnDeciderMessage.TabIndex = 2;
            this.lblTurnDeciderMessage.Text = "Press J to suffle";
            this.lblTurnDeciderMessage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Black;
            this.label6.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(47, 10);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(325, 25);
            this.label6.TabIndex = 1;
            this.label6.Text = "Turn Order";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // PicWarrior2
            // 
            this.PicWarrior2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicWarrior2.BackgroundImage")));
            this.PicWarrior2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicWarrior2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicWarrior2.Location = new System.Drawing.Point(596, 472);
            this.PicWarrior2.Name = "PicWarrior2";
            this.PicWarrior2.Size = new System.Drawing.Size(35, 45);
            this.PicWarrior2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicWarrior2.TabIndex = 56;
            this.PicWarrior2.TabStop = false;
            this.PicWarrior2.Visible = false;
            // 
            // PicElf2
            // 
            this.PicElf2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicElf2.BackgroundImage")));
            this.PicElf2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicElf2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicElf2.Location = new System.Drawing.Point(546, 472);
            this.PicElf2.Name = "PicElf2";
            this.PicElf2.Size = new System.Drawing.Size(35, 45);
            this.PicElf2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicElf2.TabIndex = 55;
            this.PicElf2.TabStop = false;
            this.PicElf2.Visible = false;
            // 
            // PicDwarf2
            // 
            this.PicDwarf2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicDwarf2.BackgroundImage")));
            this.PicDwarf2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicDwarf2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicDwarf2.Location = new System.Drawing.Point(497, 472);
            this.PicDwarf2.Name = "PicDwarf2";
            this.PicDwarf2.Size = new System.Drawing.Size(35, 45);
            this.PicDwarf2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicDwarf2.TabIndex = 54;
            this.PicDwarf2.TabStop = false;
            this.PicDwarf2.Visible = false;
            // 
            // PicMage2
            // 
            this.PicMage2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicMage2.BackgroundImage")));
            this.PicMage2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicMage2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicMage2.Location = new System.Drawing.Point(596, 422);
            this.PicMage2.Name = "PicMage2";
            this.PicMage2.Size = new System.Drawing.Size(35, 45);
            this.PicMage2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicMage2.TabIndex = 53;
            this.PicMage2.TabStop = false;
            this.PicMage2.Visible = false;
            // 
            // PicMage1
            // 
            this.PicMage1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicMage1.BackgroundImage")));
            this.PicMage1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicMage1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicMage1.Location = new System.Drawing.Point(497, 422);
            this.PicMage1.Name = "PicMage1";
            this.PicMage1.Size = new System.Drawing.Size(35, 45);
            this.PicMage1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicMage1.TabIndex = 52;
            this.PicMage1.TabStop = false;
            this.PicMage1.Visible = false;
            // 
            // PicDwarf1
            // 
            this.PicDwarf1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicDwarf1.BackgroundImage")));
            this.PicDwarf1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicDwarf1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicDwarf1.Location = new System.Drawing.Point(596, 371);
            this.PicDwarf1.Name = "PicDwarf1";
            this.PicDwarf1.Size = new System.Drawing.Size(35, 45);
            this.PicDwarf1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicDwarf1.TabIndex = 51;
            this.PicDwarf1.TabStop = false;
            this.PicDwarf1.Visible = false;
            // 
            // PicElf1
            // 
            this.PicElf1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicElf1.BackgroundImage")));
            this.PicElf1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicElf1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicElf1.Location = new System.Drawing.Point(546, 370);
            this.PicElf1.Name = "PicElf1";
            this.PicElf1.Size = new System.Drawing.Size(35, 45);
            this.PicElf1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicElf1.TabIndex = 50;
            this.PicElf1.TabStop = false;
            this.PicElf1.Visible = false;
            // 
            // PicWarrior1
            // 
            this.PicWarrior1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicWarrior1.BackgroundImage")));
            this.PicWarrior1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicWarrior1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicWarrior1.Location = new System.Drawing.Point(497, 370);
            this.PicWarrior1.Name = "PicWarrior1";
            this.PicWarrior1.Size = new System.Drawing.Size(35, 45);
            this.PicWarrior1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicWarrior1.TabIndex = 49;
            this.PicWarrior1.TabStop = false;
            this.PicWarrior1.Visible = false;
            // 
            // PicCell9
            // 
            this.PicCell9.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell9.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell9.BackgroundImage")));
            this.PicCell9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell9.Location = new System.Drawing.Point(731, 96);
            this.PicCell9.Name = "PicCell9";
            this.PicCell9.Size = new System.Drawing.Size(55, 70);
            this.PicCell9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicCell9.TabIndex = 48;
            this.PicCell9.TabStop = false;
            // 
            // PicCell40
            // 
            this.PicCell40.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell40.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell40.BackgroundImage")));
            this.PicCell40.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell40.Location = new System.Drawing.Point(673, 787);
            this.PicCell40.Name = "PicCell40";
            this.PicCell40.Size = new System.Drawing.Size(70, 55);
            this.PicCell40.TabIndex = 47;
            this.PicCell40.TabStop = false;
            // 
            // PicCell36
            // 
            this.PicCell36.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell36.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell36.BackgroundImage")));
            this.PicCell36.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell36.Location = new System.Drawing.Point(241, 699);
            this.PicCell36.Name = "PicCell36";
            this.PicCell36.Size = new System.Drawing.Size(70, 55);
            this.PicCell36.TabIndex = 46;
            this.PicCell36.TabStop = false;
            // 
            // PicCell34
            // 
            this.PicCell34.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell34.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell34.BackgroundImage")));
            this.PicCell34.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell34.Location = new System.Drawing.Point(215, 628);
            this.PicCell34.Name = "PicCell34";
            this.PicCell34.Size = new System.Drawing.Size(70, 55);
            this.PicCell34.TabIndex = 45;
            this.PicCell34.TabStop = false;
            // 
            // PicCell28
            // 
            this.PicCell28.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell28.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell28.BackgroundImage")));
            this.PicCell28.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell28.Location = new System.Drawing.Point(46, 569);
            this.PicCell28.Name = "PicCell28";
            this.PicCell28.Size = new System.Drawing.Size(70, 55);
            this.PicCell28.TabIndex = 44;
            this.PicCell28.TabStop = false;
            // 
            // PicCell27
            // 
            this.PicCell27.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell27.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell27.BackgroundImage")));
            this.PicCell27.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell27.Location = new System.Drawing.Point(954, 538);
            this.PicCell27.Name = "PicCell27";
            this.PicCell27.Size = new System.Drawing.Size(70, 55);
            this.PicCell27.TabIndex = 43;
            this.PicCell27.TabStop = false;
            // 
            // PicCell26
            // 
            this.PicCell26.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell26.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell26.BackgroundImage")));
            this.PicCell26.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell26.Location = new System.Drawing.Point(614, 529);
            this.PicCell26.Name = "PicCell26";
            this.PicCell26.Size = new System.Drawing.Size(70, 55);
            this.PicCell26.TabIndex = 42;
            this.PicCell26.TabStop = false;
            // 
            // PicCell23
            // 
            this.PicCell23.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell23.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell23.BackgroundImage")));
            this.PicCell23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell23.Location = new System.Drawing.Point(408, 462);
            this.PicCell23.Name = "PicCell23";
            this.PicCell23.Size = new System.Drawing.Size(70, 55);
            this.PicCell23.TabIndex = 41;
            this.PicCell23.TabStop = false;
            // 
            // PicCell22
            // 
            this.PicCell22.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell22.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell22.BackgroundImage")));
            this.PicCell22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell22.Location = new System.Drawing.Point(207, 462);
            this.PicCell22.Name = "PicCell22";
            this.PicCell22.Size = new System.Drawing.Size(70, 55);
            this.PicCell22.TabIndex = 40;
            this.PicCell22.TabStop = false;
            // 
            // PicCell21
            // 
            this.PicCell21.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell21.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell21.BackgroundImage")));
            this.PicCell21.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell21.Location = new System.Drawing.Point(1021, 369);
            this.PicCell21.Name = "PicCell21";
            this.PicCell21.Size = new System.Drawing.Size(70, 55);
            this.PicCell21.TabIndex = 39;
            this.PicCell21.TabStop = false;
            // 
            // PicCell20
            // 
            this.PicCell20.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell20.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell20.BackgroundImage")));
            this.PicCell20.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell20.Location = new System.Drawing.Point(856, 410);
            this.PicCell20.Name = "PicCell20";
            this.PicCell20.Size = new System.Drawing.Size(70, 55);
            this.PicCell20.TabIndex = 38;
            this.PicCell20.TabStop = false;
            // 
            // PicCell13
            // 
            this.PicCell13.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell13.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell13.BackgroundImage")));
            this.PicCell13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell13.Location = new System.Drawing.Point(1023, 197);
            this.PicCell13.Name = "PicCell13";
            this.PicCell13.Size = new System.Drawing.Size(70, 55);
            this.PicCell13.TabIndex = 37;
            this.PicCell13.TabStop = false;
            // 
            // PicCell16
            // 
            this.PicCell16.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell16.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell16.BackgroundImage")));
            this.PicCell16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell16.Location = new System.Drawing.Point(835, 251);
            this.PicCell16.Name = "PicCell16";
            this.PicCell16.Size = new System.Drawing.Size(70, 55);
            this.PicCell16.TabIndex = 36;
            this.PicCell16.TabStop = false;
            // 
            // PicCell10
            // 
            this.PicCell10.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell10.BackgroundImage")));
            this.PicCell10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell10.Location = new System.Drawing.Point(803, 120);
            this.PicCell10.Name = "PicCell10";
            this.PicCell10.Size = new System.Drawing.Size(70, 55);
            this.PicCell10.TabIndex = 35;
            this.PicCell10.TabStop = false;
            // 
            // PicCell8
            // 
            this.PicCell8.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell8.BackgroundImage")));
            this.PicCell8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell8.Location = new System.Drawing.Point(634, 89);
            this.PicCell8.Name = "PicCell8";
            this.PicCell8.Size = new System.Drawing.Size(70, 55);
            this.PicCell8.TabIndex = 34;
            this.PicCell8.TabStop = false;
            // 
            // PicCell7
            // 
            this.PicCell7.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell7.BackgroundImage")));
            this.PicCell7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell7.Location = new System.Drawing.Point(511, 136);
            this.PicCell7.Name = "PicCell7";
            this.PicCell7.Size = new System.Drawing.Size(70, 55);
            this.PicCell7.TabIndex = 33;
            this.PicCell7.TabStop = false;
            // 
            // PicCell5
            // 
            this.PicCell5.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell5.BackgroundImage")));
            this.PicCell5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell5.Location = new System.Drawing.Point(185, 135);
            this.PicCell5.Name = "PicCell5";
            this.PicCell5.Size = new System.Drawing.Size(70, 55);
            this.PicCell5.TabIndex = 32;
            this.PicCell5.TabStop = false;
            // 
            // PicCell4
            // 
            this.PicCell4.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell4.BackgroundImage")));
            this.PicCell4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell4.Location = new System.Drawing.Point(104, 135);
            this.PicCell4.Name = "PicCell4";
            this.PicCell4.Size = new System.Drawing.Size(70, 55);
            this.PicCell4.TabIndex = 31;
            this.PicCell4.TabStop = false;
            // 
            // PicCell3
            // 
            this.PicCell3.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell3.BackgroundImage")));
            this.PicCell3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell3.Location = new System.Drawing.Point(858, 36);
            this.PicCell3.Name = "PicCell3";
            this.PicCell3.Size = new System.Drawing.Size(70, 55);
            this.PicCell3.TabIndex = 30;
            this.PicCell3.TabStop = false;
            // 
            // PicCell41
            // 
            this.PicCell41.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell41.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell41.BackgroundImage")));
            this.PicCell41.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell41.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell41.Location = new System.Drawing.Point(814, 772);
            this.PicCell41.Name = "PicCell41";
            this.PicCell41.Size = new System.Drawing.Size(55, 70);
            this.PicCell41.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicCell41.TabIndex = 29;
            this.PicCell41.TabStop = false;
            // 
            // PicCell399
            // 
            this.PicCell399.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell399.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell399.BackgroundImage")));
            this.PicCell399.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell399.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell399.Location = new System.Drawing.Point(1033, 663);
            this.PicCell399.Name = "PicCell399";
            this.PicCell399.Size = new System.Drawing.Size(55, 70);
            this.PicCell399.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicCell399.TabIndex = 28;
            this.PicCell399.TabStop = false;
            // 
            // PicCell38
            // 
            this.PicCell38.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell38.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell38.BackgroundImage")));
            this.PicCell38.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell38.Location = new System.Drawing.Point(379, 704);
            this.PicCell38.Name = "PicCell38";
            this.PicCell38.Size = new System.Drawing.Size(55, 70);
            this.PicCell38.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicCell38.TabIndex = 27;
            this.PicCell38.TabStop = false;
            // 
            // PicCell37
            // 
            this.PicCell37.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell37.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell37.BackgroundImage")));
            this.PicCell37.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell37.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell37.Location = new System.Drawing.Point(255, 769);
            this.PicCell37.Name = "PicCell37";
            this.PicCell37.Size = new System.Drawing.Size(55, 70);
            this.PicCell37.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicCell37.TabIndex = 26;
            this.PicCell37.TabStop = false;
            // 
            // PicCell333
            // 
            this.PicCell333.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell333.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell333.BackgroundImage")));
            this.PicCell333.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell333.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell333.Location = new System.Drawing.Point(866, 582);
            this.PicCell333.Name = "PicCell333";
            this.PicCell333.Size = new System.Drawing.Size(55, 70);
            this.PicCell333.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicCell333.TabIndex = 25;
            this.PicCell333.TabStop = false;
            // 
            // PicCell32
            // 
            this.PicCell32.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell32.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell32.BackgroundImage")));
            this.PicCell32.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell32.Location = new System.Drawing.Point(740, 578);
            this.PicCell32.Name = "PicCell32";
            this.PicCell32.Size = new System.Drawing.Size(55, 70);
            this.PicCell32.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicCell32.TabIndex = 24;
            this.PicCell32.TabStop = false;
            // 
            // PicCell31
            // 
            this.PicCell31.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell31.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell31.BackgroundImage")));
            this.PicCell31.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell31.Location = new System.Drawing.Point(672, 601);
            this.PicCell31.Name = "PicCell31";
            this.PicCell31.Size = new System.Drawing.Size(55, 70);
            this.PicCell31.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicCell31.TabIndex = 23;
            this.PicCell31.TabStop = false;
            // 
            // PicCell30
            // 
            this.PicCell30.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell30.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell30.BackgroundImage")));
            this.PicCell30.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell30.Location = new System.Drawing.Point(608, 602);
            this.PicCell30.Name = "PicCell30";
            this.PicCell30.Size = new System.Drawing.Size(55, 70);
            this.PicCell30.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicCell30.TabIndex = 22;
            this.PicCell30.TabStop = false;
            // 
            // PicCell35
            // 
            this.PicCell35.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell35.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell35.BackgroundImage")));
            this.PicCell35.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell35.Location = new System.Drawing.Point(483, 597);
            this.PicCell35.Name = "PicCell35";
            this.PicCell35.Size = new System.Drawing.Size(55, 70);
            this.PicCell35.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicCell35.TabIndex = 21;
            this.PicCell35.TabStop = false;
            // 
            // PicCell25
            // 
            this.PicCell25.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell25.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell25.BackgroundImage")));
            this.PicCell25.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell25.Location = new System.Drawing.Point(870, 487);
            this.PicCell25.Name = "PicCell25";
            this.PicCell25.Size = new System.Drawing.Size(55, 70);
            this.PicCell25.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicCell25.TabIndex = 20;
            this.PicCell25.TabStop = false;
            // 
            // PicCell24
            // 
            this.PicCell24.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell24.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell24.BackgroundImage")));
            this.PicCell24.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell24.Location = new System.Drawing.Point(741, 488);
            this.PicCell24.Name = "PicCell24";
            this.PicCell24.Size = new System.Drawing.Size(55, 70);
            this.PicCell24.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicCell24.TabIndex = 19;
            this.PicCell24.TabStop = false;
            // 
            // PicCell29
            // 
            this.PicCell29.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell29.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell29.BackgroundImage")));
            this.PicCell29.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell29.Location = new System.Drawing.Point(311, 547);
            this.PicCell29.Name = "PicCell29";
            this.PicCell29.Size = new System.Drawing.Size(55, 70);
            this.PicCell29.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicCell29.TabIndex = 18;
            this.PicCell29.TabStop = false;
            // 
            // PicCell19
            // 
            this.PicCell19.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell19.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell19.BackgroundImage")));
            this.PicCell19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell19.Location = new System.Drawing.Point(152, 374);
            this.PicCell19.Name = "PicCell19";
            this.PicCell19.Size = new System.Drawing.Size(55, 70);
            this.PicCell19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicCell19.TabIndex = 17;
            this.PicCell19.TabStop = false;
            // 
            // PicCell18
            // 
            this.PicCell18.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell18.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell18.BackgroundImage")));
            this.PicCell18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell18.Location = new System.Drawing.Point(372, 303);
            this.PicCell18.Name = "PicCell18";
            this.PicCell18.Size = new System.Drawing.Size(55, 70);
            this.PicCell18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicCell18.TabIndex = 16;
            this.PicCell18.TabStop = false;
            // 
            // PicCell17
            // 
            this.PicCell17.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell17.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell17.BackgroundImage")));
            this.PicCell17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell17.Location = new System.Drawing.Point(46, 307);
            this.PicCell17.Name = "PicCell17";
            this.PicCell17.Size = new System.Drawing.Size(55, 70);
            this.PicCell17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicCell17.TabIndex = 15;
            this.PicCell17.TabStop = false;
            // 
            // PicCell15
            // 
            this.PicCell15.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell15.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell15.BackgroundImage")));
            this.PicCell15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell15.Location = new System.Drawing.Point(763, 234);
            this.PicCell15.Name = "PicCell15";
            this.PicCell15.Size = new System.Drawing.Size(55, 70);
            this.PicCell15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicCell15.TabIndex = 14;
            this.PicCell15.TabStop = false;
            // 
            // PicCell14
            // 
            this.PicCell14.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell14.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell14.BackgroundImage")));
            this.PicCell14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell14.Location = new System.Drawing.Point(700, 234);
            this.PicCell14.Name = "PicCell14";
            this.PicCell14.Size = new System.Drawing.Size(55, 70);
            this.PicCell14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicCell14.TabIndex = 13;
            this.PicCell14.TabStop = false;
            // 
            // PicCell12
            // 
            this.PicCell12.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell12.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell12.BackgroundImage")));
            this.PicCell12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell12.Location = new System.Drawing.Point(308, 205);
            this.PicCell12.Name = "PicCell12";
            this.PicCell12.Size = new System.Drawing.Size(55, 70);
            this.PicCell12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicCell12.TabIndex = 12;
            this.PicCell12.TabStop = false;
            // 
            // PicCell11
            // 
            this.PicCell11.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell11.BackgroundImage")));
            this.PicCell11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell11.Location = new System.Drawing.Point(927, 107);
            this.PicCell11.Name = "PicCell11";
            this.PicCell11.Size = new System.Drawing.Size(55, 70);
            this.PicCell11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicCell11.TabIndex = 11;
            this.PicCell11.TabStop = false;
            // 
            // PicCell6
            // 
            this.PicCell6.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell6.BackgroundImage")));
            this.PicCell6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell6.Location = new System.Drawing.Point(436, 119);
            this.PicCell6.Name = "PicCell6";
            this.PicCell6.Size = new System.Drawing.Size(55, 70);
            this.PicCell6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicCell6.TabIndex = 10;
            this.PicCell6.TabStop = false;
            // 
            // PicCell2
            // 
            this.PicCell2.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell2.BackgroundImage")));
            this.PicCell2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell2.Location = new System.Drawing.Point(793, 36);
            this.PicCell2.Name = "PicCell2";
            this.PicCell2.Size = new System.Drawing.Size(55, 70);
            this.PicCell2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicCell2.TabIndex = 9;
            this.PicCell2.TabStop = false;
            // 
            // PicCell0
            // 
            this.PicCell0.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell0.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell0.BackgroundImage")));
            this.PicCell0.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell0.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell0.Location = new System.Drawing.Point(267, 32);
            this.PicCell0.Name = "PicCell0";
            this.PicCell0.Size = new System.Drawing.Size(55, 70);
            this.PicCell0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicCell0.TabIndex = 8;
            this.PicCell0.TabStop = false;
            // 
            // PicTower6
            // 
            this.PicTower6.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicTower6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicTower6.BackgroundImage")));
            this.PicTower6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicTower6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicTower6.Location = new System.Drawing.Point(1022, 769);
            this.PicTower6.Name = "PicTower6";
            this.PicTower6.Size = new System.Drawing.Size(55, 70);
            this.PicTower6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicTower6.TabIndex = 7;
            this.PicTower6.TabStop = false;
            // 
            // PicTower5
            // 
            this.PicTower5.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicTower5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicTower5.BackgroundImage")));
            this.PicTower5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicTower5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicTower5.Location = new System.Drawing.Point(521, 765);
            this.PicTower5.Name = "PicTower5";
            this.PicTower5.Size = new System.Drawing.Size(55, 70);
            this.PicTower5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicTower5.TabIndex = 6;
            this.PicTower5.TabStop = false;
            // 
            // PicTower4
            // 
            this.PicTower4.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicTower4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicTower4.BackgroundImage")));
            this.PicTower4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicTower4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicTower4.Location = new System.Drawing.Point(48, 763);
            this.PicTower4.Name = "PicTower4";
            this.PicTower4.Size = new System.Drawing.Size(55, 70);
            this.PicTower4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicTower4.TabIndex = 5;
            this.PicTower4.TabStop = false;
            // 
            // PicTower3
            // 
            this.PicTower3.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicTower3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicTower3.BackgroundImage")));
            this.PicTower3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicTower3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicTower3.Location = new System.Drawing.Point(1025, 38);
            this.PicTower3.Name = "PicTower3";
            this.PicTower3.Size = new System.Drawing.Size(55, 70);
            this.PicTower3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicTower3.TabIndex = 4;
            this.PicTower3.TabStop = false;
            // 
            // PicTower2
            // 
            this.PicTower2.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicTower2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicTower2.BackgroundImage")));
            this.PicTower2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicTower2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicTower2.Location = new System.Drawing.Point(541, 35);
            this.PicTower2.Name = "PicTower2";
            this.PicTower2.Size = new System.Drawing.Size(55, 70);
            this.PicTower2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicTower2.TabIndex = 3;
            this.PicTower2.TabStop = false;
            // 
            // PicTower1
            // 
            this.PicTower1.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicTower1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicTower1.BackgroundImage")));
            this.PicTower1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicTower1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicTower1.Location = new System.Drawing.Point(58, 32);
            this.PicTower1.Name = "PicTower1";
            this.PicTower1.Size = new System.Drawing.Size(55, 70);
            this.PicTower1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicTower1.TabIndex = 2;
            this.PicTower1.TabStop = false;
            // 
            // PicCell1
            // 
            this.PicCell1.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell1.BackgroundImage")));
            this.PicCell1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell1.Location = new System.Drawing.Point(386, 31);
            this.PicCell1.Name = "PicCell1";
            this.PicCell1.Size = new System.Drawing.Size(70, 55);
            this.PicCell1.TabIndex = 1;
            this.PicCell1.TabStop = false;
            // 
            // lblDebugOutput
            // 
            this.lblDebugOutput.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblDebugOutput.Location = new System.Drawing.Point(2, 753);
            this.lblDebugOutput.Name = "lblDebugOutput";
            this.lblDebugOutput.Size = new System.Drawing.Size(129, 106);
            this.lblDebugOutput.TabIndex = 1;
            // 
            // PicCell39
            // 
            this.PicCell39.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell39.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell39.BackgroundImage")));
            this.PicCell39.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell39.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell39.Location = new System.Drawing.Point(215, 630);
            this.PicCell39.Name = "PicCell39";
            this.PicCell39.Size = new System.Drawing.Size(70, 55);
            this.PicCell39.TabIndex = 45;
            this.PicCell39.TabStop = false;
            // 
            // PicCell33
            // 
            this.PicCell33.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.PicCell33.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicCell33.BackgroundImage")));
            this.PicCell33.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicCell33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicCell33.Location = new System.Drawing.Point(1021, 370);
            this.PicCell33.Name = "PicCell33";
            this.PicCell33.Size = new System.Drawing.Size(70, 55);
            this.PicCell33.TabIndex = 39;
            this.PicCell33.TabStop = false;
            // 
            // PanelPlayerDead
            // 
            this.PanelPlayerDead.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PanelPlayerDead.BackgroundImage")));
            this.PanelPlayerDead.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PanelPlayerDead.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelPlayerDead.Controls.Add(this.lblPlayerDeadUpperMessage);
            this.PanelPlayerDead.Controls.Add(this.lblPlayerDeadLowerMessage);
            this.PanelPlayerDead.Controls.Add(this.label15);
            this.PanelPlayerDead.Controls.Add(this.panel2);
            this.PanelPlayerDead.Location = new System.Drawing.Point(714, 906);
            this.PanelPlayerDead.Name = "PanelPlayerDead";
            this.PanelPlayerDead.Size = new System.Drawing.Size(482, 229);
            this.PanelPlayerDead.TabIndex = 70;
            this.PanelPlayerDead.Visible = false;
            // 
            // lblPlayerDeadUpperMessage
            // 
            this.lblPlayerDeadUpperMessage.BackColor = System.Drawing.Color.Transparent;
            this.lblPlayerDeadUpperMessage.Font = new System.Drawing.Font("Arial Narrow", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayerDeadUpperMessage.ForeColor = System.Drawing.Color.White;
            this.lblPlayerDeadUpperMessage.Location = new System.Drawing.Point(72, 5);
            this.lblPlayerDeadUpperMessage.Name = "lblPlayerDeadUpperMessage";
            this.lblPlayerDeadUpperMessage.Size = new System.Drawing.Size(335, 35);
            this.lblPlayerDeadUpperMessage.TabIndex = 66;
            this.lblPlayerDeadUpperMessage.Text = "Warrior1\'s Turn";
            this.lblPlayerDeadUpperMessage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblPlayerDeadLowerMessage
            // 
            this.lblPlayerDeadLowerMessage.BackColor = System.Drawing.Color.Transparent;
            this.lblPlayerDeadLowerMessage.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayerDeadLowerMessage.ForeColor = System.Drawing.Color.White;
            this.lblPlayerDeadLowerMessage.Location = new System.Drawing.Point(61, 194);
            this.lblPlayerDeadLowerMessage.Name = "lblPlayerDeadLowerMessage";
            this.lblPlayerDeadLowerMessage.Size = new System.Drawing.Size(351, 30);
            this.lblPlayerDeadLowerMessage.TabIndex = 65;
            this.lblPlayerDeadLowerMessage.Text = "Press J to Continue";
            this.lblPlayerDeadLowerMessage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(30, 40);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(413, 30);
            this.label15.TabIndex = 64;
            this.label15.Text = "Player cannot revive, it is out of the game";
            this.label15.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DarkGray;
            this.panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel2.BackgroundImage")));
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Location = new System.Drawing.Point(180, 71);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(121, 121);
            this.panel2.TabIndex = 63;
            // 
            // PanelUseElixir
            // 
            this.PanelUseElixir.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PanelUseElixir.BackgroundImage")));
            this.PanelUseElixir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PanelUseElixir.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelUseElixir.Controls.Add(this.lblUseElixir);
            this.PanelUseElixir.Controls.Add(this.lblUseElixirLowerMessage);
            this.PanelUseElixir.Controls.Add(this.label16);
            this.PanelUseElixir.Controls.Add(this.panel4);
            this.PanelUseElixir.Location = new System.Drawing.Point(860, 957);
            this.PanelUseElixir.Name = "PanelUseElixir";
            this.PanelUseElixir.Size = new System.Drawing.Size(482, 229);
            this.PanelUseElixir.TabIndex = 71;
            this.PanelUseElixir.Visible = false;
            // 
            // lblUseElixir
            // 
            this.lblUseElixir.BackColor = System.Drawing.Color.Transparent;
            this.lblUseElixir.Font = new System.Drawing.Font("Arial Narrow", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUseElixir.ForeColor = System.Drawing.Color.White;
            this.lblUseElixir.Location = new System.Drawing.Point(72, 5);
            this.lblUseElixir.Name = "lblUseElixir";
            this.lblUseElixir.Size = new System.Drawing.Size(335, 35);
            this.lblUseElixir.TabIndex = 66;
            this.lblUseElixir.Text = "Warrior1\'s Turn";
            this.lblUseElixir.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblUseElixirLowerMessage
            // 
            this.lblUseElixirLowerMessage.BackColor = System.Drawing.Color.Transparent;
            this.lblUseElixirLowerMessage.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUseElixirLowerMessage.ForeColor = System.Drawing.Color.White;
            this.lblUseElixirLowerMessage.Location = new System.Drawing.Point(61, 194);
            this.lblUseElixirLowerMessage.Name = "lblUseElixirLowerMessage";
            this.lblUseElixirLowerMessage.Size = new System.Drawing.Size(351, 30);
            this.lblUseElixirLowerMessage.TabIndex = 65;
            this.lblUseElixirLowerMessage.Text = " J - Confirm Use";
            this.lblUseElixirLowerMessage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(97, 40);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(299, 30);
            this.label16.TabIndex = 64;
            this.label16.Text = "Use an Elixir";
            this.label16.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel4.BackgroundImage")));
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Location = new System.Drawing.Point(194, 71);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(105, 121);
            this.panel4.TabIndex = 63;
            // 
            // MainBoard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1402, 870);
            this.Controls.Add(this.PanelUseElixir);
            this.Controls.Add(this.PanelPlayerDead);
            this.Controls.Add(this.lblDebugOutput);
            this.Controls.Add(this.PanelTable);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainBoard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Calabozo: La Aventura de los Anillos";
            this.PanelTable.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PicTransportTager)).EndInit();
            this.PanelPlayerAction.ResumeLayout(false);
            this.PanelBattleWindow.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PicBattleTreasure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBattleDice2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBattleDice1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBattlePlayer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBattleMonster)).EndInit();
            this.PanelTreasureSelection.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTreasureSelect5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTreasureSelect4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTreasureSelect3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTreasureSelect2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTreasureSelectRArrow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTreasureSelectLArrow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTreasureSelect1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicMonsterPreviewWest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicMonsterPreviewEast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicMonsterPreviewSouth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicMonsterPreviewNorth)).EndInit();
            this.PanelWinner.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PicWinner)).EndInit();
            this.PanelTurnLost.ResumeLayout(false);
            this.PanelPlayerSelector.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.PanelOpenLockedDoorRoll.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PicLockedDoorRoll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicRightArrow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicLeftArrow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicDownArrow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicUpArrow)).EndInit();
            this.PanelRollDiceToMove.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PicDiceRoll2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicDiceRoll1)).EndInit();
            this.PanelTurnOrderDecider.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PicTurn8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTurnArrow7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTurn7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTurnArrow3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTurn3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTurnArrow2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTurn2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTurnArrow1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTurn1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTurnArrow6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTurn6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTurnArrow5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTurn5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTurnArrow4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTurn4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicWarrior2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicElf2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicDwarf2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicMage2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicMage1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicDwarf1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicElf1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicWarrior1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell399)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell333)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTower6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTower5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTower4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTower3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTower2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicTower1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCell33)).EndInit();
            this.PanelPlayerDead.ResumeLayout(false);
            this.PanelUseElixir.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelTable;
        private System.Windows.Forms.PictureBox PicTower1;
        private System.Windows.Forms.PictureBox PicCell1;
        private System.Windows.Forms.PictureBox PicCell399;
        private System.Windows.Forms.PictureBox PicCell38;
        private System.Windows.Forms.PictureBox PicCell37;
        private System.Windows.Forms.PictureBox PicCell333;
        private System.Windows.Forms.PictureBox PicCell32;
        private System.Windows.Forms.PictureBox PicCell31;
        private System.Windows.Forms.PictureBox PicCell30;
        private System.Windows.Forms.PictureBox PicCell35;
        private System.Windows.Forms.PictureBox PicCell25;
        private System.Windows.Forms.PictureBox PicCell24;
        private System.Windows.Forms.PictureBox PicCell29;
        private System.Windows.Forms.PictureBox PicCell19;
        private System.Windows.Forms.PictureBox PicCell18;
        private System.Windows.Forms.PictureBox PicCell17;
        private System.Windows.Forms.PictureBox PicCell15;
        private System.Windows.Forms.PictureBox PicCell14;
        private System.Windows.Forms.PictureBox PicCell12;
        private System.Windows.Forms.PictureBox PicCell11;
        private System.Windows.Forms.PictureBox PicCell6;
        private System.Windows.Forms.PictureBox PicCell2;
        private System.Windows.Forms.PictureBox PicCell0;
        private System.Windows.Forms.PictureBox PicTower6;
        private System.Windows.Forms.PictureBox PicTower5;
        private System.Windows.Forms.PictureBox PicTower4;
        private System.Windows.Forms.PictureBox PicTower3;
        private System.Windows.Forms.PictureBox PicTower2;
        private System.Windows.Forms.PictureBox PicCell41;
        private System.Windows.Forms.PictureBox PicCell9;
        private System.Windows.Forms.PictureBox PicCell40;
        private System.Windows.Forms.PictureBox PicCell36;
        private System.Windows.Forms.PictureBox PicCell34;
        private System.Windows.Forms.PictureBox PicCell28;
        private System.Windows.Forms.PictureBox PicCell27;
        private System.Windows.Forms.PictureBox PicCell26;
        private System.Windows.Forms.PictureBox PicCell23;
        private System.Windows.Forms.PictureBox PicCell22;
        private System.Windows.Forms.PictureBox PicCell21;
        private System.Windows.Forms.PictureBox PicCell20;
        private System.Windows.Forms.PictureBox PicCell13;
        private System.Windows.Forms.PictureBox PicCell16;
        private System.Windows.Forms.PictureBox PicCell10;
        private System.Windows.Forms.PictureBox PicCell8;
        private System.Windows.Forms.PictureBox PicCell7;
        private System.Windows.Forms.PictureBox PicCell5;
        private System.Windows.Forms.PictureBox PicCell4;
        private System.Windows.Forms.PictureBox PicCell3;
        private System.Windows.Forms.PictureBox PicWarrior2;
        private System.Windows.Forms.PictureBox PicElf2;
        private System.Windows.Forms.PictureBox PicDwarf2;
        private System.Windows.Forms.PictureBox PicMage2;
        private System.Windows.Forms.PictureBox PicMage1;
        private System.Windows.Forms.PictureBox PicDwarf1;
        private System.Windows.Forms.PictureBox PicElf1;
        private System.Windows.Forms.PictureBox PicWarrior1;
        private System.Windows.Forms.PictureBox PicRightArrow;
        private System.Windows.Forms.PictureBox PicLeftArrow;
        private System.Windows.Forms.PictureBox PicDownArrow;
        private System.Windows.Forms.PictureBox PicUpArrow;
        private System.Windows.Forms.Panel PanelPlayerSelector;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblWarrior1Select;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Label lblPlayerSelectbanner;
        private System.Windows.Forms.Label lblElf2Select;
        private System.Windows.Forms.Label lblElf1Select;
        private System.Windows.Forms.Label lblMage2Select;
        private System.Windows.Forms.Label lblMage1Select;
        private System.Windows.Forms.Label lblDwarf2Select;
        private System.Windows.Forms.Label lblDwarf1Select;
        private System.Windows.Forms.Label lblWarrior2Select;
        private System.Windows.Forms.Label lblPlayerSelectStartGame;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.Panel PanelTurnOrderDecider;
        private System.Windows.Forms.Label lblTurnDeciderMessage;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox PicTurn8;
        private System.Windows.Forms.PictureBox PicTurnArrow7;
        private System.Windows.Forms.PictureBox PicTurn7;
        private System.Windows.Forms.PictureBox PicTurnArrow3;
        private System.Windows.Forms.PictureBox PicTurn3;
        private System.Windows.Forms.PictureBox PicTurnArrow2;
        private System.Windows.Forms.PictureBox PicTurn2;
        private System.Windows.Forms.PictureBox PicTurnArrow1;
        private System.Windows.Forms.PictureBox PicTurn1;
        private System.Windows.Forms.PictureBox PicTurnArrow6;
        private System.Windows.Forms.PictureBox PicTurn6;
        private System.Windows.Forms.PictureBox PicTurnArrow5;
        private System.Windows.Forms.PictureBox PicTurn5;
        private System.Windows.Forms.PictureBox PicTurnArrow4;
        private System.Windows.Forms.PictureBox PicTurn4;
        private System.Windows.Forms.Label lblTurnOrderTop;
        private System.Windows.Forms.Label lblRoundCounter;
        private System.Windows.Forms.Label lblActivePlayerTopMessage;
        private System.Windows.Forms.Panel PanelPlayerAction;
        private System.Windows.Forms.Label lblActionSelectLowerMessage;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel PanelUseItem;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel PanelRollDice;
        private System.Windows.Forms.Label lblPLayerActionName;
        private System.Windows.Forms.Panel PanelRollDiceToMove;
        private System.Windows.Forms.PictureBox PicDiceRoll2;
        private System.Windows.Forms.PictureBox PicDiceRoll1;
        private System.Windows.Forms.Label lblRollDiceToMoveTopMessage;
        private System.Windows.Forms.Label lblRollDiceToMoveLowerMessage;
        private System.Windows.Forms.Label lblRollDiceMessage;
        private System.Windows.Forms.Label lblMovementBanner;
        private System.Windows.Forms.Panel PanelOpenLockedDoorRoll;
        private System.Windows.Forms.PictureBox PicLockedDoorRoll;
        private System.Windows.Forms.Label lblLockedRollMessage;
        private System.Windows.Forms.Label lblDebugOutput;
        private System.Windows.Forms.Panel PanelBattleWindow;
        private System.Windows.Forms.PictureBox PicBattleDice2;
        private System.Windows.Forms.PictureBox PicBattleDice1;
        private System.Windows.Forms.Label lblBattlePlayerName;
        private System.Windows.Forms.Label lblBattleMonsterName;
        private System.Windows.Forms.Label lblBattleTopMessage;
        private System.Windows.Forms.PictureBox PicBattlePlayer;
        private System.Windows.Forms.PictureBox PicBattleMonster;
        private System.Windows.Forms.Label lblBattleMonsterStrengh;
        private System.Windows.Forms.Label lblBattlePlayerBonus;
        private System.Windows.Forms.Label lblBattleLowerMessage;
        private System.Windows.Forms.PictureBox PicCell39;
        private System.Windows.Forms.PictureBox PicCell33;
        private System.Windows.Forms.Label lblBattleExtraTreasures;
        private System.Windows.Forms.PictureBox PicBattleTreasure;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel PanelActionBattle;
        private System.Windows.Forms.Panel PanelTurnLost;
        private System.Windows.Forms.Label lblTurnLostTopMessage;
        private System.Windows.Forms.Label lblTurnLostDownMessage;
        private System.Windows.Forms.Label lblTurnLostMessage;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel PanelTreasureSelection;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox PicTreasureSelect5;
        private System.Windows.Forms.PictureBox PicTreasureSelect4;
        private System.Windows.Forms.PictureBox PicTreasureSelect3;
        private System.Windows.Forms.PictureBox PicTreasureSelect2;
        private System.Windows.Forms.PictureBox PicTreasureSelectRArrow;
        private System.Windows.Forms.PictureBox PicTreasureSelectLArrow;
        private System.Windows.Forms.PictureBox PicTreasureSelect1;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel PanelPlayerDead;
        private System.Windows.Forms.Label lblPlayerDeadUpperMessage;
        private System.Windows.Forms.Label lblPlayerDeadLowerMessage;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel PanelWinner;
        private System.Windows.Forms.Label ffgvdr;
        private System.Windows.Forms.PictureBox PicWinner;
        private System.Windows.Forms.Panel PanelUseElixir;
        private System.Windows.Forms.Label lblUseElixir;
        private System.Windows.Forms.Label lblUseElixirLowerMessage;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.PictureBox PicMonsterPreviewWest;
        private System.Windows.Forms.PictureBox PicMonsterPreviewEast;
        private System.Windows.Forms.PictureBox PicMonsterPreviewSouth;
        private System.Windows.Forms.PictureBox PicMonsterPreviewNorth;
        private System.Windows.Forms.PictureBox PicTransportTager;
    }
}

