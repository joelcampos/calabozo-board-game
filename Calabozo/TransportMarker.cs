﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calabozo
{
    static class TransportMarker
    {

        public static void SetSprite(PictureBox pic, Casilla initialCasilla)
        {
            sprite = pic;
            mainHallRef = initialCasilla;
            currentCasilla = initialCasilla;
            sprite.Location = currentCasilla.GetCordinates();
        }

        public static void Reset()
        {
            sprite.Visible = false;
            currentCasilla = mainHallRef;
            sprite.Location = currentCasilla.GetCordinates();
        }

        public static void Appear()
        {
            sprite.Visible = true;
        }

        public static void Move(Casilla newCasilla)
        {
            currentCasilla = newCasilla;
            sprite.Location = currentCasilla.GetCordinates();
        }

        public static Casilla CurrentCasilla { get { return currentCasilla; } }


        public static PictureBox Sprite { get { return sprite; } }

        private static PictureBox sprite;
        private static Casilla currentCasilla;
        private static Casilla mainHallRef;
    }
}
