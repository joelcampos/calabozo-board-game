﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calabozo
{
    class Card
    {
        public Card()
        {

        }

        public string Name { get { return name; } }
        public CardID ID { get { return (CardID)id; } }


        public int id = 0;
        public string name = "None";
        public string description = "None";
        public string type = "none";
        public int warriorDamage = 0;
        public int elfDamage = 0;
        public int mageDamage = 0;
        public int dwarfDamage = 0;
    }
}
