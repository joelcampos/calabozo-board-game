﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calabozo
{
    public enum CardID
    {
        //Treasures
        GemaRoja,
        GemaAzul,
        GemaVerde,
        GemaAmarilla,
        Elixir,
        Teletransportacion,
        LlaveMagica,
        MedallonMagico,
        Velocidad,
        //Tower Treasures
        AnilloMagico,
        BaculoMagico,
        EspadaMagica,
        BallestaMagica,
        EstrellaMagica,
        GuanteMagico,
        HachaMagica,
        //Monsters
        Bruja,
        CerdoGigante,
        EscorpionGigante,
        Espectro,
        Gargola,
        Gigante,
        Gnomo,
        Hechicero,
        Licantropo,
        Momia,
        Nosferatu,
        Ogro,
        RataGigante,
        Skull,
        TarantulaGigante,
        Troll,
        VampiroGigante,
        Verdugo,
        Zombie,
        //Traps
        MonstruoMuerto,
        Jaula,
        Trampa,
        //Dragons
        DragonAmarillo,
        DragonAzul,
        DragonDorado,
        DragonMorado,
        DragonRojo,
        DragonVerde
    }
    public enum PlayerClass
    {
        None = 0,
        Warrior,
        Dwarf,
        Mage,
        Elf
    }

    public enum Player
    {
        Player1 = 0,
        Player2,
        Player3,
        Player4,
        Player5,
        Player6,
        Player7,
        Player8
    }
    public enum CharacterID
    {
        None = 0,
        Warrior1,
        Warrior2,
        Dwarf1,
        Dwarf2,
        Mage1,
        Mage2,
        Elf1,
        Elf2
    }

    public enum CasillasID
    {
        Tower1 = 0,
        Tower2 = 7,
        Tower3 = 14,
        Tower4 = 147,
        Tower5 = 171,
        Tower6 = 140,
        MainHall = 85
    }

    public enum Direction
    {
        Invalid = -1,
        North,
        South,
        East,
        West
    }

    public enum ActionSelection
    {
        RollDice,
        UseItem,
        Battle
    }
}
