﻿using System;
using System.Drawing;
using System.IO;

namespace Calabozo
{
    public static class ImageServer
    {
        public static Image CharacterImage(CharacterID id)
        {
            return Image.FromFile(
            Directory.GetCurrentDirectory() + "\\ImagesFull\\" + id + ".png");
        }
        public static Image Ring()
        {
            return Image.FromFile(
            Directory.GetCurrentDirectory() + "\\Images\\AnilloMagico.png");
        }
        public static Image Weapon(CardID id)
        {
            return Image.FromFile(
            Directory.GetCurrentDirectory() + "\\Images\\" + id + ".png");
        }
        public static Image BigDice1()
        {
            return Image.FromFile(
            Directory.GetCurrentDirectory() + "\\ImagesFull\\BigDice1.png");
        }
        public static Image BigDice2()
        {
            return Image.FromFile(
            Directory.GetCurrentDirectory() + "\\ImagesFull\\BigDice2.png");
        }
        public static Image Diceface(int x)
        {
            return Image.FromFile(
            Directory.GetCurrentDirectory() + "\\ImagesFull\\DiceFace" + x + ".png");
        }

        public static Image BattleBackground(string _roomType)
        {
            return Image.FromFile(
            Directory.GetCurrentDirectory() + "\\ImagesFull\\Room_" + _roomType + ".png");
        }
        public static Image BackCard(string _cardType)
        {
            if (_cardType == "Trampa") { _cardType = "Monster"; }
            return Image.FromFile(
            Directory.GetCurrentDirectory() + "\\ImagesFull\\BackCard_" + _cardType + ".png");
        }

        public static Image VerticalCardImage(CardID _id)
        {
            return Image.FromFile(
            Directory.GetCurrentDirectory() + "\\ImagesFull\\" + _id + ".png");
        }
        public static Image HorizontalCardImage(CardID _id)
        {
            return Image.FromFile(
            Directory.GetCurrentDirectory() + "\\ImagesFull\\" + _id + "Horizontal.png");
        }
    }
}
