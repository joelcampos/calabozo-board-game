﻿//Joel Campos
//6/29/2021
//Calabozo La Aventura de los Anillos

using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System;

namespace Calabozo
{
    public partial class MainBoard : Form
    {
        #region Private Enums
        private enum GameState
        {
            PlayerSelect,
            TurnOrderDecide,
            TurnDeciderClose,
            TurnStart,
            AIRunning,
            PlayerActionSelection,
            ActionRollDiceToMove,
            ActionRollDiceToMoveFinish,
            PlayerMove,
            LockedDoorRoll,
            BattleAttackRoll,
            BattleOver,
            TurnLostDeciderRoll,
            TurnLost,
            TreasureSelection,
            ExitGame,
            GameOver,
            UseElixir,
            TransportMarkerActive,
        }
        #endregion

        #region Constructors
        public MainBoard()
        {
            InitializeComponent();
            string jsonFilePath = Directory.GetCurrentDirectory() + "\\Json\\CardDB.json";
            string rawdata = File.ReadAllText(jsonFilePath);
            CardDB = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Card>>(rawdata);

            jsonFilePath = Directory.GetCurrentDirectory() + "\\Json\\BoardDB.json";
            rawdata = File.ReadAllText(jsonFilePath);
            CasillasDB = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Casilla>>(rawdata);

            //Populate the data
            InitializeCellsAndTowers();

            //Place the PlayerSelection panel
            PanelPlayerSelector.Location = new Point(321, 252);
        }
        #endregion

        #region Private Member Functions
        private void InitializeCellsAndTowers()
        {
            dragons.Add(CardDB[(int)CardID.DragonAmarillo]);
            dragons.Add(CardDB[(int)CardID.DragonAzul]);
            dragons.Add(CardDB[(int)CardID.DragonDorado]);
            dragons.Add(CardDB[(int)CardID.DragonMorado]);
            dragons.Add(CardDB[(int)CardID.DragonRojo]);
            dragons.Add(CardDB[(int)CardID.DragonVerde]);
            dragonTreasure.Add(CardDB[(int)CardID.BaculoMagico]);
            dragonTreasure.Add(CardDB[(int)CardID.EspadaMagica]);
            dragonTreasure.Add(CardDB[(int)CardID.BallestaMagica]);
            dragonTreasure.Add(CardDB[(int)CardID.EstrellaMagica]);
            dragonTreasure.Add(CardDB[(int)CardID.GuanteMagico]);
            dragonTreasure.Add(CardDB[(int)CardID.HachaMagica]);

            /*for (int x = 0; x < 42; x++)
            {
                monsters.Add(CardDB[(int)CardID.Zombie]);
            }*/

            monsters.Add(CardDB[(int)CardID.Bruja]);
            monsters.Add(CardDB[(int)CardID.CerdoGigante]);
            monsters.Add(CardDB[(int)CardID.EscorpionGigante]);
            monsters.Add(CardDB[(int)CardID.Espectro]);
            monsters.Add(CardDB[(int)CardID.Gargola]);
            monsters.Add(CardDB[(int)CardID.Gigante]);
            monsters.Add(CardDB[(int)CardID.Gnomo]);
            monsters.Add(CardDB[(int)CardID.Hechicero]);
            monsters.Add(CardDB[(int)CardID.Licantropo]);
            monsters.Add(CardDB[(int)CardID.Momia]);
            monsters.Add(CardDB[(int)CardID.RataGigante]);
            monsters.Add(CardDB[(int)CardID.Skull]);
            monsters.Add(CardDB[(int)CardID.TarantulaGigante]);
            monsters.Add(CardDB[(int)CardID.Troll]);
            monsters.Add(CardDB[(int)CardID.VampiroGigante]);
            monsters.Add(CardDB[(int)CardID.Verdugo]);
            monsters.Add(CardDB[(int)CardID.Zombie]);

            monsters.Add(CardDB[(int)CardID.Trampa]);
            monsters.Add(CardDB[(int)CardID.Trampa]);
            monsters.Add(CardDB[(int)CardID.Trampa]);
            monsters.Add(CardDB[(int)CardID.Jaula]);
            monsters.Add(CardDB[(int)CardID.Jaula]);
            monsters.Add(CardDB[(int)CardID.Jaula]);
            monsters.Add(CardDB[(int)CardID.MonstruoMuerto]);
            monsters.Add(CardDB[(int)CardID.MonstruoMuerto]);

            monsters.Add(CardDB[(int)CardID.Bruja]);
            monsters.Add(CardDB[(int)CardID.CerdoGigante]);
            monsters.Add(CardDB[(int)CardID.EscorpionGigante]);
            monsters.Add(CardDB[(int)CardID.Gargola]);
            monsters.Add(CardDB[(int)CardID.Gigante]);
            monsters.Add(CardDB[(int)CardID.Gnomo]);
            monsters.Add(CardDB[(int)CardID.Hechicero]);
            monsters.Add(CardDB[(int)CardID.Licantropo]);
            monsters.Add(CardDB[(int)CardID.Momia]);
            monsters.Add(CardDB[(int)CardID.Ogro]);
            monsters.Add(CardDB[(int)CardID.RataGigante]);
            monsters.Add(CardDB[(int)CardID.Skull]);
            monsters.Add(CardDB[(int)CardID.TarantulaGigante]);
            monsters.Add(CardDB[(int)CardID.Troll]);
            monsters.Add(CardDB[(int)CardID.VampiroGigante]);
            monsters.Add(CardDB[(int)CardID.Verdugo]);
            monsters.Add(CardDB[(int)CardID.Zombie]);

            treasures.Add(CardDB[(int)CardID.GemaRoja]);
            treasures.Add(CardDB[(int)CardID.GemaRoja]);
            treasures.Add(CardDB[(int)CardID.GemaRoja]);
            treasures.Add(CardDB[(int)CardID.GemaRoja]);
            treasures.Add(CardDB[(int)CardID.GemaRoja]);
            treasures.Add(CardDB[(int)CardID.GemaRoja]);
            treasures.Add(CardDB[(int)CardID.GemaAzul]);
            treasures.Add(CardDB[(int)CardID.GemaAzul]);
            treasures.Add(CardDB[(int)CardID.GemaAzul]);
            treasures.Add(CardDB[(int)CardID.GemaAzul]);
            treasures.Add(CardDB[(int)CardID.GemaAzul]);
            treasures.Add(CardDB[(int)CardID.GemaAzul]);
            treasures.Add(CardDB[(int)CardID.GemaVerde]);
            treasures.Add(CardDB[(int)CardID.GemaVerde]);
            treasures.Add(CardDB[(int)CardID.GemaVerde]);
            treasures.Add(CardDB[(int)CardID.GemaVerde]);
            treasures.Add(CardDB[(int)CardID.GemaVerde]);
            treasures.Add(CardDB[(int)CardID.GemaVerde]);
            treasures.Add(CardDB[(int)CardID.GemaAmarilla]);
            treasures.Add(CardDB[(int)CardID.GemaAmarilla]);
            treasures.Add(CardDB[(int)CardID.GemaAmarilla]);
            treasures.Add(CardDB[(int)CardID.GemaAmarilla]);
            treasures.Add(CardDB[(int)CardID.GemaAmarilla]);
            treasures.Add(CardDB[(int)CardID.GemaAmarilla]);
            treasures.Add(CardDB[(int)CardID.Teletransportacion]);
            treasures.Add(CardDB[(int)CardID.Teletransportacion]);
            treasures.Add(CardDB[(int)CardID.Teletransportacion]);
            treasures.Add(CardDB[(int)CardID.Teletransportacion]);
            treasures.Add(CardDB[(int)CardID.Teletransportacion]);
            treasures.Add(CardDB[(int)CardID.Teletransportacion]);
            treasures.Add(CardDB[(int)CardID.Teletransportacion]);
            treasures.Add(CardDB[(int)CardID.Elixir]);
            treasures.Add(CardDB[(int)CardID.Elixir]);
            treasures.Add(CardDB[(int)CardID.Elixir]);
            treasures.Add(CardDB[(int)CardID.Elixir]);
            treasures.Add(CardDB[(int)CardID.Elixir]);
            treasures.Add(CardDB[(int)CardID.Elixir]);
            treasures.Add(CardDB[(int)CardID.Elixir]);
            treasures.Add(CardDB[(int)CardID.Elixir]);
            treasures.Add(CardDB[(int)CardID.LlaveMagica]);
            treasures.Add(CardDB[(int)CardID.MedallonMagico]);
            treasures.Add(CardDB[(int)CardID.Velocidad]);

            Rand.Shuffle(dragons);
            Rand.Shuffle(dragonTreasure);
            Rand.Shuffle(monsters);
            Rand.Shuffle(treasures);

            //Initialize the additional treasure list
            for (int x = 0; x < 42; x++)
            {
                additionalTreasures[x] = new List<Card>();
            }
            for (int x = 0; x < 6; x++)
            {
                additionalTreasuresOnDragon[x] = new List<Card>();
            }

            //Initialize the arrays
            towersImageList.Add(PicTower1);
            towersImageList.Add(PicTower2);
            towersImageList.Add(PicTower3);
            towersImageList.Add(PicTower4);
            towersImageList.Add(PicTower5);
            towersImageList.Add(PicTower6);

            cellsImageList.Add(PicCell0);
            cellsImageList.Add(PicCell1);
            cellsImageList.Add(PicCell2);
            cellsImageList.Add(PicCell3);
            cellsImageList.Add(PicCell4);
            cellsImageList.Add(PicCell5);
            cellsImageList.Add(PicCell6);
            cellsImageList.Add(PicCell7);
            cellsImageList.Add(PicCell8);
            cellsImageList.Add(PicCell9);
            cellsImageList.Add(PicCell10);
            cellsImageList.Add(PicCell11);
            cellsImageList.Add(PicCell12);
            cellsImageList.Add(PicCell13);
            cellsImageList.Add(PicCell14);
            cellsImageList.Add(PicCell15);
            cellsImageList.Add(PicCell16);
            cellsImageList.Add(PicCell17);
            cellsImageList.Add(PicCell18);
            cellsImageList.Add(PicCell19);
            cellsImageList.Add(PicCell20);
            cellsImageList.Add(PicCell21);
            cellsImageList.Add(PicCell22);
            cellsImageList.Add(PicCell23);
            cellsImageList.Add(PicCell24);
            cellsImageList.Add(PicCell25);
            cellsImageList.Add(PicCell26);
            cellsImageList.Add(PicCell27);
            cellsImageList.Add(PicCell28);
            cellsImageList.Add(PicCell29);
            cellsImageList.Add(PicCell30);
            cellsImageList.Add(PicCell31);
            cellsImageList.Add(PicCell32);
            cellsImageList.Add(PicCell333);
            cellsImageList.Add(PicCell34);
            cellsImageList.Add(PicCell35);
            cellsImageList.Add(PicCell36);
            cellsImageList.Add(PicCell37);
            cellsImageList.Add(PicCell38);
            cellsImageList.Add(PicCell399);
            cellsImageList.Add(PicCell40);
            cellsImageList.Add(PicCell41);

            //Initializer the transportation marker
            TransportMarker.SetSprite(PicTransportTager, CasillasDB[(int)CasillasID.MainHall]);
        }
        private void LoadActivePlayers()
        {
            for (int x = 0; x < playerList.Count; x++)
            {
                switch (playerList[x].CharID)
                {
                    case CharacterID.Warrior1: playerList[x].SetSprite(PicWarrior1); PicWarrior1.Visible = true; break;
                    case CharacterID.Warrior2: playerList[x].SetSprite(PicWarrior2); PicWarrior2.Visible = true; break;
                    case CharacterID.Dwarf1: playerList[x].SetSprite(PicDwarf1); PicDwarf1.Visible = true; break;
                    case CharacterID.Dwarf2: playerList[x].SetSprite(PicDwarf2); PicDwarf2.Visible = true; break;
                    case CharacterID.Mage1: playerList[x].SetSprite(PicMage1); PicMage1.Visible = true; break;
                    case CharacterID.Mage2: playerList[x].SetSprite(PicMage2); PicMage2.Visible = true; break;
                    case CharacterID.Elf1: playerList[x].SetSprite(PicElf1); PicElf1.Visible = true; break;
                    case CharacterID.Elf2: playerList[x].SetSprite(PicElf2); PicElf2.Visible = true; break;
                }

                //Set all players as occupants of the main hall
                CasillasDB[(int)CasillasID.MainHall].AddOccupant(playerList[x].PlayerID);
            }

            //Set the winning ring target based on the # of players
            if (playerList.Count < 3) { ringsTargetAmount = 4; }
            else if (playerList.Count < 7) { ringsTargetAmount = 3; }
            else { ringsTargetAmount = 2; }

           //playerList[1].AddTreasure(CardDB[(int)CardID.Teletransportacion]);
           //playerList[1].AddTreasure(CardDB[(int)CardID.Teletransportacion]);
           //playerList[1].AddTreasure(CardDB[(int)CardID.GemaAmarilla]);
           //playerList[1].AddTreasure(CardDB[(int)CardID.GemaAmarilla]);
            //playerList[0].AddTreasure(CardDB[(int)CardID.GemaAmarilla]);
        }
        private void ShowMovementArrowsForPlayer()
        {
            //Set the player
            PlayerData _currentPlayer = activePlayerRef;

            //Get the player's current location
            Point _currentPlayerLocation = _currentPlayer.Location;

            //Genreate the valid direction list
            validDirectionList = GetValidDirectionList();

            if(validDirectionList.Count == 0)
            {
                spacesToMove = 0;
                lblMovementBanner.Location = new Point(_currentPlayerLocation.X - 63, _currentPlayerLocation.Y - 48);
                lblMovementBanner.Text = "Player Is Stuck";
                lblMovementBanner.Visible = true;
            }
            else
            {
                //show arrows
                if (validDirectionList.Contains(Direction.North))
                {
                    PicUpArrow.Location = new Point(_currentPlayerLocation.X - 5, _currentPlayerLocation.Y - 19);
                    PicUpArrow.Visible = true;
                    DisplayMonsterPreview(Direction.North, _currentPlayerLocation);
                }
                if (validDirectionList.Contains(Direction.South))
                {
                    PicDownArrow.Location = new Point(_currentPlayerLocation.X - 5, _currentPlayerLocation.Y + 45);
                    PicDownArrow.Visible = true;
                    DisplayMonsterPreview(Direction.South, _currentPlayerLocation);
                }
                if (validDirectionList.Contains(Direction.East))
                {
                    PicRightArrow.Location = new Point(_currentPlayerLocation.X + 39, _currentPlayerLocation.Y);
                    PicRightArrow.Visible = true;
                    DisplayMonsterPreview(Direction.East, _currentPlayerLocation);
                }
                if (validDirectionList.Contains(Direction.West))
                {
                    PicLeftArrow.Location = new Point(_currentPlayerLocation.X - 22, _currentPlayerLocation.Y);
                    PicLeftArrow.Visible = true;
                    DisplayMonsterPreview(Direction.West, _currentPlayerLocation);
                }

                lblMovementBanner.Location = new Point(_currentPlayerLocation.X - 63, _currentPlayerLocation.Y - 48);
                lblMovementBanner.Text = "Spaces Left: " + spacesToMove;
                lblMovementBanner.Size = new Size(170, 30);
                lblMovementBanner.Visible = true;
            }           
        }
        private void ShowMovementArrowsForTransportMarker()
        {
            //Get the marker's current location
            Point _currentMarkerLocation = TransportMarker.Sprite.Location;

            PicUpArrow.Visible = false;
            PicDownArrow.Visible = false;
            PicRightArrow.Visible = false;
            PicLeftArrow.Visible = false;

            if (TransportMarker.CurrentCasilla.GetDoor(Direction.North) != "Wall")
            {
                //show arror
                PicUpArrow.Location = new Point(_currentMarkerLocation.X - 3, _currentMarkerLocation.Y - 19);
                PicUpArrow.Visible = true;
            }
            if (TransportMarker.CurrentCasilla.GetDoor(Direction.South) != "Wall")
            {
                //show arror
                PicDownArrow.Location = new Point(_currentMarkerLocation.X - 3, _currentMarkerLocation.Y + 39);
                PicDownArrow.Visible = true;
            }
            if (TransportMarker.CurrentCasilla.GetDoor(Direction.East) != "Wall")
            {
                //show arror
                PicRightArrow.Location = new Point(_currentMarkerLocation.X + 39, _currentMarkerLocation.Y - 4);
                PicRightArrow.Visible = true;
            }
            if (TransportMarker.CurrentCasilla.GetDoor(Direction.West) != "Wall")
            {
                //show arror
                PicLeftArrow.Location = new Point(_currentMarkerLocation.X - 18, _currentMarkerLocation.Y - 4);
                PicLeftArrow.Visible = true;
            }

            lblMovementBanner.Location = new Point(_currentMarkerLocation.X - 113, _currentMarkerLocation.Y - 48);
            lblMovementBanner.Text = "J - Confirm   I - Cancel";
            lblMovementBanner.Size = new Size(280, 30);
            lblMovementBanner.Visible = true;

        }
        private void DisplayMonsterPreview(Direction _d, Point _currentPlayerLocation)
        {
            PictureBox thisPicture = null;
            switch (_d)
            {
                case Direction.North: thisPicture = PicMonsterPreviewNorth; break;
                case Direction.South: thisPicture = PicMonsterPreviewSouth; break;
                case Direction.East: thisPicture = PicMonsterPreviewEast; break;
                case Direction.West: thisPicture = PicMonsterPreviewWest; break;
            }

            int _casillaID = activePlayerRef.GetNextCasillaID(_d);
            string _casillaType = CasillasDB[_casillaID].type;
            if (CasillasDB[_casillaID].isMonsterInIt &&  (CasillasDB[_casillaID].monsterUp || activePlayerRef.HasMedallion()))
            {
                if (_casillaType == "Cell")
                {
                    int index = CasillasDB[_casillaID].cellID;
                    thisPicture.Image = ImageServer.VerticalCardImage(monsters[index].ID);
                    thisPicture.Visible = true;
                    if (activePlayerRef.HasMedallion() && activePlayerID != Player.Player1) { thisPicture.Image = ImageServer.BackCard("Monster"); }
                }
                else
                {
                    int index = CasillasDB[_casillaID].towerID;
                    thisPicture.Image = ImageServer.VerticalCardImage(dragons[index].ID);
                    thisPicture.Visible = true;
                    if (activePlayerRef.HasMedallion() && activePlayerID != Player.Player1) { thisPicture.Image = ImageServer.BackCard("Dragon"); }
                }

                //Place the Picture box based on its direction
                switch (_d)
                {
                    case Direction.North: thisPicture.Location = new Point(_currentPlayerLocation.X - 65, _currentPlayerLocation.Y - 210); break;
                    case Direction.South: thisPicture.Location = new Point(_currentPlayerLocation.X - 65, _currentPlayerLocation.Y + 65); break;
                    case Direction.East: thisPicture.Location = new Point(_currentPlayerLocation.X + 59, _currentPlayerLocation.Y - 80); break;
                    case Direction.West: thisPicture.Location = new Point(_currentPlayerLocation.X - 182, _currentPlayerLocation.Y - 80); break;
                }
            }
            else
            {
                thisPicture.Visible = false;
            }
        }
        private Casilla GetPlayerNextCasilla(Direction _d)
        {
            //Get the next casilla
            int _nextCasillaID = activePlayerRef.GetNextCasillaID(_d);
            return CasillasDB[_nextCasillaID];
        }
        private void UpdatePlayerSelectSelection()
        {
            switch (oldPlayerSelectSelection)
            {
                case CharacterID.Warrior1: lblWarrior1Select.BackColor = Color.DarkOliveGreen; break;
                case CharacterID.Warrior2: lblWarrior2Select.BackColor = Color.DarkOliveGreen; break;
                case CharacterID.Dwarf1: lblDwarf1Select.BackColor = Color.DarkOliveGreen; break;
                case CharacterID.Dwarf2: lblDwarf2Select.BackColor = Color.DarkOliveGreen; break;
                case CharacterID.Mage1: lblMage1Select.BackColor = Color.DarkOliveGreen; break;
                case CharacterID.Mage2: lblMage2Select.BackColor = Color.DarkOliveGreen; break;
                case CharacterID.Elf1: lblElf1Select.BackColor = Color.DarkOliveGreen; break;
                case CharacterID.Elf2: lblElf2Select.BackColor = Color.DarkOliveGreen; break;
                case CharacterID.None: lblPlayerSelectStartGame.BackColor = Color.DarkOliveGreen; break;
            }

            switch (currentPlayerSelectSelection)
            {
                case CharacterID.Warrior1: lblWarrior1Select.BackColor = Color.Maroon; break;
                case CharacterID.Warrior2: lblWarrior2Select.BackColor = Color.Maroon; break;
                case CharacterID.Dwarf1: lblDwarf1Select.BackColor = Color.Maroon; break;
                case CharacterID.Dwarf2: lblDwarf2Select.BackColor = Color.Maroon; break;
                case CharacterID.Mage1: lblMage1Select.BackColor = Color.Maroon; break;
                case CharacterID.Mage2: lblMage2Select.BackColor = Color.Maroon; break;
                case CharacterID.Elf1: lblElf1Select.BackColor = Color.Maroon; break;
                case CharacterID.Elf2: lblElf2Select.BackColor = Color.Maroon; break;
                case CharacterID.None: lblPlayerSelectStartGame.BackColor = Color.Maroon; break;
            }
        }
        private void UpdatePlayerSelectLabelOption(Label lbloption)
        {
            switch (lbloption.Text)
            {
                case "Select":
                    if (mainPlayerSelection != CharacterID.None) { lbloption.Text = "CPU"; cpuList.Add(currentPlayerSelectSelection); }
                    else { lbloption.Text = "Player"; mainPlayerSelection = currentPlayerSelectSelection; } break;
                case "Player": lbloption.Text = "CPU"; mainPlayerSelection = CharacterID.None; cpuList.Add(currentPlayerSelectSelection); break;
                case "CPU": lbloption.Text = "Select"; cpuList.Remove(currentPlayerSelectSelection); break;
            }

            //Update "Start Game" label if PLayer was set and there is at least 1 cpu
            if (mainPlayerSelection != CharacterID.None && cpuList.Count > 0)
            {
                lblPlayerSelectStartGame.Text = "Start Game";
            }
            else
            {
                lblPlayerSelectStartGame.Text = "Not Enough Players";
            }
        }
        private void UpdateTurnOrderTopMesage()
        {
            CharacterID _activeID = playerList[(int)activePlayerID].CharID;
            lblActivePlayerTopMessage.Text = ParseCharacterIDMessage("Active Player: " + _activeID + "(" + activePlayerID + ")");

            lblTurnOrderTop.Text = "Turn Order: ";
            for (int x = 0; x < playerList.Count; x++)
            {
                Player thisPlayerID = turnOrderList[x];
                if (!defeatedPlayers.Contains(thisPlayerID))
                {
                    CharacterID _cID = playerList[(int)thisPlayerID].CharID;

                    if (x == 0) { lblTurnOrderTop.Text += _cID; }
                    else { lblTurnOrderTop.Text += " > " + _cID; }
                }
            }

            lblTurnOrderTop.Text = ParseCharacterIDMessage(lblTurnOrderTop.Text);
        }
        private void VanishArrowsAndBanner()
        {
            //Vanish the arrows and banner
            PicUpArrow.Visible = false;
            PicDownArrow.Visible = false;
            PicRightArrow.Visible = false;
            PicLeftArrow.Visible = false;
            lblMovementBanner.Visible = false;
        }
        private static void WaitNSeconds(double milliseconds)
        {
            if (milliseconds < 1) return;
            DateTime _desired = DateTime.Now.AddMilliseconds(milliseconds);
            while (DateTime.Now < _desired)
            {
                Application.DoEvents();
            }
        }
        private Direction GetRandomValidDirection()
        {
            List<Direction> _availableMoves = new List<Direction>();


            for (int x = 0; x < 4; x++)
            {
                if (activePlayerRef.GetNextCasillaID((Direction)x) != -1 && activePlayerRef.CurrentCasilla.GetDoor((Direction)x) != "Wall")
                {
                    Casilla _nextCasilla = CasillasDB[activePlayerRef.GetNextCasillaID((Direction)x)];
                    bool DirectionStatus = GetDirectionValidStatus(_nextCasilla, spacesToMove);
                    if (DirectionStatus)
                    {
                        _availableMoves.Add((Direction)x);
                    }
                }
            }


            if (_availableMoves.Count == 0)
            {
                return Direction.Invalid;
            }
            else
            {
                if (_availableMoves.Contains(activePlayerRef.PreviousDirection) && _availableMoves.Count > 1)
                {
                    _availableMoves.Remove(activePlayerRef.PreviousDirection);
                }

                //Pick a direction from the reminding available list
                int randIndex = Rand.Range(0, _availableMoves.Count);
                return _availableMoves[randIndex];
            }
        }
        private List<Direction> GetValidDirectionList()
        {
            List<Direction> _availableMoves = new List<Direction>();


            for (int x = 0; x < 4; x++)
            {
                if (activePlayerRef.GetNextCasillaID((Direction)x) != -1 && activePlayerRef.CurrentCasilla.GetDoor((Direction)x) != "Wall")
                {
                    bool skip = false;
                    if ((Direction)x == Direction.North)
                    {
                        if(activePlayerRef.CurrentCasilla.IsOccupiedBySomeoneElse && activePlayerRef.CurrentCasilla.northDoor == "Locked")
                        {
                            //Exclude this as a valid direction
                            skip = true;
                        }
                    }
                    if ((Direction)x == Direction.South)
                    {
                        if (activePlayerRef.CurrentCasilla.IsOccupiedBySomeoneElse && activePlayerRef.CurrentCasilla.southDoor == "Locked")
                        {
                            //Exclude this as a valid direction
                            skip = true;
                        }
                    }
                    if ((Direction)x == Direction.East)
                    {
                        if (activePlayerRef.CurrentCasilla.IsOccupiedBySomeoneElse && activePlayerRef.CurrentCasilla.eastDoor == "Locked")
                        {
                            //Exclude this as a valid direction
                            skip = true;
                        }
                    }
                    if ((Direction)x == Direction.West)
                    {
                        if (activePlayerRef.CurrentCasilla.IsOccupiedBySomeoneElse && activePlayerRef.CurrentCasilla.westDoor == "Locked")
                        {
                            //Exclude this as a valid direction
                            skip = true;
                        }
                    }

                    if (!skip)
                    {
                        Casilla _nextCasilla = CasillasDB[activePlayerRef.GetNextCasillaID((Direction)x)];
                        bool DirectionStatus = GetDirectionValidStatus(_nextCasilla, spacesToMove);
                        if (DirectionStatus)
                        {
                            _availableMoves.Add((Direction)x);
                        }
                    }
                }
            }


            return _availableMoves;
        }
        private string ParseCharacterIDMessage(string _ogMessage)
        {
            _ogMessage = _ogMessage.Replace("Warrior1", "Guerrero Dorado");
            _ogMessage = _ogMessage.Replace("Warrior2", "Guerrero Azul");
            _ogMessage = _ogMessage.Replace("Dwarf1", "Duende Fuego");
            _ogMessage = _ogMessage.Replace("Dwarf2", "Duende Salvage");
            _ogMessage = _ogMessage.Replace("Mage1", "Mago");
            _ogMessage = _ogMessage.Replace("Mage2", "Maga");
            _ogMessage = _ogMessage.Replace("Elf1", "Elfo");
            _ogMessage = _ogMessage.Replace("Elf2", "Elfa");
            return _ogMessage;
        }
        private void TransportToCasilla(int _casillaID)
        {
            //Before transport remove player as an occupant of the current casilla
            activePlayerRef.CurrentCasilla.RemoveOccupant(activePlayerID);

            //set the target casilla
            Casilla _targetCasilla = CasillasDB[_casillaID];
            Point _targetLocation = _targetCasilla.GetCordinates();
            if (_casillaID == 85)
            {
                switch (activePlayerRef.CharID)
                {
                    case CharacterID.Warrior1: _targetLocation = new Point(497, 370); break;
                    case CharacterID.Warrior2: _targetLocation = new Point(596, 472); break;
                    case CharacterID.Dwarf1: _targetLocation = new Point(596, 371); break;
                    case CharacterID.Dwarf2: _targetLocation = new Point(497, 472); break;
                    case CharacterID.Mage1: _targetLocation = new Point(497, 422); break;
                    case CharacterID.Mage2: _targetLocation = new Point(596, 422); break;
                    case CharacterID.Elf1: _targetLocation = new Point(546, 370); break;
                    case CharacterID.Elf2: _targetLocation = new Point(546, 472); break;
                }
            }

            //Move the sprite to the next location
            activePlayerRef.Sprite.Location = _targetLocation;

            //Update casilla
            activePlayerRef.CurrentCasilla = _targetCasilla;

            //Now add player as occupant of the current (new) casilla
            activePlayerRef.CurrentCasilla.AddOccupant(activePlayerID);
        }
        private void OpenTreasureSelectionPanel()
        {
            //Show the panel
            PanelTreasureSelection.Location = new Point(353, 340);
            PanelTreasureSelection.Visible = true;

            currentTreasureSelectionIndex = 0;

            //Load the list UI
            UpdateTreasureSelectionList();
        }
        private void UpdateTreasureSelectionList()
        {
            if (currentTreasureSelectionIndex < 2)
            {
                PicTreasureSelect1.Visible = false;
            }
            else
            {
                PicTreasureSelect1.Visible = true;
                PicTreasureSelect1.Image = ImageServer.VerticalCardImage(activePlayerRef.TreasureList[currentTreasureSelectionIndex - 2].ID);
            }
            if (currentTreasureSelectionIndex == 0)
            {
                PicTreasureSelect2.Visible = false;
            }
            else
            {
                PicTreasureSelect2.Visible = true;
                PicTreasureSelect2.Image = ImageServer.VerticalCardImage(activePlayerRef.TreasureList[currentTreasureSelectionIndex - 1].ID);
            }
            PicTreasureSelect3.Visible = true;
            PicTreasureSelect3.Image = ImageServer.VerticalCardImage(activePlayerRef.TreasureList[currentTreasureSelectionIndex].ID);
            if (currentTreasureSelectionIndex < activePlayerRef.TreasureList.Count - 1)
            {
                PicTreasureSelect4.Visible = true;
                PicTreasureSelect4.Image = ImageServer.VerticalCardImage(activePlayerRef.TreasureList[currentTreasureSelectionIndex + 1].ID);
            }
            else
            {
                PicTreasureSelect4.Visible = false;
            }
            if (currentTreasureSelectionIndex < activePlayerRef.TreasureList.Count - 2)
            {
                PicTreasureSelect5.Visible = true;
                PicTreasureSelect5.Image = ImageServer.VerticalCardImage(activePlayerRef.TreasureList[currentTreasureSelectionIndex + 2].ID);
            }
            else
            {
                PicTreasureSelect5.Visible = false;
            }

            if (currentTreasureSelectionIndex == 0)
            {
                PicTreasureSelectLArrow.Visible = false;
            }
            else
            {
                PicTreasureSelectLArrow.Visible = true;
            }
            if (currentTreasureSelectionIndex == activePlayerRef.TreasureList.Count - 1)
            {
                PicTreasureSelectRArrow.Visible = false;
            }
            else
            {
                PicTreasureSelectRArrow.Visible = true;
            }
        }
        private void TransportToRandomInframundo()
        {
            //generate a random number
            List<int> inframundoIDs = new List<int>();
            inframundoIDs.Add(42);
            inframundoIDs.Add(48);
            inframundoIDs.Add(137);
            inframundoIDs.Add(177);

            int randIndex = Rand.Range(0, 4);
            TransportToCasilla(inframundoIDs[randIndex]);

            //Set the player as dead
            activePlayerRef.Dies();
        }
        private bool GetDirectionValidStatus(Casilla thisCasilla, int casillasCounter)
        {
            casillasCounter--;
            if (thisCasilla.IsOccupied && thisCasilla.id != (int)CasillasID.MainHall)
            {
                //If this is the last casilla to check, return true if it is unoccupied, false otherwise
                if (casillasCounter == 0)
                {
                    return false;
                }
                //if the casilla is occupied but also has a monster in it return false, a player cannot step into a cell/tower with a monster and a player in it.
                else if (thisCasilla.isMonsterInIt)
                {
                    return false;
                }
                else
                {
                    //Check the status of the next casilla in each direction if the counter still is not 0
                    bool northStatus = false;
                    bool southStatus = false;
                    bool eastStatus = false;
                    bool westStatus = false;

                    //if this casilla is occupied, each locked door wont count as a good direction
                    if (thisCasilla.IsOccupied && thisCasilla.northDoor == "Locked" && !activePlayerRef.HasKey())
                    {
                        northStatus = false;
                    }
                    else
                    {
                        if (thisCasilla.northCasillaID != -1 && thisCasilla.northDoor != "Wall") { northStatus = GetDirectionValidStatus(CasillasDB[thisCasilla.northCasillaID], casillasCounter); }
                    }
                                       
                    if (thisCasilla.IsOccupied && thisCasilla.southDoor == "Locked" && !activePlayerRef.HasKey()) { southStatus = false; }
                    else { if (thisCasilla.southCasillaID != -1 && thisCasilla.southDoor != "Wall") { southStatus = GetDirectionValidStatus(CasillasDB[thisCasilla.southCasillaID], casillasCounter); } }
                   
                    if (thisCasilla.IsOccupied && thisCasilla.eastDoor == "Locked" && !activePlayerRef.HasKey()) { eastStatus = false; }
                    else { if (thisCasilla.eastCasillaID != -1 && thisCasilla.eastDoor != "Wall") { eastStatus = GetDirectionValidStatus(CasillasDB[thisCasilla.eastCasillaID], casillasCounter); } }
                   
                    if (thisCasilla.IsOccupied && thisCasilla.westDoor == "Locked" && !activePlayerRef.HasKey()) { westStatus = false; }
                    else { if (thisCasilla.westCasillaID != -1 && thisCasilla.westDoor != "Wall") { westStatus = GetDirectionValidStatus(CasillasDB[thisCasilla.westCasillaID], casillasCounter); } }

                    //Return true if at least 1 direction resulted in true.
                    return (northStatus || southStatus || eastStatus || westStatus);
                }
            }
            else
            {
                return true;
            }
        }
        private void GenerateAIMoveListToTransportTarget(Direction startingD)
        {
            List<Direction> directionList = new List<Direction>();

            Casilla thisCasilla = CasillasDB[(int)CasillasID.MainHall];
            casillasAlreadyChecked = new List<int>();

            switch(startingD)
            {
                case Direction.North: CheckCasillaTargetStatus(CasillasDB[thisCasilla.northCasillaID], directionList, Direction.North); break;
                case Direction.South: CheckCasillaTargetStatus(CasillasDB[thisCasilla.southCasillaID], directionList, Direction.South); break;
                case Direction.East: CheckCasillaTargetStatus(CasillasDB[thisCasilla.eastCasillaID], directionList, Direction.East); break;
                case Direction.West: CheckCasillaTargetStatus(CasillasDB[thisCasilla.westCasillaID], directionList, Direction.West); break;
            }

            //clear the casillas already cehck at the ebnd
            casillasAlreadyChecked.Clear();
        }
        private void CheckCasillaTargetStatus(Casilla thisCasilla, List<Direction> currentMoveList, Direction thisDirection)
        {
            List<Direction> thisList = new List<Direction>();
            
            //make a local copy of the list
            for(int x = 0; x < currentMoveList.Count; x++)
            {
                thisList.Add(currentMoveList[x]);
            }

            thisList.Add(thisDirection);

            if(casillasAlreadyChecked.Contains(thisCasilla.id))
            {
                //skip it
            }
            else
            {
                casillasAlreadyChecked.Add(thisCasilla.id);
                //if the target casilla was found, set the final list, only if the list is smaller than the curent final
                if (thisCasilla.id == cpuTransportCasillaTarget)
                {
                    if (!firstMarkListSet)
                    {
                        finalMoveMarkerList = thisList;
                        firstMarkListSet = true;
                    }
                    else
                    {
                        if (thisList.Count < finalMoveMarkerList.Count)
                        {
                            finalMoveMarkerList = thisList;
                        }
                    }
                }
                else
                {
                    //Shuffle the order in which directions to check so it is not always the same path
                    List<Direction> directions = new List<Direction>();
                    directions.Add(Direction.North);
                    directions.Add(Direction.South);
                    directions.Add(Direction.East);
                    directions.Add(Direction.West);
                    directions.Shuffle();

                    for(int x = 0; x < directions.Count; x++)
                    {
                        switch(directions[x])
                        {
                            case Direction.North: if (thisDirection != Direction.South && CasillasDB[thisCasilla.id].northDoor != "Wall") { CheckCasillaTargetStatus(CasillasDB[thisCasilla.northCasillaID], thisList, Direction.North); } break;
                            case Direction.South: if (thisDirection != Direction.North && CasillasDB[thisCasilla.id].southDoor != "Wall") { CheckCasillaTargetStatus(CasillasDB[thisCasilla.southCasillaID], thisList, Direction.South); } break;
                            case Direction.East:  if (thisDirection != Direction.West && CasillasDB[thisCasilla.id].eastDoor != "Wall") { CheckCasillaTargetStatus(CasillasDB[thisCasilla.eastCasillaID], thisList, Direction.East); } break;
                            case Direction.West:  if (thisDirection != Direction.East && CasillasDB[thisCasilla.id].westDoor != "Wall") { CheckCasillaTargetStatus(CasillasDB[thisCasilla.westCasillaID], thisList, Direction.West); } break;
                        }
                    }
                }
            }           
        }
        #endregion

        #region Turn Steps Functions
        private void TurnStart()
        {
            UpdateDebugOutput();

            if (activePlayerRef.TurnsLost > 0)
            {
                //Show the lost turn sequence
                PanelTurnLost.Location = new Point(325, 331);
                lblTurnLostTopMessage.Text = ParseCharacterIDMessage(activePlayerRef.CharID + "'s Turn");
                lblTurnLostMessage.Text = "Current Turns Lost Left: " + activePlayerRef.TurnsLost;
                if (activePlayerID == Player.Player1) { lblTurnLostDownMessage.Text = "Press J to end turn"; }
                else { lblTurnLostDownMessage.Text = "CPU taking action"; }
                PanelTurnLost.Visible = true;

                activePlayerRef.TurnsLost--;

                //Change Staste if player is active
                if (activePlayerID == Player.Player1)
                {
                    currentState = GameState.TurnLost;
                }
                else
                {
                    //AI action pess J to continue
                    WaitNSeconds(1000);
                    SelectCommand_TurnLost();
                }
            }
            else
            {
                if (activePlayerRef.IsDead && activePlayerRef.HasAnElixir())
                {
                    //Make active player use an elixir
                    PanelUseElixir.Location = new Point(460, 357);
                    lblUseElixir.Text = ParseCharacterIDMessage(activePlayerRef.CharID + "'s Turn");
                    if (activePlayerID == Player.Player1) { lblUseElixirLowerMessage.Text = "J - Revive and end turn."; }
                    else { lblUseElixirLowerMessage.Text = "CPU taking action"; }
                    PanelUseElixir.Visible = true;

                    //Change Staste if player is active
                    if (activePlayerID == Player.Player1)
                    {
                        currentState = GameState.UseElixir;
                    }
                    else
                    {
                        //AI action Press J to use
                        WaitNSeconds(1500);
                        SelectCommand_UseElixir();
                    }
                }
                else
                {
                    PanelPlayerAction.Location = new Point(360, 326);
                    currentActionSelection = ActionSelection.RollDice;
                    PanelRollDice.BackColor = Color.DarkRed;
                    PanelUseItem.BackColor = Color.Transparent;
                    PanelActionBattle.BackColor = Color.Transparent;
                    lblActivePlayerTopMessage.Text = ParseCharacterIDMessage("Active Player: " + activePlayerRef.CharID + "(" + activePlayerID + ")");
                    lblPLayerActionName.Text = ParseCharacterIDMessage(activePlayerRef.CharID + "'s Turn");
                    if (activePlayerID == Player.Player1) { lblActionSelectLowerMessage.Text = "AD - Move Selector     J - Select"; }
                    else { lblActionSelectLowerMessage.Text = "CPU taking action"; }
                    PanelPlayerAction.Visible = true;

                    //Change Staste if player is active
                    if (activePlayerID == Player.Player1)
                    {
                        currentState = GameState.PlayerActionSelection;
                    }
                    else
                    {
                        //AI action (decide to roll or to use items  or battle)

                        //Determine CPU player is on a Cell with a monter/dragon
                        bool willbattle = false;
                        if (activePlayerRef.CurrentCasilla.type == "Cell" && activePlayerRef.CurrentCasilla.isMonsterInIt)
                        {
                            willbattle = true;
                        }
                        else if (activePlayerRef.CurrentCasilla.type == "Tower" && activePlayerRef.CurrentCasilla.isMonsterInIt)
                        {
                            //if player has no treasures, he cannot fight the dragon
                            if (activePlayerRef.TreasuresOnHold > 0)
                            {
                                willbattle = true;
                            }
                        }

                        if (willbattle)
                        {
                            //Move the selection to battle and select it
                            WaitNSeconds(1000);
                            RightCommand_PlayerActionSelection();
                            WaitNSeconds(1000);
                            RightCommand_PlayerActionSelection();
                            WaitNSeconds(1000);
                            SelectCommand_PlayerActionSelection();
                        }
                        else if (activePlayerRef.HasATeletranspotation())
                        {
                            //If the player has at least 2 treasures (1 of them is the teletransportatioin itself)
                            if(activePlayerRef.TreasuresOnHold >= 2)
                            {
                                //For now, generate the Casilla id of a unoccupied cell with a monster and go there.
                                int casillaID = -1;

                                for (int x = 0; x < CasillasDB.Count; x++)
                                {
                                    int index = Rand.Range(0, CasillasDB.Count);
                                    if (!CasillasDB[index].IsOccupied && CasillasDB[index].isMonsterInIt)
                                    {
                                        casillaID = CasillasDB[index].id; break;
                                    }
                                }

                                //Go to that casilla
                                if (casillaID != -1)
                                {
                                    cpuTransportCasillaTarget = casillaID;
                                    WaitNSeconds(1000);
                                    RightCommand_PlayerActionSelection();
                                    WaitNSeconds(1000);
                                    SelectCommand_PlayerActionSelection();
                                }
                            }
                            else
                            {
                                //Roll dice instead
                                WaitNSeconds(1500);
                                SelectCommand_PlayerActionSelection();
                            }                           
                        }
                        else
                        {
                            //Select  action 
                            WaitNSeconds(1500);
                            SelectCommand_PlayerActionSelection();
                        }
                    }
                }
            }
        }
        private void TurnEnds()
        {
            //Vanish arrows
            VanishArrowsAndBanner();

            bool nextPlayerNotFound = true;
            while(nextPlayerNotFound)
            {
                //Change the turn iterator
                if (turnIterator == playerList.Count - 1)
                {
                    turnIterator = 0;
                    //Move the next round counter
                    roundNumber++;
                    lblRoundCounter.Text = "Round: " + roundNumber;
                }
                else
                {
                    turnIterator++;
                }

                //Change the active player
                activePlayerID = turnOrderList[turnIterator];
                activePlayerRef = playerList[(int)activePlayerID];

                if(!defeatedPlayers.Contains(activePlayerID))
                {
                    nextPlayerNotFound = false;
                }
            }

            if(activePlayerID != Player.Player1)
            {
                currentState = GameState.AIRunning;
            }

            //Do a small delay before next player's turn
            UpdateDebugOutput();
            WaitNSeconds(1000);

            //Start a new turn
            TurnStart();
            
        }
        private void MovePlayer(Direction _d)
        {
            //Vanish arrows
            VanishArrowsAndBanner();

            //Before moving, remove player as an occupant of the current casilla
            activePlayerRef.CurrentCasilla.RemoveOccupant(activePlayerID);

            //Move
            //Set the next cord location
            Casilla _NextCasilla = GetPlayerNextCasilla(_d);
            Point _NextLocation = _NextCasilla.GetCordinates();
            if (_NextCasilla.type == "Great Hall")
            {
                switch (activePlayerRef.CharID)
                {
                    case CharacterID.Warrior1: _NextLocation = new Point(497, 370); break;
                    case CharacterID.Warrior2: _NextLocation = new Point(596, 472); break;
                    case CharacterID.Dwarf1: _NextLocation = new Point(596, 371); break;
                    case CharacterID.Dwarf2: _NextLocation = new Point(497, 472); break;
                    case CharacterID.Mage1: _NextLocation = new Point(497, 422); break;
                    case CharacterID.Mage2: _NextLocation = new Point(596, 422); break;
                    case CharacterID.Elf1: _NextLocation = new Point(546, 370); break;
                    case CharacterID.Elf2: _NextLocation = new Point(546, 472); break;
                }
            }

            //Move the sprite to the next location
            activePlayerRef.Sprite.Location = _NextLocation;

            //Update casillas
            activePlayerRef.PreviousPreviousCasilla = activePlayerRef.PreviousCasilla;
            activePlayerRef.PreviousCasilla = activePlayerRef.CurrentCasilla;
            activePlayerRef.CurrentCasilla = _NextCasilla;

            //Update previous direction
            switch (_d)
            {
                case Direction.North: activePlayerRef.PreviousDirection = Direction.South; break;
                case Direction.South: activePlayerRef.PreviousDirection = Direction.North; break;
                case Direction.East: activePlayerRef.PreviousDirection = Direction.West; break;
                case Direction.West: activePlayerRef.PreviousDirection = Direction.East; break;
            }

            //Now add player as occupant of the current (new) casilla
            activePlayerRef.CurrentCasilla.AddOccupant(activePlayerID);

            //Reduce left spaces
            spacesToMove--;

            if (spacesToMove == 0)
            {
                //Vanish the Monster preview in case there was any on
                PicMonsterPreviewNorth.Visible = false;
                PicMonsterPreviewSouth.Visible = false;
                PicMonsterPreviewEast.Visible = false;
                PicMonsterPreviewWest.Visible = false;

                if (activePlayerRef.CurrentCasilla.type == "Great Hall" && activePlayerRef.RingsOnHold >= ringsTargetAmount)
                {
                    //player wins the game
                    PicWinner.Image = ImageServer.CharacterImage(activePlayerRef.CharID);
                    PanelWinner.Location = new Point(461, 310);
                    PanelWinner.Visible = true;
                    currentState = GameState.GameOver;
                }
                else
                {
                    CheckForBattle();
                }
            }
            else
            {
                CheckForBattle();               
            }
        }
        private void CheckForBattle()
        {
            bool willbattle = false;
            if (activePlayerRef.CurrentCasilla.type == "Cell" && activePlayerRef.CurrentCasilla.isMonsterInIt)
            {
                willbattle = true;
            }
            else if (activePlayerRef.CurrentCasilla.type == "Tower" && activePlayerRef.CurrentCasilla.isMonsterInIt)
            {
                //if player has no treasures, he cannot fight the dragon
                if (activePlayerRef.TreasuresOnHold > 0)
                {
                    willbattle = true;
                }
            }
            
            if(willbattle)
            {
                //trigger the battle with the monster/dragon
                StartBattle();
            }
            else
            {
                if (spacesToMove == 0)
                {
                    TurnEnds();
                }
                else
                {
                    //allow to move again
                    ShowMovementArrowsForPlayer();

                    //If IA, trigger the next move action
                    if (activePlayerID != Player.Player1)
                    {
                        AI_Move();
                    }
                }
            }
        }
        private void StartBattle()
        {
            //Save the current casilla reference and cell/tower
            Casilla _activeCasilla = activePlayerRef.CurrentCasilla;

            //Load components based on tower/cell location
            string _roomType = _activeCasilla.type;
            PanelBattleWindow.BackgroundImage = ImageServer.BattleBackground(_roomType);

            //Get the monster/dragon card and set the cellid
            Card _enemyMonster;
            if (_roomType == "Cell")
            {
                _enemyMonster = monsters[_activeCasilla.cellID];
            }
            else
            {
                _enemyMonster = dragons[_activeCasilla.towerID];
            }

            //Calculate the monster strengh
            switch (activePlayerRef.CharID)
            {
                case CharacterID.Warrior1: enemyMonsterStrenght = _enemyMonster.warriorDamage; break;
                case CharacterID.Warrior2: enemyMonsterStrenght = _enemyMonster.warriorDamage; break;
                case CharacterID.Dwarf1: enemyMonsterStrenght =  _enemyMonster.dwarfDamage; break;
                case CharacterID.Dwarf2: enemyMonsterStrenght =  _enemyMonster.dwarfDamage; break;
                case CharacterID.Mage1: enemyMonsterStrenght = _enemyMonster.mageDamage; break;
                case CharacterID.Mage2: enemyMonsterStrenght = _enemyMonster.mageDamage; break;
                case CharacterID.Elf1: enemyMonsterStrenght = _enemyMonster.elfDamage; break;
                case CharacterID.Elf2: enemyMonsterStrenght = _enemyMonster.elfDamage; break;
            }

            //Display the monster/dragon. If monster is not flipped yet, show the back of the card.
            if (_activeCasilla.monsterUp)
            {
                //Show the monster/dragon
                lblBattleTopMessage.Text = "Battle!";
                PicBattleMonster.BackgroundImage = ImageServer.VerticalCardImage(_enemyMonster.ID);
                lblBattleMonsterName.Text = _enemyMonster.Name;
                lblBattleMonsterStrengh.Text = "Strenght: " + enemyMonsterStrenght;
            }
            else
            {
                //Show the back of the card
                lblBattleTopMessage.Text = "Mistery Card";
                PicBattleMonster.BackgroundImage = ImageServer.BackCard(_enemyMonster.type);
                lblBattleMonsterName.Text = "?";
                lblBattleMonsterStrengh.Text = "";
            }

            //Display the player info
            PicBattlePlayer.BackgroundImage = ImageServer.CharacterImage(activePlayerRef.CharID);
            lblBattlePlayerName.Text = ParseCharacterIDMessage(activePlayerRef.CharID.ToString());
            lblBattlePlayerBonus.Text = "Bonus Points: " + activePlayerRef.BonusPoints;

            //vanish dices pre battle
            lblBattleLowerMessage.Text = "";
            PicBattleDice1.Visible = false;
            PicBattleDice2.Visible = false;

            //Open the battle panel
            PicBattleTreasure.Visible = false;
            lblBattleExtraTreasures.Visible = false;
            PanelBattleWindow.Location = new Point(306, 234);
            PanelBattleWindow.Visible = true;

            //If the monster was facedown, show the flip animation
            if (!_activeCasilla.monsterUp) 
            {
                WaitNSeconds(1000);
                //Show the card
                lblBattleTopMessage.Text = "Battle!";
                PicBattleMonster.BackgroundImage = ImageServer.VerticalCardImage(_enemyMonster.ID);
                lblBattleMonsterName.Text = _enemyMonster.Name;
                if(_enemyMonster.type == "Trampa")
                {
                    lblBattleMonsterStrengh.Text = "";
                }
                else
                {
                    lblBattleMonsterStrengh.Text = "Strenght: " + enemyMonsterStrenght;
                }               

                //Also flip the card on the board
                if (_roomType == "Cell")
                {
                    if (cellsImageList[_activeCasilla.cellID].Size.Width == 55) 
                    { 
                        cellsImageList[_activeCasilla.cellID].BackgroundImage = ImageServer.VerticalCardImage(_enemyMonster.ID);
                    }
                    else 
                    { 
                        cellsImageList[_activeCasilla.cellID].BackgroundImage = ImageServer.HorizontalCardImage(_enemyMonster.ID); 
                    }                  
                }
                else
                {
                    towersImageList[_activeCasilla.towerID].BackgroundImage = ImageServer.VerticalCardImage(_enemyMonster.ID);
                }
                activePlayerRef.CurrentCasilla.MarkMonterFlipped();
            }

            //set action based on the card to battle, if a trap, no battle is done.
            if (_enemyMonster.type == "Trampa")
            {
                lblBattleLowerMessage.Visible = true;
                switch (_enemyMonster.ID)
                {
                    case CardID.Trampa:
                        lblBattleTopMessage.Text = "A Trap! Return to the Main Hall";
                        if (activePlayerID == Player.Player1) { lblBattleLowerMessage.Text = "J - Continue"; ; }
                        else { lblBattleLowerMessage.Text = "CPU is taking action"; }
                        Battle_ShowGainedTreasures(_activeCasilla.cellID, treasures, additionalTreasures[_activeCasilla.cellID]);
                        //remove trap from board
                        cellsImageList[activePlayerRef.CurrentCasilla.cellID].Visible = false;
                        activePlayerRef.CurrentCasilla.isMonsterInIt = false;
                        //Move player sprite to the main hall
                        TransportToCasilla((int)CasillasID.MainHall);
                        //Change Staste if player is active
                        if (activePlayerID == Player.Player1)
                        {
                            currentState = GameState.BattleOver;
                        }
                        else
                        {
                            //AI Press J to continue
                            WaitNSeconds(1000);
                            SelectCommand_BattleOver();
                        }
                        break;
                    case CardID.Jaula:
                        lblBattleTopMessage.Text = "A Jaula! Roll Dice to determine trapped turns";
                        if (activePlayerID == Player.Player1) { lblBattleLowerMessage.Text = "J - Roll Dice"; ; }
                        else { lblBattleLowerMessage.Text = "CPU is taking action"; }
                        PicBattleDice1.BackgroundImage = ImageServer.BigDice1();
                        PicBattleDice1.Visible = true;
                        //remove trap from board
                        cellsImageList[activePlayerRef.CurrentCasilla.cellID].Visible = false;
                        activePlayerRef.CurrentCasilla.isMonsterInIt = false;
                        //Change state
                        if (activePlayerID == Player.Player1)
                        {
                            currentState = GameState.TurnLostDeciderRoll;
                        }
                        else
                        {
                            //AI Press J to Roll dice
                            WaitNSeconds(1000);
                            SelectCommand_TurnLostDeciderRoll();                          
                        }
                        break;
                    case CardID.MonstruoMuerto:
                        lblBattleTopMessage.Text = "Monstruo Muerto! Take the treasure!";
                        if (activePlayerID == Player.Player1) { lblBattleLowerMessage.Text = "J - Continue"; ; }
                        else { lblBattleLowerMessage.Text = "CPU is taking action"; }
                        Battle_ShowGainedTreasures(_activeCasilla.cellID, treasures, additionalTreasures[_activeCasilla.cellID]);
                        //remove trap from board
                        cellsImageList[activePlayerRef.CurrentCasilla.cellID].Visible = false;
                        activePlayerRef.CurrentCasilla.isMonsterInIt = false;
                        //Change Staste if player is active
                        if (activePlayerID == Player.Player1)
                        {
                            currentState = GameState.BattleOver;
                        }
                        else
                        {
                            //AI Press J to continue
                            WaitNSeconds(1000);
                            SelectCommand_BattleOver();
                        }
                        break;
                }
            }
            else
            {
                //Go ahead with the battle
                PicBattleDice1.BackgroundImage = ImageServer.BigDice1();
                PicBattleDice2.BackgroundImage = ImageServer.BigDice2();
                PicBattleDice1.Visible = true;
                PicBattleDice2.Visible = true;
                if (activePlayerID == Player.Player1) { lblBattleLowerMessage.Text = "J - Roll Dice"; ; }
                else { lblBattleLowerMessage.Text = "CPU is taking action"; }
                lblBattleLowerMessage.Visible = true;

                //Change Staste if player is active
                if (activePlayerID == Player.Player1)
                {
                    currentState = GameState.BattleAttackRoll;
                }
                else
                {
                    //AI action (Roll the dice to attack)
                    WaitNSeconds(1000);
                    SelectCommand_BattleAttackRoll();
                }
            }     
        }
        private void Battle_ShowGainedTreasures(int _cellID, List<Card> _treasureList, List<Card> _extraTreasureList)
        {
            //determine main treasure
            Card _gainedTreasure = _treasureList[_cellID];
            PicBattleTreasure.BackgroundImage = ImageServer.VerticalCardImage(_gainedTreasure.ID);
            PicBattleTreasure.Visible = true;

            if(_extraTreasureList.Count > 0)
            {
                lblBattleExtraTreasures.Text = "Extra Treasures: ";
                for (int x = 0; x < _extraTreasureList.Count; x++)
                {
                    lblBattleExtraTreasures.Text += "" + _extraTreasureList[x].Name + ",";
                }               
                lblBattleExtraTreasures.Visible = true;
            }

            //Add treasures to player data
            activePlayerRef.AddTreasure(_gainedTreasure);
            for (int x = 0; x < _extraTreasureList.Count; x++)
            {
                activePlayerRef.AddTreasure(_extraTreasureList[x]);
            }

            //Update info box
            playerBoxList[(int)activePlayerID].UpdateData();
        }
        private void AI_Move()
        {
            //AI action (Select the first move, select 1 direction at random)
            WaitNSeconds(1000);
            if (validDirectionList.Count == 0)
            {
                //finish moving
                spacesToMove = 0;
                CheckForBattle();
            }
            else
            {
                //Get a direction from the list and move

                if (validDirectionList.Contains(activePlayerRef.PreviousDirection))
                {
                    //if the previous direction is the only choice, then do it.
                    if (validDirectionList.Count == 1)
                    {
                        MoveCommand_PlayerMove(activePlayerRef.PreviousDirection);
                    }
                    //Remove the previous direction and pick one of the rest
                    else
                    {
                        validDirectionList.Remove(activePlayerRef.PreviousDirection);
                        int index = Rand.Range(0, validDirectionList.Count);
                        Direction _newDirection = validDirectionList[index];
                        MoveCommand_PlayerMove(_newDirection);
                    }
                }
                else
                {
                    //pick one at random
                    int index = Rand.Range(0, validDirectionList.Count);
                    Direction _newDirection = validDirectionList[index];
                    MoveCommand_PlayerMove(_newDirection);
                }
            }
        }
        private int AI_GetTreasureSelectionIndex(List<CardID> _treasureList)
        {
            //if only 1 treasure just select that one
            if(_treasureList.Count == 1)
            {
                return 0;
            }
            //remove items until there is only once
            else
            {
                if (_treasureList.Contains(CardID.GemaAmarilla)) { return _treasureList.IndexOf(CardID.GemaAmarilla); }
                else if (_treasureList.Contains(CardID.GemaAzul)) { return _treasureList.IndexOf(CardID.GemaAzul); }
                else if (_treasureList.Contains(CardID.GemaRoja)) { return _treasureList.IndexOf(CardID.GemaRoja); }
                else if (_treasureList.Contains(CardID.GemaVerde)) { return _treasureList.IndexOf(CardID.GemaVerde); }
                else if (_treasureList.Contains(CardID.Teletransportacion)) { return _treasureList.IndexOf(CardID.Teletransportacion); }
                else if (_treasureList.Contains(CardID.Elixir)) { return _treasureList.IndexOf(CardID.Elixir); }
                else if (_treasureList.Contains(CardID.Velocidad)) { return _treasureList.IndexOf(CardID.Velocidad); }
                else if (_treasureList.Contains(CardID.MedallonMagico)) { return _treasureList.IndexOf(CardID.MedallonMagico); }
                else if (_treasureList.Contains(CardID.LlaveMagica)) { return _treasureList.IndexOf(CardID.LlaveMagica); }
                else if (_treasureList.Contains(CardID.BaculoMagico)) { return _treasureList.IndexOf(CardID.BaculoMagico); }
                else if (_treasureList.Contains(CardID.EspadaMagica)) { return _treasureList.IndexOf(CardID.EspadaMagica); }
                else if (_treasureList.Contains(CardID.BallestaMagica)) { return _treasureList.IndexOf(CardID.BallestaMagica); }
                else if (_treasureList.Contains(CardID.EstrellaMagica)) { return _treasureList.IndexOf(CardID.EstrellaMagica); }
                else if (_treasureList.Contains(CardID.GuanteMagico)) { return _treasureList.IndexOf(CardID.GuanteMagico); }
                else if (_treasureList.Contains(CardID.HachaMagica)) { return _treasureList.IndexOf(CardID.HachaMagica); }
                else  { return _treasureList.IndexOf(CardID.AnilloMagico); }
            }           
        }
        private void AI_MoveTransportMarkerToTarget()
        {
            GenerateAIMoveListToTransportTarget(Direction.North);
            GenerateAIMoveListToTransportTarget(Direction.South);
            GenerateAIMoveListToTransportTarget(Direction.East);
            GenerateAIMoveListToTransportTarget(Direction.West);
            GenerateAIMoveListToTransportTarget(Direction.North);
            GenerateAIMoveListToTransportTarget(Direction.South);
            GenerateAIMoveListToTransportTarget(Direction.East);
            GenerateAIMoveListToTransportTarget(Direction.West);

            for (int x = 0; x < finalMoveMarkerList.Count; x++)
            {
                WaitNSeconds(1000);
                MoveCommand_TransportMarkerActive(finalMoveMarkerList[x]);
            }

            firstMarkListSet = false;
            finalMoveMarkerList.Clear();
        }
        #endregion

        #region Data
        //Gameplay data
        private int ringsTargetAmount = -1;
        private int roundNumber = 1;
        //Databases
        private List<Card> CardDB = new List<Card>();
        private List<Casilla> CasillasDB = new List<Casilla>();

        //Player list data
        private List<PlayerData> playerList = new List<PlayerData>();
        private List<PlayerInfoBox> playerBoxList = new List<PlayerInfoBox>();
        private Player activePlayerID;
        private PlayerData activePlayerRef;
        private List<Player> turnOrderList = new List<Player>();
        private int turnIterator = 0;
        private List<Player> defeatedPlayers = new List<Player>();

        //Active Player turn action data
        private int spacesToMove = 0;
        private Direction savedDirection = Direction.North;
        private List<Direction> validDirectionList = new List<Direction>();

        //Battle Data
        private int enemyMonsterStrenght = -1;

        //Board Cards data
        private List<Card> dragons = new List<Card>();
        private List<Card> dragonTreasure = new List<Card>();
        private List<Card> monsters = new List<Card>();
        private List<Card> treasures = new List<Card>();
        private List<Card>[] additionalTreasuresOnDragon = new List<Card>[6];
        private List<Card>[] additionalTreasures = new List<Card>[42];
        private List<PictureBox> towersImageList = new List<PictureBox>();
        private List<PictureBox> cellsImageList = new List<PictureBox>();

        //Game state data
        private GameState currentState = GameState.PlayerSelect;

        //Player Select Panel data
        private CharacterID currentPlayerSelectSelection = CharacterID.Warrior1;
        private CharacterID oldPlayerSelectSelection = CharacterID.Warrior1;
        private CharacterID mainPlayerSelection = CharacterID.None;
        private List<CharacterID> cpuList = new List<CharacterID>();

        //Player Action selection data
        private ActionSelection currentActionSelection = ActionSelection.RollDice;

        //Treasure selection data
        private int currentTreasureSelectionIndex = 0;
        private string battleCellType = "";
        private int BattlecelltowerID = -1;

        //Transporter Mark data
        private int cpuTransportCasillaTarget = -1;
        private List<int> casillasAlreadyChecked = new List<int>();
        private List<Direction> finalMoveMarkerList = new List<Direction>();
        private bool firstMarkListSet = false;
        #endregion

        #region Input Commands
        private void LeftCommand_PlayerSelect()
        {
            if (currentPlayerSelectSelection > 0)
            {
                oldPlayerSelectSelection = currentPlayerSelectSelection;

                if (currentPlayerSelectSelection == CharacterID.Warrior1)
                {
                    currentPlayerSelectSelection = CharacterID.Elf2;
                }
                else
                {
                    currentPlayerSelectSelection--;
                }

                UpdatePlayerSelectSelection();
            }            
        }
        private void RightCommand_PlayerSelect()
        {
            if(currentPlayerSelectSelection > 0)
            {
                oldPlayerSelectSelection = currentPlayerSelectSelection;

                if (currentPlayerSelectSelection == CharacterID.Elf2)
                {
                    currentPlayerSelectSelection = CharacterID.Warrior1;
                }
                else
                {
                    currentPlayerSelectSelection++;
                }

                UpdatePlayerSelectSelection();
            }
        }
        private void DownCommand_PlayerSelect()
        {
            if(currentPlayerSelectSelection > 0)
            {
                if (mainPlayerSelection != CharacterID.None && cpuList.Count > 0)
                {
                    oldPlayerSelectSelection = currentPlayerSelectSelection;
                    currentPlayerSelectSelection = CharacterID.None;
                }

                UpdatePlayerSelectSelection();
            }
        }
        private void UpCommand_PlayerSelect()
        {
            if(currentPlayerSelectSelection == CharacterID.None)
            {
                oldPlayerSelectSelection = currentPlayerSelectSelection;
                currentPlayerSelectSelection = CharacterID.Warrior1;
            }

            UpdatePlayerSelectSelection();
        }
        private void SelectCommand_PlayerSelect()
        {
            switch (currentPlayerSelectSelection)
            {
                case CharacterID.Warrior1: UpdatePlayerSelectLabelOption(lblWarrior1Select); break;
                case CharacterID.Warrior2: UpdatePlayerSelectLabelOption(lblWarrior2Select); break;
                case CharacterID.Dwarf1: UpdatePlayerSelectLabelOption(lblDwarf1Select); break;
                case CharacterID.Dwarf2: UpdatePlayerSelectLabelOption(lblDwarf2Select); break;
                case CharacterID.Mage1: UpdatePlayerSelectLabelOption(lblMage1Select); break;
                case CharacterID.Mage2: UpdatePlayerSelectLabelOption(lblMage2Select); break;
                case CharacterID.Elf1: UpdatePlayerSelectLabelOption(lblElf1Select); break;
                case CharacterID.Elf2: UpdatePlayerSelectLabelOption(lblElf2Select); break;
                case CharacterID.None:
                    //Start the game
                    //Initialize Players 
                    Rand.Shuffle(cpuList);
                    playerList.Add(new PlayerData(mainPlayerSelection, CasillasDB[(int)CasillasID.MainHall], Player.Player1));

                    for (int x = 0; x < cpuList.Count; x++)
                    {
                        playerList.Add(new PlayerData(cpuList[x], CasillasDB[(int)CasillasID.MainHall], (Player)x + 1));
                    }              
                    LoadActivePlayers();

                    //Load Player info boxes
                    for(int x = 0; x < playerList.Count; x++)
                    {
                        playerBoxList.Add(new PlayerInfoBox(playerList[x], this));
                    }

                    //Close the Player Selection panel and chage the game state
                    PanelPlayerSelector.Visible = false;

                    //Open the Turn decider
                    PanelTurnOrderDecider.Location = new Point(380, 378);
                    PanelTurnOrderDecider.BringToFront();
                    PanelTurnOrderDecider.Visible = true;

                    currentState = GameState.TurnOrderDecide;
                    break;
            }
        }
        private void SelectCommand_TurnDecider()
        {
            //Make a duplicate of the player list and shuffle it
            List<PlayerData> tmp = new List<PlayerData>();
            for (int x = 0; x < playerList.Count; x++)
            {
                tmp.Add(playerList[x]);
            }

            Rand.Shuffle(tmp);

            for (int x = 0; x < tmp.Count; x++)
            {
                turnOrderList.Add(tmp[x].PlayerID);
            }

            //show order
            switch(turnOrderList.Count)
            {
                case 2:
                    PicTurn4.Visible = true;
                    PicTurnArrow4.Visible = true;
                    PicTurn4.Image = ImageServer.CharacterImage(tmp[0].CharID);
                    PicTurn5.Visible = true;
                    PicTurn5.Image = ImageServer.CharacterImage(tmp[1].CharID);
                    break;
                case 3:
                    PicTurn4.Visible = true;
                    PicTurnArrow4.Visible = true;
                    PicTurn4.Image = ImageServer.CharacterImage(tmp[0].CharID);
                    PicTurn5.Visible = true;
                    PicTurnArrow5.Visible = true;
                    PicTurn5.Image = ImageServer.CharacterImage(tmp[1].CharID);
                    PicTurn6.Visible = true;
                    PicTurn6.Image = ImageServer.CharacterImage(tmp[2].CharID);
                    break;
                case 4:
                    PicTurn3.Visible = true;
                    PicTurnArrow3.Visible = true;
                    PicTurn3.Image = ImageServer.CharacterImage(tmp[0].CharID);
                    PicTurn4.Visible = true;
                    PicTurnArrow4.Visible = true;
                    PicTurn4.Image = ImageServer.CharacterImage(tmp[1].CharID);
                    PicTurn5.Visible = true;
                    PicTurnArrow5.Visible = true;
                    PicTurn5.Image = ImageServer.CharacterImage(tmp[2].CharID);
                    PicTurn6.Visible = true;
                    PicTurn6.Image = ImageServer.CharacterImage(tmp[3].CharID);
                    break;
                case 5:
                    PicTurn3.Visible = true;
                    PicTurnArrow3.Visible = true;
                    PicTurn3.Image = ImageServer.CharacterImage(tmp[0].CharID);
                    PicTurn4.Visible = true;
                    PicTurnArrow4.Visible = true;
                    PicTurn4.Image = ImageServer.CharacterImage(tmp[1].CharID);
                    PicTurn5.Visible = true;
                    PicTurnArrow5.Visible = true;
                    PicTurn5.Image = ImageServer.CharacterImage(tmp[2].CharID);
                    PicTurn6.Visible = true;
                    PicTurnArrow6.Visible = true;
                    PicTurn6.Image = ImageServer.CharacterImage(tmp[3].CharID);
                    PicTurn7.Visible = true;
                    PicTurn7.Image = ImageServer.CharacterImage(tmp[4].CharID);
                    break;
                case 6:
                    PicTurn2.Visible = true;
                    PicTurnArrow2.Visible = true;
                    PicTurn2.Image = ImageServer.CharacterImage(tmp[0].CharID);
                    PicTurn3.Visible = true;
                    PicTurnArrow3.Visible = true;
                    PicTurn3.Image = ImageServer.CharacterImage(tmp[1].CharID);
                    PicTurn4.Visible = true;
                    PicTurnArrow4.Visible = true;
                    PicTurn4.Image = ImageServer.CharacterImage(tmp[2].CharID);
                    PicTurn5.Visible = true;
                    PicTurnArrow5.Visible = true;
                    PicTurn5.Image = ImageServer.CharacterImage(tmp[3].CharID);
                    PicTurn6.Visible = true; 
                    PicTurnArrow6.Visible = true;
                    PicTurn6.Image = ImageServer.CharacterImage(tmp[4].CharID);
                    PicTurn7.Visible = true;
                    PicTurn7.Image = ImageServer.CharacterImage(tmp[5].CharID);
                    break;
                case 7:
                    PicTurn2.Visible = true;
                    PicTurnArrow2.Visible = true;
                    PicTurn2.Image = ImageServer.CharacterImage(tmp[0].CharID);
                    PicTurn3.Visible = true;
                    PicTurnArrow3.Visible = true;
                    PicTurn3.Image = ImageServer.CharacterImage(tmp[1].CharID);
                    PicTurn4.Visible = true;
                    PicTurnArrow4.Visible = true;
                    PicTurn4.Image = ImageServer.CharacterImage(tmp[2].CharID);
                    PicTurn5.Visible = true;
                    PicTurnArrow5.Visible = true;
                    PicTurn5.Image = ImageServer.CharacterImage(tmp[3].CharID);
                    PicTurn6.Visible = true;
                    PicTurnArrow6.Visible = true;
                    PicTurn6.Image = ImageServer.CharacterImage(tmp[4].CharID);
                    PicTurn7.Visible = true;
                    PicTurnArrow7.Visible = true;
                    PicTurn7.Image = ImageServer.CharacterImage(tmp[5].CharID);
                    PicTurn8.Visible = true;
                    PicTurn8.Image = ImageServer.CharacterImage(tmp[6].CharID);
                    break;
                case 8:
                    PicTurn1.Visible = true;
                    PicTurnArrow1.Visible = true;
                    PicTurn1.Image = ImageServer.CharacterImage(tmp[0].CharID);
                    PicTurn2.Visible = true;
                    PicTurnArrow2.Visible = true;
                    PicTurn2.Image = ImageServer.CharacterImage(tmp[1].CharID);
                    PicTurn3.Visible = true;
                    PicTurnArrow3.Visible = true;
                    PicTurn3.Image = ImageServer.CharacterImage(tmp[2].CharID);
                    PicTurn4.Visible = true;
                    PicTurnArrow4.Visible = true;
                    PicTurn4.Image = ImageServer.CharacterImage(tmp[3].CharID);
                    PicTurn5.Visible = true;
                    PicTurnArrow5.Visible = true;
                    PicTurn5.Image = ImageServer.CharacterImage(tmp[4].CharID);
                    PicTurn6.Visible = true;
                    PicTurnArrow6.Visible = true;
                    PicTurn6.Image = ImageServer.CharacterImage(tmp[5].CharID);
                    PicTurn7.Visible = true;
                    PicTurnArrow7.Visible = true;
                    PicTurn7.Image = ImageServer.CharacterImage(tmp[6].CharID);
                    PicTurn8.Visible = true;
                    PicTurn8.Image = ImageServer.CharacterImage(tmp[7].CharID);
                    break;
            }

            //Set the active player
            activePlayerID = turnOrderList[0];
            activePlayerRef = playerList[(int)activePlayerID];

            //Update the top message
            UpdateTurnOrderTopMesage();

            //Change message
            lblTurnDeciderMessage.Text = "Press J to Continue";

            //Change State
            currentState = GameState.TurnDeciderClose;
        }
        private void SelectCommand_TurnDeciderOver()
        {
            //Close the turn decider and start with the first player
            PanelTurnOrderDecider.Visible = false;

            playerBoxList[0].UpdateData();

            TurnStart();
        }
        private void LeftCommand_PlayerActionSelection()
        {
            //change the selection
            if(currentActionSelection == ActionSelection.RollDice)
            {
                currentActionSelection = ActionSelection.Battle;
            }
            else
            {
                currentActionSelection--;
            }

            PanelRollDice.BackColor = Color.Transparent;
            PanelUseItem.BackColor = Color.Transparent;
            PanelActionBattle.BackColor = Color.Transparent;
            //Update the selector
            switch (currentActionSelection)
            {
                case ActionSelection.RollDice: PanelRollDice.BackColor = Color.DarkRed; break;
                case ActionSelection.UseItem: PanelUseItem.BackColor = Color.DarkRed; break;
                case ActionSelection.Battle: PanelActionBattle.BackColor = Color.DarkRed;  break;
            }
        }
        private void RightCommand_PlayerActionSelection()
        {
            //change the selection
            if (currentActionSelection == ActionSelection.Battle)
            {
                currentActionSelection = ActionSelection.RollDice;
            }
            else
            {
                currentActionSelection++;
            }

            PanelRollDice.BackColor = Color.Transparent;
            PanelUseItem.BackColor = Color.Transparent;
            PanelActionBattle.BackColor = Color.Transparent;
            //Update the selector
            switch (currentActionSelection)
            {
                case ActionSelection.RollDice: PanelRollDice.BackColor = Color.DarkRed; break;
                case ActionSelection.UseItem: PanelUseItem.BackColor = Color.DarkRed; break;
                case ActionSelection.Battle: PanelActionBattle.BackColor = Color.DarkRed; break;
            }
        }
        private void SelectCommand_PlayerActionSelection()
        {
            switch(currentActionSelection)
            {
                case ActionSelection.RollDice:
                    //Switch panels
                    PanelPlayerAction.Visible = false;
                    PicDiceRoll1.BackgroundImage = ImageServer.BigDice1();
                    PicDiceRoll2.BackgroundImage = ImageServer.BigDice2();
                    lblRollDiceMessage.Text = "Roll Dice!";
                    if (activePlayerID == Player.Player1) { lblRollDiceToMoveLowerMessage.Text = "J - Roll Dice"; }
                    else { lblRollDiceToMoveLowerMessage.Text = "CPU is taking action"; }
                    PanelRollDiceToMove.Location = new Point(404, 303);
                    lblRollDiceToMoveTopMessage.Text = ParseCharacterIDMessage(activePlayerRef.CharID + "'s turn!");
                    PanelRollDiceToMove.Visible = true;

                    //Change Staste if player is active
                    if (activePlayerID == Player.Player1)
                    {
                        currentState = GameState.ActionRollDiceToMove;
                    }
                    else
                    {
                        //AI action (Roll the dice)
                        WaitNSeconds(2000);
                        SelectCommand_ActionRollDiceToMove();
                    }
                    break;

                case ActionSelection.UseItem:
                    //Check for items, if player has no comsumables them show error
                    if (activePlayerRef.HasATeletranspotation())
                    {
                        PanelPlayerAction.Visible = false;

                        TransportMarker.Appear();
                        ShowMovementArrowsForTransportMarker();

                        if(activePlayerID == Player.Player1)
                        {
                            //Change the state
                            currentState = GameState.TransportMarkerActive;
                        }
                        else
                        {
                            //have the AI move the marker to the target casilla
                            WaitNSeconds(1500);
                            AI_MoveTransportMarkerToTarget();
                            SelectCommand_TransportMarkerActive();
                        }                       
                    }
                    else
                    {
                        //show error
                        lblActionSelectLowerMessage.Text = "No Teletransportation on inventory!!";
                        WaitNSeconds(1000);
                        lblActionSelectLowerMessage.Text = "AD - Move Selection       J - Select";
                    }
                    break;

                case ActionSelection.Battle:
                    //Check if the player is in a cell/tower with a monster/dragon
                    bool willbattle = false;
                    bool dragonNoTreasure = false;
                    if (activePlayerRef.CurrentCasilla.type == "Cell" && activePlayerRef.CurrentCasilla.isMonsterInIt)
                    {
                        willbattle = true;
                    }
                    else if (activePlayerRef.CurrentCasilla.type == "Tower" && activePlayerRef.CurrentCasilla.isMonsterInIt)
                    {
                        //if player has no treasures, he cannot fight the dragon
                        if (activePlayerRef.TreasuresOnHold > 0)
                        {
                            willbattle = true;
                        }
                        else
                        {
                            dragonNoTreasure = true;
                        }
                    }

                    if (willbattle)
                    {
                        //trigger the battle with the monster/dragon
                        PanelPlayerAction.Visible = false;
                        StartBattle();
                    }
                    else
                    {
                        //show error
                        if (dragonNoTreasure) { lblActionSelectLowerMessage.Text = "Cannot fight Dragon without Treasures!!"; }
                        else { lblActionSelectLowerMessage.Text = "Not Monster/Dragon on current space!!"; }
                        WaitNSeconds(1000);
                        lblActionSelectLowerMessage.Text = "AD - Move Selection       J - Select";
                    }
                    break;
            }
        }
        private void SelectCommand_ActionRollDiceToMove()
        {
            //roll the dice
            int _diceResult1 = Rand.Range(1, 7);
            int _diceResult2 = Rand.Range(1, 7);
            //int _diceResult1 = 1;
            //int _diceResult2 = 1;

            for (int x = 0; x < 10; x++)
            {
                PicDiceRoll1.BackgroundImage = ImageServer.Diceface(Rand.Range(1,7));
                PicDiceRoll2.BackgroundImage = ImageServer.Diceface(Rand.Range(1,7));
                WaitNSeconds(100);
            }

            //Show results
            PicDiceRoll1.BackgroundImage = ImageServer.Diceface(_diceResult1);
            PicDiceRoll2.BackgroundImage = ImageServer.Diceface(_diceResult2);
            if (activePlayerID == Player.Player1) { lblRollDiceToMoveLowerMessage.Text = "Press J to move"; }
            else { lblRollDiceToMoveLowerMessage.Text = "CPU is taking action"; }

            //IF velocidad active
            int _bonusMultliplier = 1;
            if(activePlayerRef.HasValocidad())
            {
                lblRollDiceMessage.Text = "Roll Dice! - Velocidad bonus: x2!!";
                _bonusMultliplier = 2;
            }

            //save data
            spacesToMove = (_diceResult1 + _diceResult2)*_bonusMultliplier;

            //Change Staste if player is active
            if (activePlayerID == Player.Player1)
            {
                currentState = GameState.ActionRollDiceToMoveFinish;
            }
            else
            {
                //AI action (press J to move)
                WaitNSeconds(2000);
                SelectCommand_ActionRollDiceToMoveFinish();
            }
        }
        private void SelectCommand_ActionRollDiceToMoveFinish()
        {
            //Close the roll dice panel
            PanelRollDiceToMove.Visible = false;

            //Show the moving arrows on player
            ShowMovementArrowsForPlayer();

            //Change Staste if player is active
            if (activePlayerID == Player.Player1)
            {
                currentState = GameState.PlayerMove;
            }
            else
            {
                //AI action (Select the first move, select 1 direction at random)
                AI_Move();
            }
        }
        private void MoveCommand_PlayerMove(Direction _d)
        {
            bool goAhead = true;
            //Validate direction for player 1
            if (activePlayerID == Player.Player1)
            {
                if(!validDirectionList.Contains(_d))
                {
                    goAhead = false;
                }
            }

            if(goAhead)
            {
                PicMonsterPreviewNorth.Visible = false;
                PicMonsterPreviewSouth.Visible = false;
                PicMonsterPreviewEast.Visible = false;
                PicMonsterPreviewWest.Visible = false;

                if (activePlayerRef.CurrentCasilla.GetDoor(_d) == "Locked" && !activePlayerRef.HasKey())
                {
                    //Attempt to open the door
                    PicLockedDoorRoll.BackgroundImage = ImageServer.BigDice1();
                    PanelOpenLockedDoorRoll.Location = new Point(491, 364);
                    if (activePlayerID == Player.Player1) { lblLockedRollMessage.Text = "J - Roll Dice   I - Cancel"; }
                    else { lblLockedRollMessage.Text = "CPU acting"; }
                    PanelOpenLockedDoorRoll.Visible = true;

                    //Save the direction for the rool
                    savedDirection = _d;

                    //Change Staste if player is active
                    if (activePlayerID == Player.Player1)
                    {
                        currentState = GameState.LockedDoorRoll;
                    }
                    else
                    {
                        //AI action (Roll the dice)
                        WaitNSeconds(2000);
                        SelectCommand_LockedDoorRoll();
                    }
                }
                else if (activePlayerRef.CurrentCasilla.GetDoor(_d) != "Wall")
                {
                    //Move player and reduce spaces left by 1 
                    MovePlayer(_d);
                }
            }
        }
        private void SelectCommand_LockedDoorRoll()
        {
            int _diceResult1 = Rand.Range(1, 6);

            for (int x = 0; x < 5; x++)
            {
                PicLockedDoorRoll.BackgroundImage = ImageServer.Diceface(Rand.Range(1, 7));
                WaitNSeconds(100);
            }

            //Show results
            PicLockedDoorRoll.BackgroundImage = ImageServer.Diceface(_diceResult1);
            WaitNSeconds(1000);
            PanelOpenLockedDoorRoll.Visible = false;

            if(_diceResult1 < 4)
            {
                //Change Staste if player is active
                if (activePlayerID == Player.Player1)
                {
                    currentState = GameState.PlayerMove;
                }

                //Move
                MovePlayer(savedDirection);
            }
            else
            {
                TurnEnds();
            }
        }
        private void CancelCommand_LockedDoorRoll()
        {
            PanelOpenLockedDoorRoll.Visible = false;
            currentState = GameState.PlayerMove;
        }
        private void SelectCommand_BattleAttackRoll()
        {
            //roll the dice

            int _diceResult1 = Rand.Range(1, 7);
            int _diceResult2 = Rand.Range(1, 7);
            //int _diceResult1 = 1;
            //int _diceResult2 = 1;

            int _totalAttack = _diceResult1 + _diceResult2;

            //Add any additional bonus point
            _totalAttack += activePlayerRef.BonusPoints;

            for (int x = 0; x < 10; x++)
            {
                PicBattleDice1.BackgroundImage = ImageServer.Diceface(Rand.Range(1, 7));
                PicBattleDice2.BackgroundImage = ImageServer.Diceface(Rand.Range(1, 7));
                WaitNSeconds(100);
            }

            //Show results
            PicBattleDice1.BackgroundImage = ImageServer.Diceface(_diceResult1);
            PicBattleDice2.BackgroundImage = ImageServer.Diceface(_diceResult2);

            //Small delay to show the result
            lblBattleLowerMessage.Text = "Total Attack Power: " + _totalAttack;
            WaitNSeconds(3000);

            //Determine if win or not
            if (_totalAttack >= enemyMonsterStrenght)
            {
                //Player wins
                lblBattleTopMessage.Text = "Player Wins!! Treasure Won!";
                PicBattleDice1.Visible = false;
                PicBattleDice2.Visible = false;


                //Set which treasure get from and remove monster image from board
                if (activePlayerRef.CurrentCasilla.type == "Cell")
                {
                    activePlayerRef.AddMonsterDefeated(monsters[activePlayerRef.CurrentCasilla.cellID]);
                    Battle_ShowGainedTreasures(activePlayerRef.CurrentCasilla.cellID, treasures, additionalTreasures[activePlayerRef.CurrentCasilla.cellID]);
                    cellsImageList[activePlayerRef.CurrentCasilla.cellID].Visible = false;
                }
                else
                {
                    lblBattleTopMessage.Text += " Ring Obtained!!";
                    activePlayerRef.AddTreasure(CardDB[(int)CardID.AnilloMagico]);
                    activePlayerRef.AddMonsterDefeated(monsters[activePlayerRef.CurrentCasilla.towerID]);
                    Battle_ShowGainedTreasures(activePlayerRef.CurrentCasilla.towerID, dragonTreasure, additionalTreasuresOnDragon[activePlayerRef.CurrentCasilla.towerID]);
                    towersImageList[activePlayerRef.CurrentCasilla.towerID].Visible = false;
                }
                activePlayerRef.CurrentCasilla.isMonsterInIt = false;

                //Change Staste if player is active
                if (activePlayerID == Player.Player1)
                {
                    lblBattleLowerMessage.Text = "J - Finish Turn";
                    currentState = GameState.BattleOver;
                }
                else
                {
                    //AI action (press J to continue and finish the turn)
                    lblBattleLowerMessage.Text = "CPU - Finish Turn";
                    WaitNSeconds(3000);
                    SelectCommand_BattleOver();
                }
            }
            else
            {
                //enemy attacks
                lblBattleTopMessage.Text = "Enemy survived!! Enemy attacks!";

                _diceResult1 = Rand.Range(1, 7);
                _diceResult2 = Rand.Range(1, 7);
                //_diceResult1 = 6;
                //_diceResult2 = 6;
                _totalAttack = _diceResult1 + _diceResult2;

                for (int x = 0; x < 10; x++)
                {
                    PicBattleDice1.BackgroundImage = ImageServer.Diceface(Rand.Range(1, 7));
                    PicBattleDice2.BackgroundImage = ImageServer.Diceface(Rand.Range(1, 7));
                    WaitNSeconds(100);
                }

                //Show results
                PicBattleDice1.BackgroundImage = ImageServer.Diceface(_diceResult1);
                PicBattleDice2.BackgroundImage = ImageServer.Diceface(_diceResult2);

                //Small delay to show the result
                lblBattleLowerMessage.Text = "Enemy Attack: " + _totalAttack;
                WaitNSeconds(3000);

                //Determine action based on result
                if (_totalAttack <= 3)
                {
                    //A salvo, stay on place
                    lblBattleTopMessage.Text = "A salvo! Stay in place!";
                    if (activePlayerID == Player.Player1) { lblBattleLowerMessage.Text = "Press J to continue"; }
                    else { lblBattleLowerMessage.Text = "CPU is taking action"; }

                    //Change Staste if player is active
                    if (activePlayerID == Player.Player1)
                    {
                        currentState = GameState.BattleOver;
                    }
                    else
                    {
                        //AI action (press J to continue and finish the turn)
                        WaitNSeconds(1000);
                        SelectCommand_BattleOver();
                    }
                }
                else if (_totalAttack <= 5)
                {
                    //Retirada, regresa 1 espacio, pierde un turno
                    lblBattleTopMessage.Text = "Retirada! Go back 1 and lose a turn!";

                    //Player loses 1 turn
                    activePlayerRef.TurnsLost = 1;
                    TransportToCasilla(activePlayerRef.PreviousCasilla.id);

                    //Change Staste if player is active
                    if (activePlayerID == Player.Player1)
                    {
                        currentState = GameState.BattleOver;
                    }
                    else
                    {
                        //AI action (press J to continue and finish the turn)
                        WaitNSeconds(1000);
                        SelectCommand_BattleOver();
                    }
                }
                else if (_totalAttack <= 6)
                {
                    //A salvo, stay on place
                    lblBattleTopMessage.Text = "A salvo! Stay in place!";
                    if (activePlayerID == Player.Player1) { lblBattleLowerMessage.Text = "Press J to continue"; }
                    else { lblBattleLowerMessage.Text = "CPU is taking action"; }

                    //Change Staste if player is active
                    if (activePlayerID == Player.Player1)
                    {
                        currentState = GameState.BattleOver;
                    }
                    else
                    {
                        //AI action (press J to continue and finish the turn)
                        WaitNSeconds(1000);
                        SelectCommand_BattleOver();
                    }
                }
                else if (_totalAttack <= 8)
                {
                    //Retirada, regresa 2 espacio, pierde un turno
                    lblBattleTopMessage.Text = "Retirada! Go back 2 and lose a turn!";

                    //Player loses 1 turn
                    activePlayerRef.TurnsLost = 1;
                    TransportToCasilla(activePlayerRef.PreviousPreviousCasilla.id);

                    //Change Staste if player is active
                    if (activePlayerID == Player.Player1)
                    {
                        currentState = GameState.BattleOver;
                    }
                    else
                    {
                        //AI action (press J to continue and finish the turn)
                        WaitNSeconds(1000);
                        SelectCommand_BattleOver();
                    }
                }
                else if (_totalAttack <= 10)
                {
                    //Retirada, regresa 2 espacio, pierde 2 turnos
                    lblBattleTopMessage.Text = "Retirada! Go back 2 and lose 2 turns!";

                    //Player loses 1 turn
                    activePlayerRef.TurnsLost = 2;
                    TransportToCasilla(activePlayerRef.PreviousPreviousCasilla.id);

                    //Change Staste if player is active
                    if (activePlayerID == Player.Player1)
                    {
                        currentState = GameState.BattleOver;
                    }
                    else
                    {
                        //AI action (press J to continue and finish the turn)
                        WaitNSeconds(1000);
                        SelectCommand_BattleOver();
                    }
                }
                else if (_totalAttack <= 11)
                {
                    //Herido, regresa 2 espacio, pierde 2 turnos y 1 tesoro
                    lblBattleTopMessage.Text = "Herido! Go back 2 and lose 2 turns and 1 treasure!";

                    //Player loses 1 turn
                    activePlayerRef.TurnsLost = 2;
                    
                    //Save the cell/tower id where the battle took place.
                    if (activePlayerRef.CurrentCasilla.type == "Cell")
                    {
                        BattlecelltowerID = activePlayerRef.CurrentCasilla.cellID;
                    }
                    else
                    {
                        BattlecelltowerID = activePlayerRef.CurrentCasilla.towerID;
                    }
                    battleCellType = activePlayerRef.CurrentCasilla.type;
                 
                    if(activePlayerRef.TreasuresOnHold == 0)
                    {
                        //player goes to inframundo
                        WaitNSeconds(1500);
                        lblBattleTopMessage.Text = "No treasures left! Player goes to the Inframundo!";
                        TransportToRandomInframundo();

                        //Turn ends change state
                        if (activePlayerID == Player.Player1)
                        {
                            lblBattleLowerMessage.Text = "Press J to Continue.";
                            currentState = GameState.BattleOver;
                        }
                        else
                        {
                            WaitNSeconds(2000);
                            SelectCommand_BattleOver();
                        }
                    }
                    else
                    {
                        TransportToCasilla(activePlayerRef.PreviousPreviousCasilla.id);

                        WaitNSeconds(1500);
                        //Lose a treasure, player gets to pick
                        if (activePlayerID == Player.Player1)
                        {
                            PanelBattleWindow.Visible = false;
                            //Open the treasure selection panel for player
                            OpenTreasureSelectionPanel();

                            //Change State
                            currentState = GameState.TreasureSelection;

                        }
                        else
                        {
                            List<CardID> _localTreasureList = new List<CardID>();
                            for(int x = 0; x < activePlayerRef.TreasuresOnHold; x++)
                            {
                                _localTreasureList.Add(activePlayerRef.TreasureList[x].ID);
                            }

                            int indexOfTreasure = AI_GetTreasureSelectionIndex(_localTreasureList);

                            Card thisTreasure = activePlayerRef.TreasureList[indexOfTreasure];

                            //Remove the treasure of the generated index.
                            activePlayerRef.RemoveTreasure(indexOfTreasure);

                            //add it to the new list
                            if (battleCellType == "Cell")
                            {
                                //add treasure
                                additionalTreasures[BattlecelltowerID].Add(thisTreasure);
                            }
                            else
                            {
                                additionalTreasuresOnDragon[BattlecelltowerID].Add(thisTreasure);
                            }

                            //Update info box
                            playerBoxList[(int)activePlayerID].UpdateData();

                            //end turn now
                            TurnEnds();
                        }
                    }                  
                }
                else
                {
                    //Derrotado, pierde todos los tesoros y ve al inframundo
                    lblBattleTopMessage.Text = "Muerto! Lose all treasures, go to inframundo!";

                    //Player loses 2 turn
                    activePlayerRef.TurnsLost = 2;

                    //Save the cell/tower id where the battle took place.
                    if (activePlayerRef.CurrentCasilla.type == "Cell")
                    {
                        BattlecelltowerID = activePlayerRef.CurrentCasilla.cellID;
                    }
                    else
                    {
                        BattlecelltowerID = activePlayerRef.CurrentCasilla.towerID;
                    }
                    battleCellType = activePlayerRef.CurrentCasilla.type;

                    //Lose all treasures
                    bool elixirSaved = false;
                    for (int x = 0; x < activePlayerRef.TreasuresOnHold; x++)
                    {
                        //remove treasure
                        Card thisTreasure = activePlayerRef.TreasureList[x];
                        if(thisTreasure.ID == CardID.Elixir && !elixirSaved)
                        {
                            //save this elixir
                            elixirSaved = true;
                        }
                        else
                        {
                            activePlayerRef.RemoveTreasure(x);

                            //add it to the new list
                            if (battleCellType == "Cell")
                            {
                                //add treasure
                                additionalTreasures[BattlecelltowerID].Add(thisTreasure);
                            }
                            else
                            {
                                additionalTreasuresOnDragon[BattlecelltowerID].Add(thisTreasure);
                            }

                            //Update info box
                            playerBoxList[(int)activePlayerID].UpdateData();

                            x--;
                        }
                    }


                    //Sent to inframundo
                    TransportToRandomInframundo();


                    //Turn ends change state
                    if (activePlayerID == Player.Player1)
                    {
                        string elixirSavedMessage = "";
                        if (elixirSaved) { elixirSavedMessage = "Elixir was saved!! "; }
                        lblBattleLowerMessage.Text = elixirSavedMessage + "Press J to Continue.";
                        currentState = GameState.BattleOver;
                    }
                    else
                    {
                        WaitNSeconds(2000);
                        SelectCommand_BattleOver();
                    }
                }
            }
        }
        private void SelectCommand_BattleOver()
        {
            //Vanish teh battle board
            PanelBattleWindow.Visible = false;

            if (activePlayerRef.IsDead && !activePlayerRef.HasAnElixir())
            {
                //Player loses the game
                PanelPlayerDead.Location = new Point(460, 342);
                lblPlayerDeadUpperMessage.Text = ParseCharacterIDMessage(activePlayerRef.CharID + " is dead!");
                if (activePlayerID == Player.Player1) { lblPlayerDeadLowerMessage.Text = "Press J to end game."; }
                else { lblPlayerDeadLowerMessage.Text = "CPU taking action"; }
                PanelPlayerDead.Visible = true;

                //Change Staste if player is active
                if (activePlayerID == Player.Player1)
                {
                    currentState = GameState.ExitGame;
                }
                else
                {
                    //AI action pess J to continue
                    WaitNSeconds(1500);
                    SelectCommand_ExitGame();
                }
            }
            else
            {
                //end turn
                TurnEnds();
            }
        }
        private void SelectCommand_TurnLostDeciderRoll()
        {
            //Roll dice and make player lose those turns.
            int _diceResult1 = Rand.Range(1, 6);

            for (int x = 0; x < 10; x++)
            {
                PicBattleDice1.BackgroundImage = ImageServer.Diceface(Rand.Range(1, 7));
                WaitNSeconds(100);
            }

            //save the turns lost on the player data
            activePlayerRef.TurnsLost = _diceResult1;

            //Show results
            PicBattleDice1.BackgroundImage = ImageServer.Diceface(_diceResult1);
            string letterS = "";
            if(_diceResult1 > 1) { letterS = "s"; }
            lblBattleTopMessage.Text = ParseCharacterIDMessage(activePlayerRef.CharID + " lost " +  _diceResult1 + " turn" + letterS + "! - Take the treasure!");

            WaitNSeconds(1500);
            PicBattleDice1.Visible = false;
            //Save the current casilla reference and cell/tower
            Casilla _activeCasilla = activePlayerRef.CurrentCasilla;
            Battle_ShowGainedTreasures(_activeCasilla.cellID, treasures, additionalTreasures[_activeCasilla.cellID]);

            if (activePlayerID == Player.Player1) { lblBattleLowerMessage.Text = "Press J to end turn"; }
            else { lblBattleLowerMessage.Text = "CPU is taking action"; }

            //Change Staste if player is active
            if (activePlayerID == Player.Player1)
            {
                currentState = GameState.BattleOver;
            }
            else
            {
                //AI action (press J to continue and finish the turn)
                WaitNSeconds(1000);
                SelectCommand_BattleOver();
            }
        }
        private void SelectCommand_TurnLost()
        {
            //Close the turn lost panel
            PanelTurnLost.Visible = false;

            TurnEnds();
        }
        private void LeftCommand_TreasureSelect()
        {
            if(currentTreasureSelectionIndex == 0)
            {
                //do nothing
            }
            else
            {
                //move left
                currentTreasureSelectionIndex--;
                UpdateTreasureSelectionList();
            }
        }
        private void RightCommand_TreasureSelect()
        {
            if (currentTreasureSelectionIndex == activePlayerRef.TreasureList.Count -1)
            {
                //do nothing
            }
            else
            {
                //move right
                currentTreasureSelectionIndex++;
                UpdateTreasureSelectionList();
            }
        }
        private void SelectCommand_TreasureSelect()
        {
            //remove this treasure from the active player list and add it to the monster cell additional treasure list

            Card thisTreasure = activePlayerRef.TreasureList[currentTreasureSelectionIndex];

            //remove treasure
            activePlayerRef.RemoveTreasure(currentTreasureSelectionIndex);

            //add it to the new list
            if(battleCellType == "Cell")
            {
                //add treasure
                additionalTreasures[BattlecelltowerID].Add(thisTreasure);
            }
            else
            {
                additionalTreasuresOnDragon[BattlecelltowerID].Add(thisTreasure);
            }

            //Update info box
            playerBoxList[(int)activePlayerID].UpdateData();

            //Vanish panel
            PanelTreasureSelection.Visible = false;

            //end turn now
            TurnEnds();
        }
        private void SelectCommand_ExitGame()
        {
            //vanish the player death panel
            PanelPlayerDead.Visible = false;

            //vanish the player sprite and transport it to the main hall
            activePlayerRef.Sprite.Visible = false;
            TransportToCasilla((int)CasillasID.MainHall);

            defeatedPlayers.Add(activePlayerID);

            //Reload the board upper message to remove player 
            UpdateTurnOrderTopMesage();

            //if there is only 1 player left, that player wins
            if(defeatedPlayers.Count == playerList.Count -1)
            {
                //open the winning panel
                PanelWinner.Location = new Point(461, 310);
                for(int x = 0; x < turnOrderList.Count; x++)
                {
                    if(!defeatedPlayers.Contains(turnOrderList[x]))
                    {
                        PicWinner.Image = ImageServer.CharacterImage(playerList[(int)turnOrderList[x]].CharID);
                    }
                }
                PanelWinner.Visible = true;
                currentState = GameState.GameOver;
            }
            else
            {
                TurnEnds();
            }
        }
        private void SelectCommand_GameOver()
        {
            Application.Exit();
        }
        private void SelectCommand_UseElixir()
        {
            //1 Elixir is remove from the active player 
            PanelUseElixir.Visible = false;
            activePlayerRef.Revives();
            //Remove the only treasure in list
            activePlayerRef.TreasureList.RemoveAt(0);
            TurnEnds();
        }
        private void MoveCommand_TransportMarkerActive(Direction _d)
        {
            if (TransportMarker.CurrentCasilla.GetDoor(_d) != "Wall")
            {
                //Move marker
                Casilla _newCasilla = new Casilla();
                switch(_d)
                {
                    case Direction.North: _newCasilla = CasillasDB[TransportMarker.CurrentCasilla.northCasillaID]; break;
                    case Direction.South: _newCasilla = CasillasDB[TransportMarker.CurrentCasilla.southCasillaID]; break;
                    case Direction.East: _newCasilla = CasillasDB[TransportMarker.CurrentCasilla.eastCasillaID]; break;
                    case Direction.West: _newCasilla = CasillasDB[TransportMarker.CurrentCasilla.westCasillaID]; break;
                }

                TransportMarker.Move(_newCasilla);
                ShowMovementArrowsForTransportMarker();
            }
        }
        private void CancelCommand_TransportMarkerActive()
        {
            //Return to the action command panel
            TransportMarker.Reset();
            PicUpArrow.Visible = false;
            PicDownArrow.Visible = false;
            PicRightArrow.Visible = false;
            PicLeftArrow.Visible = false;
            lblMovementBanner.Visible = false;

            PanelPlayerAction.Visible = true;

            currentState = GameState.PlayerActionSelection;
        }
        private void SelectCommand_TransportMarkerActive()
        {
            //Use 1 teletranport and transport to the 
            int casillaID = TransportMarker.CurrentCasilla.id;

            if(casillaID == (int)CasillasID.MainHall)
            {
                //Cannot go there
                lblMovementBanner.Text = "Cannot select the Main Hall.";
                WaitNSeconds(1500);
                lblMovementBanner.Text = "J - Confirm   I - Cancel";
            }
            else if (CasillasDB[casillaID].IsOccupied)
            {
                //Cannot go there
                lblMovementBanner.Text = "Casilla occupied!!";
                WaitNSeconds(1500);
                lblMovementBanner.Text = "J - Confirm   I - Cancel";
            }
            else
            {
                //Reset and vanish marker objects
                TransportMarker.Reset();
                PicUpArrow.Visible = false;
                PicDownArrow.Visible = false;
                PicRightArrow.Visible = false;
                PicLeftArrow.Visible = false;
                lblMovementBanner.Visible = false;

                //Transport player sprite
                TransportToCasilla(casillaID);

                //Remove teletransport from treasures
                int index = activePlayerRef.GetTreasureIndex(CardID.Teletransportacion);
                activePlayerRef.RemoveTreasure(index);
                playerBoxList[(int)activePlayerID].UpdateData();

                //Just in case set the spaces to move to 0
                spacesToMove = 0;
                CheckForBattle();
            }           
        }
        #endregion     

        #region Controls
        /// <summary>
        /// Controls the Commands of the W Button.
        /// </summary>
        protected virtual void WButtom()
        {
            switch (currentState)
            {
                case GameState.PlayerSelect: UpCommand_PlayerSelect(); break;
                case GameState.PlayerMove: MoveCommand_PlayerMove(Direction.North); break;
                case GameState.TransportMarkerActive: MoveCommand_TransportMarkerActive(Direction.North); break;
            }
        }
        /// <summary>
        /// Controls the Commands of the S Button.
        /// </summary>
        protected virtual void SButtom()
        {
            switch (currentState)
            {
                case GameState.PlayerSelect: DownCommand_PlayerSelect(); break;
                case GameState.PlayerMove: MoveCommand_PlayerMove(Direction.South); break;
                case GameState.TransportMarkerActive: MoveCommand_TransportMarkerActive(Direction.South); break;
            }
        }
        /// <summary>
        /// Controls the Commands of the A Button.
        /// </summary>
        protected virtual void AButtom()
        {
            switch(currentState)
            {
                case GameState.PlayerSelect: LeftCommand_PlayerSelect(); break;
                case GameState.PlayerActionSelection: LeftCommand_PlayerActionSelection(); break;
                case GameState.PlayerMove: MoveCommand_PlayerMove(Direction.West); break;
                case GameState.TreasureSelection: LeftCommand_TreasureSelect(); break;
                case GameState.TransportMarkerActive: MoveCommand_TransportMarkerActive(Direction.West); break;
            }
        }
        /// <summary>
        /// Controls the Commands of the D Button.
        /// </summary>
        protected virtual void DButtom()
        {
            switch (currentState)
            {
                case GameState.PlayerSelect: RightCommand_PlayerSelect(); break;
                case GameState.PlayerActionSelection: RightCommand_PlayerActionSelection(); break;
                case GameState.PlayerMove: MoveCommand_PlayerMove(Direction.East); break;
                case GameState.TreasureSelection: RightCommand_TreasureSelect(); break;
                case GameState.TransportMarkerActive: MoveCommand_TransportMarkerActive(Direction.East); break;
            }
        }
        /// <summary>
        /// Controls the Commands of the J Button.
        /// </summary>
        protected virtual void JButtom()
        {
            switch (currentState)
            {
                case GameState.PlayerSelect: SelectCommand_PlayerSelect(); break;
                case GameState.TurnOrderDecide: SelectCommand_TurnDecider(); break;
                case GameState.TurnDeciderClose: SelectCommand_TurnDeciderOver(); break;
                case GameState.PlayerActionSelection: SelectCommand_PlayerActionSelection(); break;
                case GameState.ActionRollDiceToMove: SelectCommand_ActionRollDiceToMove(); break;
                case GameState.ActionRollDiceToMoveFinish: SelectCommand_ActionRollDiceToMoveFinish(); break;
                case GameState.LockedDoorRoll: SelectCommand_LockedDoorRoll(); break;
                case GameState.BattleAttackRoll: SelectCommand_BattleAttackRoll(); break;
                case GameState.BattleOver: SelectCommand_BattleOver(); break;
                case GameState.TurnLostDeciderRoll: SelectCommand_TurnLostDeciderRoll(); break;
                case GameState.TurnLost: SelectCommand_TurnLost(); break;
                case GameState.TreasureSelection: SelectCommand_TreasureSelect(); break;
                case GameState.ExitGame: SelectCommand_ExitGame(); break;
                case GameState.GameOver: SelectCommand_GameOver(); break;
                case GameState.UseElixir: SelectCommand_UseElixir(); break;
                case GameState.TransportMarkerActive: SelectCommand_TransportMarkerActive(); break;
            }
        }
        /// <summary>
        /// Controls the Commands of the I Button.
        /// </summary>
        protected virtual void IButtom()
        {
            switch (currentState)
            {
                case GameState.LockedDoorRoll: CancelCommand_LockedDoorRoll(); break;
                case GameState.TransportMarkerActive: CancelCommand_TransportMarkerActive(); break;
            }
        }
        /// <summary>
        /// Controls the Commands of the K Button.
        /// </summary>
        protected virtual void KButtom()
        {
        }
        /// <summary>
        /// Controls the Commands of the L Button.
        /// </summary>
        protected virtual void LButtom()
        {
        }
        /// <summary>
        /// Controls the Commands of the Enter Button.
        /// </summary>
        protected virtual void EnterButtom()
        {
        }
        #endregion

        #region Internal System functions
        private void UpdateDebugOutput()
        {
            lblDebugOutput.Text = "State: " + currentState + "\r\n" +
                                  "activePlayerID: " + activePlayerID + "\r\n" +
                                  "Input Enabled: " + InputEnabled;
        }
        private bool InputEnabled = true;
        /// <summary>
        /// Processes the Key Press in the Key Board.
        /// </summary>
        /// <param name="msg">Windows message.</param>
        /// <param name="keyData">Name of the Key pressed.</param>
        /// <returns><b>True</b> if the Key was processed.</returns>
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.W)
            {
                if(InputEnabled)
                {
                    InputEnabled = false;
                    UpdateDebugOutput();
                    WButtom();
                    InputEnabled = true;
                    UpdateDebugOutput();
                }
                return true;
            }
            else if (keyData == Keys.S)
            {
                if (InputEnabled)
                {
                    InputEnabled = false;
                    UpdateDebugOutput();
                    SButtom();
                    InputEnabled = true;
                    UpdateDebugOutput();
                }
                return true;
            }
            else if (keyData == Keys.A)
            {
                if (InputEnabled)
                {
                    InputEnabled = false;
                    UpdateDebugOutput();
                    AButtom();
                    InputEnabled = true;
                    UpdateDebugOutput();
                }
                return true;
            }
            else if (keyData == Keys.D)
            {
                if (InputEnabled)
                {
                    InputEnabled = false;
                    UpdateDebugOutput();
                    DButtom();
                    InputEnabled = true;
                    UpdateDebugOutput();
                }
                return true;
            }
            else if (keyData == Keys.J)
            {
                if (InputEnabled)
                {
                    InputEnabled = false;
                    UpdateDebugOutput();
                    JButtom();
                    InputEnabled = true;
                    UpdateDebugOutput();
                }
                return true;
            }
            else if (keyData == Keys.I)
            {
                if (InputEnabled)
                {
                    InputEnabled = false;
                    UpdateDebugOutput();
                    IButtom();
                    InputEnabled = true;
                    UpdateDebugOutput();
                }
                return true;
            }
            else if (keyData == Keys.K)
            {
                if (InputEnabled)
                {
                    InputEnabled = false;
                    UpdateDebugOutput();
                    KButtom();
                    InputEnabled = true;
                }
                return true;
            }
            else if (keyData == Keys.L)
            {
                if (InputEnabled)
                {
                    InputEnabled = false;
                    UpdateDebugOutput();
                    LButtom();
                    InputEnabled = true;
                    UpdateDebugOutput();
                }
                return true;
            }
            else if (keyData == Keys.Enter)
            {
                if (InputEnabled)
                {
                    InputEnabled = false;
                    UpdateDebugOutput();
                    EnterButtom();
                    InputEnabled = true;
                    UpdateDebugOutput();
                }
                return true;
            }
            else
                return base.ProcessCmdKey(ref msg, keyData);
        }
        /// <summary>
        /// Closes the whole application when this Form is closed.
        /// </summary>
        /// <param name="e">Event Argument.</param>
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            Application.Exit();
        }
        #endregion
    }
}