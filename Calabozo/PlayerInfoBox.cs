﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Calabozo
{
    class PlayerInfoBox
    {
        public PlayerInfoBox(PlayerData _data, Form _mainBoard)
        {
            thisPlayerData = _data;

            //Main Box
            PanelMainBox.Size = new Size(121, 196);
            PanelMainBox.BorderStyle = BorderStyle.FixedSingle;
            PanelMainBox.BackgroundImage = ImageServer.CharacterImage(_data.CharID);
            PanelMainBox.BackgroundImageLayout = ImageLayout.Stretch;
            _mainBoard.Controls.Add(PanelMainBox);
            switch(_data.PlayerID)
            {
                case Player.Player1: PanelMainBox.Location = new Point(5, 9); break;
                case Player.Player2: PanelMainBox.Location = new Point(1270, 9); break;
                case Player.Player3: PanelMainBox.Location = new Point(5, 230); break;
                case Player.Player4: PanelMainBox.Location = new Point(1270, 230); break;
                case Player.Player5: PanelMainBox.Location = new Point(5, 450); break;
                case Player.Player6: PanelMainBox.Location = new Point(1270, 450); break;
                case Player.Player7: PanelMainBox.Location = new Point(5, 670); break;
                case Player.Player8: PanelMainBox.Location = new Point(1270, 670); break;
            }

            lblName.AutoSize = false;
            lblName.Size = new Size(99, 25);
            lblName.Font = new Font("Arial Narrow", 12, FontStyle.Bold);
            lblName.BackColor = Color.Black;
            lblName.ForeColor = Color.White;
            lblName.TextAlign = ContentAlignment.TopCenter;
            PanelMainBox.Controls.Add(lblName);
            lblName.Location = new Point(8, 3);
            lblName.Text = _data.PlayerID.ToString();

            lblTreasures.AutoSize = false;
            lblTreasures.Size = new Size(107, 82);
            lblTreasures.Font = new Font("Arial Narrow", 12, FontStyle.Bold);
            lblTreasures.BackColor = Color.Black;
            lblTreasures.ForeColor = Color.White;
            lblTreasures.TextAlign = ContentAlignment.TopCenter;
            PanelMainBox.Controls.Add(lblTreasures);
            lblTreasures.Location = new Point(6, 43);
            lblTreasures.Text = "Treasures: x0";

            lblMonsters.AutoSize = false;
            lblMonsters.Size = new Size(99, 52);
            lblMonsters.Font = new Font("Arial Narrow", 12, FontStyle.Bold);
            lblMonsters.BackColor = Color.Black;
            lblMonsters.ForeColor = Color.White;
            lblMonsters.TextAlign = ContentAlignment.TopCenter;
            PanelMainBox.Controls.Add(lblMonsters);
            lblMonsters.Location = new Point(9, 135);
            lblMonsters.Text = "Monsters Defeated: x0";

            for(int x = 0; x < 6; x++)
            {
                PictureBox Picring = new PictureBox();
                Picring.Size = new Size(17, 17);
                Picring.Image = ImageServer.Ring();
                Picring.SizeMode = PictureBoxSizeMode.StretchImage;
                Picring.Visible = false;
                PanelMainBox.Controls.Add(Picring);
                Picring.BringToFront();
                Picring.BorderStyle = BorderStyle.FixedSingle;
                switch(x)
                {
                    case 0: Picring.Location = new Point(8, 70); break;
                    case 1: Picring.Location = new Point(25, 70); break;
                    case 2: Picring.Location = new Point(42, 70); break;
                    case 3: Picring.Location = new Point(59, 70); break;
                    case 4: Picring.Location = new Point(76, 70); break;
                    case 5: Picring.Location = new Point(93, 70); break;
                }
                Rings.Add(Picring);
            }

            for (int x = 0; x < 6; x++)
            {
                PictureBox PicWeapon = new PictureBox();
                PicWeapon.Size = new Size(17, 17);
                switch (x)
                {
                    case 0: PicWeapon.Image = ImageServer.Weapon(CardID.BaculoMagico); break;
                    case 1: PicWeapon.Image = ImageServer.Weapon(CardID.EspadaMagica); break;
                    case 2: PicWeapon.Image = ImageServer.Weapon(CardID.BallestaMagica); break;
                    case 3: PicWeapon.Image = ImageServer.Weapon(CardID.EstrellaMagica); break;
                    case 4: PicWeapon.Image = ImageServer.Weapon(CardID.GuanteMagico); break;
                    case 5: PicWeapon.Image = ImageServer.Weapon(CardID.HachaMagica); break;
                }
                PicWeapon.SizeMode = PictureBoxSizeMode.StretchImage;
                PicWeapon.Visible = false;
                PanelMainBox.Controls.Add(PicWeapon);
                PicWeapon.BringToFront();
                PicWeapon.BorderStyle = BorderStyle.FixedSingle;
                switch (x)
                {
                    case 0: PicWeapon.Location = new Point(12, 87); break;
                    case 1: PicWeapon.Location = new Point(28, 87); break;
                    case 2: PicWeapon.Location = new Point(44, 87); break;
                    case 3: PicWeapon.Location = new Point(60, 87); break;
                    case 4: PicWeapon.Location = new Point(76, 87); break;
                    case 5: PicWeapon.Location = new Point(92, 87); break;
                }
                Weapons.Add(PicWeapon);
            }

            for (int x = 0; x < 3; x++)
            {
                PictureBox PicItem = new PictureBox();
                PicItem.Size = new Size(17, 17);
                switch (x)
                {
                    case 0: PicItem.Image = ImageServer.Weapon(CardID.LlaveMagica); break;
                    case 1: PicItem.Image = ImageServer.Weapon(CardID.MedallonMagico); break;
                    case 2: PicItem.Image = ImageServer.Weapon(CardID.Velocidad); break;
                }
                PicItem.SizeMode = PictureBoxSizeMode.StretchImage;
                PicItem.Visible = false;
                PanelMainBox.Controls.Add(PicItem);
                PicItem.BringToFront();
                PicItem.BorderStyle = BorderStyle.FixedSingle;
                switch (x)
                {
                    case 0: PicItem.Location = new Point(33, 105); break;
                    case 1: PicItem.Location = new Point(53, 105); break;
                    case 2: PicItem.Location = new Point(73, 105); break;
                }
                MagicItems.Add(PicItem);
            }
        }

        public void UpdateData()
        {
            lblTreasures.Text = "Treasures: x" + thisPlayerData.TreasuresOnHold;
            for (int x = 0; x < thisPlayerData.RingsOnHold; x++)
            {
                Rings[x].Visible = true;
            }

            //display the weapons
            if (thisPlayerData.HasTreasure(CardID.BaculoMagico)) { Weapons[0].Visible = true; }
            if (thisPlayerData.HasTreasure(CardID.EspadaMagica)) { Weapons[1].Visible = true; }
            if (thisPlayerData.HasTreasure(CardID.BallestaMagica)) { Weapons[2].Visible = true; }
            if (thisPlayerData.HasTreasure(CardID.EstrellaMagica)) { Weapons[3].Visible = true; }
            if (thisPlayerData.HasTreasure(CardID.GuanteMagico)) { Weapons[4].Visible = true; }
            if (thisPlayerData.HasTreasure(CardID.HachaMagica)) { Weapons[5].Visible = true; }

            //display magic items
            if (thisPlayerData.HasTreasure(CardID.LlaveMagica)) { MagicItems[0].Visible = true; }
            if (thisPlayerData.HasTreasure(CardID.MedallonMagico)) { MagicItems[1].Visible = true; }
            if (thisPlayerData.HasTreasure(CardID.Velocidad)) { MagicItems[2].Visible = true; }

            lblMonsters.Text = "Monsters Defeated: x" + thisPlayerData.MonstersDefeated;
        }

        private PlayerData thisPlayerData;
        private Panel PanelMainBox = new Panel();
        private Label lblName = new Label();
        private Label lblTreasures = new Label();
        private Label lblMonsters = new Label();
        private List<PictureBox> Rings = new List<PictureBox>();
        private List<PictureBox> Weapons = new List<PictureBox>();
        private List<PictureBox> MagicItems = new List<PictureBox>();
    }
}
