﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Calabozo
{
    class PlayerData
    {
        public PlayerData(CharacterID _cid, Casilla _startingCasilla, Player _pid)
        {
            cID = _cid;
            playerID = _pid;

            currentCasilla = _startingCasilla;
            previousCasilla = currentCasilla;

            previousDirection = (Direction)Rand.Range(0, 4);
        }
        public void SetSprite(PictureBox _pic)
        {
            spriteRef = _pic;
        }
        public bool IsDead { get { return isDead; } }
        public CharacterID CharID { get { return cID; } }
        public Player PlayerID { get { return playerID; } }
        public Point Location 
        { 
            get
            { 
                if(currentCasilla.id == (int)CasillasID.MainHall)
                {
                    switch(cID)
                    {
                        case CharacterID.Warrior1: return new Point(497, 370);
                        case CharacterID.Warrior2: return new Point(596, 472);
                        case CharacterID.Dwarf1: return new Point(596, 371);
                        case CharacterID.Dwarf2: return new Point(497, 472);
                        case CharacterID.Mage1: return new Point(497, 422);
                        case CharacterID.Mage2: return new Point(596, 422);
                        case CharacterID.Elf1: return new Point(546, 370);
                        case CharacterID.Elf2: return new Point(546, 472);
                        default: throw new Exception();
                    }
                }
                else
                {
                    return new Point(currentCasilla.xCord, currentCasilla.yCord);
                }    
            } 
        }
        public Casilla CurrentCasilla { get { return currentCasilla; } set { currentCasilla = value; } }
        public Casilla PreviousCasilla { get { return previousCasilla; } set { previousCasilla = value; } }
        public Casilla PreviousPreviousCasilla { get { return previousPreviousCasilla; } set { previousPreviousCasilla = value; } }
        public int GetNextCasillaID(Direction _d)
        {
            switch (_d)
            {
                case Direction.North: return CurrentCasilla.northCasillaID;
                case Direction.South: return CurrentCasilla.southCasillaID;
                case Direction.East: return CurrentCasilla.eastCasillaID;
                case Direction.West: return CurrentCasilla.westCasillaID;
                default: throw new Exception();
            }
        }
        public PictureBox Sprite { get { return spriteRef; } }
        public bool HasTreasure(CardID _id)
        {
            bool found = false;

            for (int x = 0; x < treasuresOnHold.Count; x++)
            {
                if (treasuresOnHold[x].ID == _id)
                {
                    found = true; break;
                }
            }

            return found;
        }
        public int GetTreasureIndex(CardID _treasure)
        {
            int index = -1;

            for (int x = 0; x < treasuresOnHold.Count; x++)
            {
                if (treasuresOnHold[x].ID == _treasure)
                {
                    index = x; break;
                }
            }

            return index;
        }
        public bool HasAnElixir()
        {
            bool found = false;

            for (int x = 0; x < treasuresOnHold.Count; x++)
            {
                if (treasuresOnHold[x].ID == CardID.Elixir)
                {
                    found = true; break;
                }
            }

            return found;
        }
        public bool HasATeletranspotation()
        {
            bool found = false;

            for (int x = 0; x < treasuresOnHold.Count; x++)
            {
                if (treasuresOnHold[x].ID == CardID.Teletransportacion)
                {
                    found = true; break;
                }
            }

            return found;
        }
        public bool HasMedallion()
        {
            bool found = false;

            for (int x = 0; x < treasuresOnHold.Count; x++)
            {
                if (treasuresOnHold[x].ID == CardID.MedallonMagico)
                {
                    found = true; break;
                }
            }

            return found;
        }
        public bool HasKey()
        {
            bool found = false;

            for (int x = 0; x < treasuresOnHold.Count; x++)
            {
                if (treasuresOnHold[x].ID == CardID.LlaveMagica)
                {
                    found = true; break;
                }
            }

            return found;
        }
        public bool HasValocidad()
        {
            bool found = false;

            for (int x = 0; x < treasuresOnHold.Count; x++)
            {
                if (treasuresOnHold[x].ID == CardID.Velocidad)
                {
                    found = true; break;
                }
            }

            return found;
        }
        public void Dies()
        {
            isDead = true;
        }
        public void Revives()
        {
            isDead = false;
        }
        public int RingsOnHold { get { return ringOnHold; } }
        public int TreasuresOnHold { get { return treasuresOnHold.Count; } }
        public List<Card> TreasureList { get { return treasuresOnHold; } }
        public int BonusPoints { get { return bonusPoints; } }
        public int MonstersDefeated { get { return monstersDefeated.Count; } }
        public int TurnsLost { get { return turnsLost; } set { turnsLost = value; } }
        public Direction PreviousDirection { get { return previousDirection; } set { previousDirection = value; } }
        public bool CanMoveOn(Direction _direction)
        {
            bool _canMove = false;

            switch(_direction)
            {
                case Direction.North: if(currentCasilla.northDoor != "Wall") { _canMove = true; } break;
                case Direction.South: if(currentCasilla.southDoor != "Wall") { _canMove = true; } break;
                case Direction.East: if(currentCasilla.eastDoor != "Wall") { _canMove = true; } break;
                case Direction.West: if(currentCasilla.westDoor != "Wall") { _canMove = true; } break;
            }

            return _canMove;
        }
        public void AddTreasure(Card _treasure)
        {
            treasuresOnHold.Add(_treasure);

            if(_treasure.type == "Weapon")
            {
                //add bonus point
                switch(cID)
                {
                    case CharacterID.Warrior1: bonusPoints += _treasure.warriorDamage; break;
                    case CharacterID.Warrior2: bonusPoints += _treasure.warriorDamage; break;
                    case CharacterID.Dwarf1: bonusPoints += _treasure.dwarfDamage; break;
                    case CharacterID.Dwarf2: bonusPoints += _treasure.dwarfDamage; break;
                    case CharacterID.Mage1: bonusPoints += _treasure.mageDamage; break;
                    case CharacterID.Mage2: bonusPoints += _treasure.mageDamage; break;
                    case CharacterID.Elf1: bonusPoints += _treasure.elfDamage; break;
                    case CharacterID.Elf2: bonusPoints += _treasure.elfDamage; break;
                }
            }

            if (_treasure.ID == CardID.AnilloMagico) { ringOnHold++; }
        }
        public void RemoveTreasure(int index)
        {
            if (treasuresOnHold[index].type == "Weapon")
            {
                //add bonus point
                switch (cID)
                {
                    case CharacterID.Warrior1: bonusPoints -= treasuresOnHold[index].warriorDamage; break;
                    case CharacterID.Warrior2: bonusPoints -= treasuresOnHold[index].warriorDamage; break;
                    case CharacterID.Dwarf1: bonusPoints -= treasuresOnHold[index].dwarfDamage; break;
                    case CharacterID.Dwarf2: bonusPoints -= treasuresOnHold[index].dwarfDamage; break;
                    case CharacterID.Mage1: bonusPoints -= treasuresOnHold[index].mageDamage; break;
                    case CharacterID.Mage2: bonusPoints -= treasuresOnHold[index].mageDamage; break;
                    case CharacterID.Elf1: bonusPoints -= treasuresOnHold[index].elfDamage; break;
                    case CharacterID.Elf2: bonusPoints -= treasuresOnHold[index].elfDamage; break;
                }
            }

            if (treasuresOnHold[index].ID == CardID.AnilloMagico) { ringOnHold--; }

            //then remove it
            treasuresOnHold.RemoveAt(index);
        }
        public void AddMonsterDefeated(Card _monster)
        {
            monstersDefeated.Add(_monster);
        }

        private CharacterID cID = CharacterID.None;
        private Player playerID;
        private int ringOnHold = 0;
        private List<Card> treasuresOnHold = new List<Card>();
        private List<Card> monstersDefeated = new List<Card>();
        private int bonusPoints = 0;
        private Casilla currentCasilla;
        private Casilla previousCasilla;
        private Casilla previousPreviousCasilla;
        private PictureBox spriteRef;
        private int turnsLost = 0;
        private Direction previousDirection;
        private bool isDead = false;
    }
}
