﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Calabozo
{
    class Casilla
    {

        public string GetDoor(Direction _d)
        {
            switch(_d)
            {
                case Direction.North: return northDoor;
                case Direction.South: return southDoor;
                case Direction.East: return eastDoor;
                case Direction.West: return westDoor;
                default: throw new Exception();
            }
        }

        public Point GetCordinates()
        {
            return new Point(xCord, yCord);
        }

        public void MarkMonterFlipped()
        {
            monsterUp = true;
        }

        public void AddOccupant(Player _p)
        {
            occupants.Add(_p);
        }
        public void RemoveOccupant(Player _p)
        {
            occupants.Remove(_p);
        }

        public bool IsOccupied { get { return occupants.Count != 0; } }
        public bool IsOccupiedBySomeoneElse { get { return occupants.Count > 1; } }

        public int id = 0;
        public string type = "Tower";
        public int cellID = -1;
        public int towerID = -1;
        public bool isMonsterInIt = false;
        public bool monsterUp = false;
        
        public string northDoor = "Free";
        public string southDoor = "Free";
        public string eastDoor = "Free";
        public string westDoor = "Free";

        public int northCasillaID = 0;
        public int southCasillaID = 0;
        public int eastCasillaID = 0;
        public int westCasillaID = 0;

        public int xCord = 0;
        public int yCord = 0;

        private List<Player> occupants = new List<Player>();
    }
}
